/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
Controllers.controller("PanelChannelController",
    ["$rootScope", "$scope", "VideoBroadcastService", "SearchManagerService",
     function($rootScope, $scope, VideoBroadcastService, SearchManagerService) {
        $scope.isRequired = false;
//        VideoBroadcastService.videoBroadcastObject.currentChannel = {logoURL: ""};
        $scope.videoBroadcastService = VideoBroadcastService;
        $scope.searchManagerService = SearchManagerService;
        $scope.programmeTitle;
        $scope.startTime;
        $scope.stopTime;
        $scope.genres;
        $scope.duration;
        $scope.parentalRating = -10;
        $scope.newWidth = "{'width':'50px'}";

        $scope.chanLogo = "";

    $rootScope.$watch("channelInfo", function(newInfo) {
        console.group("PanelChannelController");
        console.log("PanelChannelController");
        if (newInfo) {
            console.log("Panel channel info will be activated.");
            $scope.chanLogo = $scope.videoBroadcastService.getCurrentChannelLogoUrl();
            $scope.programmeTitle = $scope.searchManagerService.getCurrentProgrammeName();
            $scope.genres = $scope.searchManagerService.getCurrentProgrammeGenre();
            $scope.startTime = $scope.searchManagerService.getCurrentProgrammeStartTime() * 1000;
            $scope.duration = $scope.searchManagerService.getCurrentProgrammeDuration() * 1000;
            $scope.stopTime = $scope.duration + $scope.startTime;
            $scope.newWidth = "{'width':'70px'}";
            $scope.isRequired = true;

        } else if (!newInfo) {
            console.log("Panel channel info will be deactivated.");
            $scope.isRequired = false;
        }
        console.groupEnd("PanelChannelController");
    });

}]);
