/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
Controllers.controller("PowerStateController",
    ["$rootScope", "$scope", "$timeout", "ConfigurationService", "ChannelConfigService",
     function($rootScope, $scope, $timeout, ConfigurationService, ChannelConfigService) {

    $scope.state = "show";
    $scope.beforeON = false;
    $rootScope.systemReady = false;
    $scope.configurationService = ConfigurationService;
    $scope.channelConfigService = ChannelConfigService;

    $scope.configurationService.configurationObject.localSystem.onPowerStateChange = function(state) {

        if ($scope.waitingState == state && $scope.result) {
            if (state == 0) {
                console.log("POWER STATE CHANGE: OFF.");
                $scope.state = "show";
                $rootScope.systemReady = false;

            } else {
                console.log("POWER STATE CHANGE: ON.");
                $scope.setSystemReadyAndHideWaitingView();
            }

        } else {

            console.log("POWER STATE CHANGE: ERROR.");

        }
        $scope.$apply();

        console.groupEnd("PowerStateController");
    };

    $scope.$on("PowerStateChangeEvent", function() {

            console.group("PowerStateController");
            console.log("The power state will change.");

            if ($scope.configurationService.getPowerState() == 0) {

                $scope.waitingState = 1;
                $scope.beforeON = true;
                $timeout(function() {
                    $scope.result = $scope.configurationService.setPowerState(1);
                }, 3500);

            } else if ($scope.configurationService.getPowerState() == 1) {

                $scope.waitingState = 0;
                $scope.result = $scope.configurationService.setPowerState(0);
            }

    });

    $scope.setSystemReadyAndHideWaitingView = function() {
        console.log("The system is ready.");
        $scope.state = "hidden";
        $rootScope.systemReady = true;
        $scope.beforeON = false;
    }

}]);
