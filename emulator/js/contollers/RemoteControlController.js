/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
Controllers.controller("RemoteControlController",
    ["$rootScope", "$scope", "$http", "VideoBroadcastService", function($rootScope, $scope, $http, VideoBroadcastService) {

        $http.get("./emulator/remoteControl/remoteControl.json").success(function(data) {
          $scope.remoteControl = data;
        });

        $scope.remoteControlClick = function(buttonId) {
            var methodName = $scope.clickMapping[buttonId];
            console.log("methodName:", methodName, $scope[methodName]);
            methodName && $scope[methodName].call($scope, buttonId);
        };

        $scope.videoBroadcastService = VideoBroadcastService;

        $scope.channel = "";
        $scope.channelNumber = "";
        $scope.channelNumberActive = false;
        $rootScope.channelChange = false;

        $scope.setPowerChange = function() {
            $scope.$broadcast("PowerStateChangeEvent");
        };

        $scope.setChannel = function(changeType) {
            $scope.$broadcast("ChannelChangeEvent", changeType);
        };

        $scope.setVolume = function(changeType) {
//            $rootScope.volumeChange = id;
            $scope.$broadcast("VolumeEvent", changeType);
        };

        $scope.setChannelByNumber = function(number) {
        };

        $scope.clickMapping = {
            POWER: "setPowerChange",
            0: "setChannelByNumber",
            1: "setChannelByNumber",
            2: "setChannelByNumber",
            3: "setChannelByNumber",
            4: "setChannelByNumber",
            5: "setChannelByNumber",
            6: "setChannelByNumber",
            7: "setChannelByNumber",
            8: "setChannelByNumber",
            9: "setChannelByNumber",
            "V+": "setVolume",
            "V-": "setVolume",
            "P+": "setChannel",
            "P-": "setChannel"
        };

}]);
