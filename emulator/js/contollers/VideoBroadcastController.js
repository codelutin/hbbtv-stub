/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
Controllers.controller("VideoBroadcastController",
    ["$rootScope", "$scope", "$timeout", "VideoBroadcastService", "SearchManagerService", "ConfigurationService",
     function($rootScope, $scope, $timeout, VideoBroadcastService, SearchManagerService, ConfigurationService) {

    $scope.videoBroadcastService = VideoBroadcastService;
    $scope.searchManagerService = SearchManagerService;
    $scope.configurationService = ConfigurationService;

    $scope.chanLogo = "";
    $scope.displayLogo = true;
    $scope.promiseVolumeView = null;
    $scope.volumeClass = "glyphicon-volume-down";
    $scope.currentVolume = $scope.configurationService.configurationObject.localSystem.volume;

    $rootScope.channelInfo = false;
    $scope.volumeViewActive = false;

    $scope.setChannelByNumber = function(buttonId) {
            $scope.channelNumberActive = true;

            if ($scope.channel.length == 0) {
                $timeout(function() {
                    $scope.channel = $scope.channelNumber.toString();
                    $scope.channelNumber = "";
                    $scope.channelNumberActive = false;
                }, 3000);
            }
            $scope.channelNumber += buttonId;
    };

    $scope.$on("ChannelChangeEvent", function(event, changeType) {
        console.log(changeType);
        $scope.videoBroadcastService.changeChannel(changeType)
            .then(function() {
                console.log($scope.videoBroadcastService.getCurrentChannel().name);
            })
            .then(function() {
                return $scope.searchManagerService
                    .findCurrentProgramme($scope.videoBroadcastService.getCurrentChannel());
            })
            .then(function() {
                $rootScope.channelInfo = false;
                $timeout(function() {
                    $scope.chanLogo = $scope.videoBroadcastService.getCurrentChannelLogoUrl();
                    $scope.displayLogo = true;
                    $rootScope.channelInfo = true;
                }, 1000);
            });
        });

    $scope.$on("VolumeEvent", function(event, changeType) {
        console.log("A VolumeEvent has been received: The volume will change.");
        console.log("Activation of volume view.");

        if (changeType == "V+") {
            $scope.volumeClass = "glyphicon-volume-up";

        } else if (changeType == "V-") {
            $scope.volumeClass = "glyphicon-volume-down";
        }

        $scope.volumeViewActive = true;
        var result = $scope.videoBroadcastService.setVolume(changeType);

        if (result) {
            $scope.currentVolume = $scope.videoBroadcastService.getVolume();
        }

        $scope.promiseVolumeView = $timeout(function() {
            console.log("Deactivation of volume view.");
            $scope.volumeViewActive = false;
        }, 5000);

   });

    $rootScope.$watch("systemReady", function(isReady) {

        if (isReady) {
            console.group("VideoBroadcastController");
            console.log("System ready after power ON: active channel info view");
            var promise = $scope.videoBroadcastService.bindToCurrentChannel.call($scope.videoBroadcastService);

            promise.then(function() {
                console.log("VideoBroadcast current channel binding OK.");
                console.groupEnd("VideoBroadcastController");
//                $rootScope.channelInfo = true;
                $scope.chanLogo = $scope.videoBroadcastService.getCurrentChannelLogoUrl();
            })
            .then(function() {
                return $scope.searchManagerService
                    .findCurrentProgramme($scope.videoBroadcastService.getCurrentChannel());
            })
            .then(function() {
                console.log("Current programme OK.");
                $rootScope.channelInfo = true;
                $scope.displayLogo = true;
                console.groupEnd("VideoBroadcastController");
                $timeout(function() {
//                        $rootScope.channelInfo = false;
                }, 4000);
            });
        } else {
            var promise = $scope.videoBroadcastService.release.call($scope.videoBroadcastService);
            promise.then(function() {
                console.group("VideoBroadcastController");
                $scope.chanLogo = "";
                $rootScope.channelInfo = false;
                $scope.displayLogo = false;
                console.log("The video-broadcast object have been released, SYSTEM OFF.")
                console.groupEnd("VideoBroadcastController");
            });
        }

    });

     /*$rootScope.$watch("channelChange", function(type) {

        if (type == "+P") {
           console.group("VideoBroadcastController");
            console.log("Require a channel switch up");
            var promise = $scope.videoBroadcastService.prevChannel.call($scope.videoBroadcastService);

            promise.then(function() {
                $rootScope.channelInfo = true;
                $scope.chanLogo = $scope.videoBroadcastService.getCurrentChannelLogoUrl();
                $timeout(function() {
                        $rootScope.channelInfo = false;
                }, 4000);
            });

        } else {
            console.group("VideoBroadcastController");
            console.log("Require a channel switch down");
            var promise = $scope.videoBroadcastService.nextChannel.call($scope.videoBroadcastService);

            promise.then(function() {
                $rootScope.channelInfo = true;
                $scope.chanLogo = $scope.videoBroadcastService.getCurrentChannelLogoUrl();
                $timeout(function() {
                        $rootScope.channelInfo = false;
                }, 4000);
            });
        }

    });*/

}]);
