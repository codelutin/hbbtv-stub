/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
Directives.directive("programmeProgress", ["$interval", function($interval) {

  function link(scope, element, attrs) {
    var timeoutId;
//    var increment = 1000 * 100 / scope.duration;

      //FIX-ME: Add a block for programmes already started
    function updateProgrammeProgress() {
        /*if (element.width == 100) {

        }
        element.width += increment + "%";*/
        console.log("WIDTH", element, element.css("width"));
        $interval.cancel(timeoutId);
    }

    scope.$watch(attrs.programmeProgress, function(value) {
         element.animate({
            width: "+=50px"
          });
        updateProgrammeProgress();
      });

    element.on("$destroy", function() {
      $interval.cancel(timeoutId);
    });

    /*// start the UI update process; save the timeoutId for canceling
    timeoutId = $interval(function() {
      updateProgrammeProgress(); // update DOM
    }, 1000);*/
  }

  return {
    link: link
  };

}]);
