/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
OIPFServices.factory("SearchManagerService", ["$q", function(promise) {

    var searchManagerService = new Service();

    var searchManagerObject = oipfObjectFactory.createSearchManagerObject();

    searchManagerService.setCurrentProgramme = function(programme) {
        this.currentProgramme = programme;
    };

    searchManagerService.getCurrentProgramme = function() {
        return this.currentProgramme;
    };

    searchManagerService.getCurrentProgrammeName = function() {
        return this.currentProgramme.name || "Programme";
    };

    searchManagerService.getCurrentProgrammeGenre = function() {
        return this.currentProgramme.genre || ["Unknow"];
    };

    searchManagerService.getCurrentProgrammeStartTime = function() {
        return this.currentProgramme.startTime || "Unkonw";
    };

    searchManagerService.getCurrentProgrammeDuration = function() {
        return this.currentProgramme.duration || "Unkonw";
    };

    searchManagerObject.onMetadataSearch = function(resolve, reject, search, state) {

            console.log("[INFO]: onMetadataSearch called");

            switch (state) {

                case 0:
                    if (search.result.length == 1) {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: found");
                        searchManagerService.setCurrentProgramme(search.result[0]);
                        resolve();

                    } else {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: not found");
                        reject();
                    }
                    break;

                case 3:
                    var message = "[INFO] MetadataSearch in Idle state because of either search abort or parameters have been modified (query, constraints or search target)";
                    console.log(message);
                    reject();
                    break;

                case 4:
                    message = "[INFO] The search cannot be complete because of lack of ressources or any other reason.";
                    console.log(message);
                    reject();
                    break;

                default:
                    console.log("Unknow state");
                    reject();
            }
        };

    var deferred = promise.defer();

    searchManagerObject.onMetadataSearch = searchManagerObject.onMetadataSearch
            .bind(searchManagerObject, deferred.resolve, deferred.reject);

    searchManagerService.findCurrentProgramme = function(channel) {
        var searchTarget = 1;
        var metaDataSearch = this.searchManagerObject.createSearch(searchTarget);
        var startTime = null;
        metaDataSearch.findProgrammesFromStream(channel, startTime);
        var offset = 0;
        var count = 1;
        metaDataSearch.result.getResults(offset, count);

        return deferred.promise;
    };

    searchManagerService.searchManagerObject = searchManagerObject;

    return searchManagerService;
}]);
