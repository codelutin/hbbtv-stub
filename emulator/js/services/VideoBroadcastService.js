/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
OIPFServices.factory("VideoBroadcastService", ["$q", function($q) {

    var videoBroadcastService = new Service();
    var videoBroadcastObject = document.getElementById("videoBroadcast");

    if (videoBroadcastObject) {

        videoBroadcastService.videoBroadcastObject = videoBroadcastObject;

    } else {

        console.log("There is no video-broadcast object present in DOM!!!!");

    }

    videoBroadcastService.getCurrentChannel = function() {
        return this.videoBroadcastObject.currentChannel;
    };

    videoBroadcastService.getCurrentChannelLogoUrl = function() {
        return this.getCurrentChannel().logoURL;
    };

    videoBroadcastService.bindToCurrentChannel = function() {
        var deferred = $q.defer();
        console.log("bindToCurrentChannel", deferred);

        this.previousState = this.videoBroadcastObject.playState;
        this.currentIndex = 0;
        this.resetTransition();

        this.videoBroadcastObject.onPlayStateChange = function(state, error) {
            if (state == videoBroadcastConstants.state.PRESENTING) {
                deferred.resolve();
            }
        };
//        this.onChangeState.bind(this, deferred.resolve.bind(deferred), deferred.reject.bind(deferred));

        this.addTransition(videoBroadcastConstants.state.UNREALIZED,
                          videoBroadcastConstants.state.CONNECTING);

        this.addTransition(videoBroadcastConstants.state.CONNECTING,
                          videoBroadcastConstants.state.PRESENTING,
                           deferred.resolve.bind(deferred));

        this.videoBroadcastObject.bindToCurrentChannel();

        return deferred.promise;
    };

    videoBroadcastService.release = function() {

        var deferred = $q.defer();

        if (this.videoBroadcastObject.playState) {
            this.resetTransition();
            this.currentIndex = 0;
            this.previousState = this.videoBroadcastObject.playState;

            this.videoBroadcastObject.onPlayStateChange =
                this.onChangeState.bind(this, deferred.resolve, deferred.reject);

            this.addTransition(this.previousState,
                              videoBroadcastConstants.state.UNREALIZED,
                              deferred.resolve);

            this.videoBroadcastObject.release && this.videoBroadcastObject.release();
        }


        return deferred.promise;
    };

    videoBroadcastService.changeChannel = function(changeType) {
        this.resetTransition();
        var deferred = $q.defer();
        this.currentIndex = 0;

        this.previousState = this.videoBroadcastObject.playState;

        this.addTransition(this.previousState,
                              videoBroadcastConstants.state.CONNECTING);

        this.addTransition(videoBroadcastConstants.state.CONNECTING,
                                      videoBroadcastConstants.state.PRESENTING);

        this.videoBroadcastObject.onChannelChangeSucceeded = function(channel) {
            deferred.resolve();
        };

        if (changeType == "P+") {
            this.videoBroadcastObject.nextChannel && this.videoBroadcastObject.prevChannel();

        } else if (changeType == "P-") {
            this.videoBroadcastObject.prevChannel && this.videoBroadcastObject.nextChannel();
        }

        return deferred.promise;

    };

    videoBroadcastService.setVolume = function(buttonId) {

        if (buttonId == "V+") {
            var volume = this.videoBroadcastObject.getVolume();
            var result = this.videoBroadcastObject.setVolume(volume + 1);
        } else if (buttonId == "V-") {
            var volume = this.videoBroadcastObject.getVolume();
            result = this.videoBroadcastObject.setVolume(volume - 1);
        }

        return result;
    }

    videoBroadcastService.getVolume = function() {
        this.videoBroadcastObject.getVolume && console.log(this.videoBroadcastObject.getVolume());
        return this.videoBroadcastObject.getVolume && this.videoBroadcastObject.getVolume() || 50;
    }

    return videoBroadcastService;
}]);
