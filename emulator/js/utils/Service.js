/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var Service = Class.extend({

    init: function() {

        this.transitions = []
        this.currentIndex = 0;

        this.onChangeState = function(resolve, reject, newState, errorCode) {
            var currentTransition = this.transitions[this.currentIndex];
            var begin = currentTransition.begin;
            var end = currentTransition.end;
            var error = currentTransition.error;

            if (begin == this.previousState && end == newState && error == errorCode) {
                console.log("Transition succeeded.");
                currentTransition.callback && currentTransition.callback();
                this.previousState = newState;
                this.currentIndex++;
            } else {
                if (begin == this.CONNECTING && this.transitions.length == this.currentIndex + 1) {
                    reject("Untestable case for the moment.");
                } else {
                    reject("Invalid state.");
                }
            }
        };

        this.addTransition = function(begin, end, callback) {
            this.addTransitionWithError(begin, end, null, callback);
        };

        this.addTransitionWithError = function(begin, end, error, callback) {
            this.transitions.push({
                begin: begin,
                end: end,
                error: error,
                callback: callback
            });
        };

        this.resetTransition = function() {
            this.transitions.length = 0;
        }

    }

});
