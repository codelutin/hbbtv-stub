This project is a web project built on top of oipf implementation. It gives you the ability to use the oipf implementation directly on your local machine.

To launch the project, run node server.js and browse http://<ip address of your machine>:4000
