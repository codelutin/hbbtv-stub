/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * The OITF SHALL support following non-visual embedded object with
 * the mime type application/oipfCapabilities.
 */
class CapabilitiesProperties {

    constructor() {
        this.defaultConfig = {
            extraSDVideoDecodes: null,
            extraHDVideoDecodes: null
        };

        this.baseUIProfileName = [
            "OITF_SDEU_UIPROF",
            "OITF_SD60_UIPROF",
            "OITF_SDUS_UIPROF",
            "OITF_HD_UIPROF",
            "OITF_FULL_HD_UIPROF"
        ];

        this.UIProfileNameFragment = [
            "+TRICKMODE",
            "+AVCAD",
            "+DL",
            "+IPTV_SDS",
            "+IPTV_URI",
            "+ANA",
            "+DVB_C",
            "+DVB_T",
            "+DVB_S",
            "+DVB_C2",
            "+DVB_T2",
            "+DVB_S2",
            "+META_BCG",
            "+META_EIT",
            "+META_SI",
            "+ITV_KEYS",
            "+CONTROLLED",
            "+PVR",
            "+DRM",
            "+CommunicationServices",
            "+SVG",
            "+POINTER",
            "+WIDGETS",
            "+HTML5_MEDIA",
            "+RCF",
            "+TELEPHONY",
            "+VIDEOTELEPHONY"
        ];

        this.profileNameMapping = {
            "+DVB_T": "DVB",
            "+META_EIT": "META"
            /*"+AVCAD": "DVB",
            "+IPTV_SDS": "DVB",
            "+IPTV_URI": "DVB",
            "+ANA": "DVB",
            "+DVB_C": "DVB",
            "+DVB_S": "DVB",
            "+DVB_C2": "DVB",
            "+DVB_T2": "DVB",
            "+DVB_S2": "DVB",
            "+META_BCG": "META",
            "+META_SI": "META"*/
        };

        this.roots = "profilelist";

        this.defaultCapabilities = {
            "DVB": {
                video_broadcast: {
                    value: true,
                    attributes: {
                        type: "ID_DVB_T ID_DVB_SI_DIRECT",
                        transport: "",
                        nrstreams: 1,
                        scaling: "arbitrary",
                        minSize: 0,
                        postList: false,
                        networkTimeshift: false,
                        localTimeshift: false
                    }
                }
            },
            "OITF_SDEU_UIPROF": {
                overlaylocaltuner: {
                    value: "per-pixel"
                }
            },
            "+PVR": {
                recording: {
                    value: true,
                    attributes: {
                        ipBroadcast: false,
                        HAS: false,
                        DASH: false,
                        manageRecordings: "initiator",
                        postList: false
                    }
                }
            },
            META: {
                clientMetadata: {
                    value: true,
                    attributes: {
                        type: "eit-pf"
                    }
                }
            },
            "+CONTROLLED": {
                configurationChanges: {
                    value: true
                }
            }
        };
    }



    rearrangeDefaultProperties() {
        var defaultProperties = {};

        var defaultCapabilitiesKeys = Object.keys(this.defaultCapabilities);

        for (var i = 0, l = defaultCapabilitiesKeys.length; i < l; i++) {
            var currentKey = defaultCapabilitiesKeys[i];
            var currentCapability = this.defaultCapabilities[currentKey];
            var currentCapabilityKeysList = Object.keys(currentCapability);

            for (var j = 0, l1 = currentCapabilityKeysList.length; j < l1; j++) {
                var currentCapabilityKeys = currentCapabilityKeysList[j];
                defaultProperties[currentCapabilityKeys] =
                        currentCapability[currentCapabilityKeys];
            }
        }
        return defaultProperties;
    }

    /*
     * FIXME:
     * Yannis - 13/05/2015 - Achieved a more detailed verification on capability
     * but it's not an obligation
     */
    hasCapablity(profileName) {
        this.defaultCapabilities[profileName];
        this.profileNameMapping[profileName];

        return this.defaultCapabilities[profileName] ||
                this.profileNameMapping[profileName];
    }

    searchAnAttributeValue(attributeValue) {

    }

    searchATagName(tagName) {

    }

    searchAValue(value) {

    }
}

