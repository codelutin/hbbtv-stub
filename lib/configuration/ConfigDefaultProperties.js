/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

class ConfigDefaultProperties {

    constructor() {

        this.restrictedPowerState = {
            0: false, // OFF
            6: true // FACTORY_RESET
        };

        this.tvStandardsSupported = {
            1: true,
            2: true,
            4: true,
            8: true,
            16: true
        };

        this.screenSize = {
            "1280x720": true, //HD 720
            "1920x1080": true, //HD 1080
            "640x480": true //VGA
        };

        this.configuration = {
            preferredAudioLanguage: "fra",
            preferredSubtitleLanguage: "fra",
            preferredUILanguage: "fra",
            countryId: "FRA",
            regionId: 0,
            pvrPolicy: configurationConstants.pvrPolicy.WATCHED_RECORDINGS,
            pvrSaveEpisodes: 5,
            pvrSaveDays: 15,
            pvrStartPadding: 0, //Measured in (s)
            pvrEndPadding: 0, //Measured in (s)
            preferredTimeShiftMode: videoBroadcastConstants.timeShiftMode.OFF
        };

        this.startupInformation = {
            urlSource: null,
            url: null
        };

        this.aVOutput = {
            name: null,
            type: null,
            enabled: null,
            subtitleEnabled: null,
            videoMode: null,
            digitalAudioMode: null,
            audioRange: null,
            hdVideoFormat: null,
            tvAspectRatio: null,
            supportedVideoModes: null,
            supportedDigitalAudioModes: null,
            supportedAudioRanges: null,
            supportedHdVideoFormats: null,
            supportedAspectRatios: null,
            current3DMode: null
        };

        this.signalInfo = {
            strength: null,
            quality: null,
            ber: null,
            snr: null,
            lock: null
        };

        this.lNBInfo = {
            lnbType: null,
            lnbLowFreq: null,
            lnbHighFreq: null,
            crossoverFrequency: null,
            lnbStartFrequency: null,
            lnbStopFrequency: null,
            orbitalPosition: null
        };

        this.tuner = {
            id: null,
            enableTuner: true,
            signalInfo: new SignalInfo(this.signalInfo),
            lnbInfo: new LNBInfo(this.lNBInfo),
            frontEndPosition: null,
            powerOnExternal: null
        };

        var idTypes = new IntegerCollection(12, 11);
        var tunerTNT = new Tuner("TNT", idTypes, this.tuner);
        var tuners = new TunerCollection(tunerTNT);

        var masterAVOutput =
        new AVOutput("master", "both", true, this.aVOutput);

        var networkInterface =
        new NetworkInterface("192.168.99.191", "6D:FF:29:E1:AF:33", true, true);

        this.localSystem = {
            deviceID: undefined,
            systemReady: false,
            vendorName: null,
            modelName: null,
            familyName: null,
            softwareVersion: null,
            hardwareVersion: null,
            serialNumber: null,
            releaseVersion: null,
            majorVersion: null,
            minorVersion: null,
            oipfProfile: null,
            pvrEnabled: true, //This property is deprecated.
            ciplusEnabled: false,
            powerState: 0,
            previousPowerState: 0,
            timeCurrentPowerState: new Date().getTime(),
            volume: 0,
            mute: false,
            tuners: tuners,
            outputs: new AVOutputCollection(masterAVOutput),
            networkInterfaces: new NetworkInterfaceCollection(networkInterface),
            tvStandardsSupported: null, //Represented by a bitfield
            tvStandard: configurationConstants.tvStandard.SECAM,
            pvrSupport: 1,
            startupInformation: null
        };

    }
}

