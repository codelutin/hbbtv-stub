/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Used in the channelType property to indicate a TV channel.
var CHANNEL_CONSTANT = {"TYPE_TV": 0};

var VideoBroadcastObject = document.registerElement('video-broadcast', {
    prototype: VideoBroadcastObject.prototype,
    extends: "object"
});

/*
 * Description:
 * Contain the identifier type of channel as key and a boolean as value to indicate if this type of channel
 * can be created.
 */
var CHANNEL_CREATION = {
    0: true, //ID_ANALOG
    10: true, //ID_DVB_C
    11: true, //ID_DVB_S
    12: true, //ID_DVB_T
    13: true, //ID_DVB_SI_DIRECT
    14: true, //ID_DVB_C2
    15: true, //ID_DVB_S2
    16: true, //ID_DVB_T2
    20: true, //ID_ISDB_C
    21: true, //ID_ISDB_S
    22: true, //ID_ISDB_T
    30: true, //ID_ATSC_T
    40: true, //ID_IPTV_SDS
    41: true  //ID_IPTV_URI
};
/*
 * Description:
 * Information necessary to channel creation of identifier type ID_DVB_*.
 * Not a real data, just for simulation but the syntactic structure is based on
 * DVB-SI [EN 300 468] section 6.2.13.
 */
var TERRESTIAL_DELIVERY_SYSTEM_DESCRIPTOR = [
    "0x5A", //descriptor_tag
    "01010000", //descriptor_length
    "0x00000001", //centre_frequency
    "011", //bandwidth
    "1", //priority
    "1", //Time_Slicing_indicator
    "1", //MPE-FEC_indicator
    "N/A", //reserved_future_use
    "00", //constellation
    "000", //hierarchy_information
    "001", //code_rate-HP_stream
    "000", //code_rate-LP_stream
    "00", //guard_interval
    "00", //transmission_mode
    "0", //other_frequency_flag
    "N/A" //reserved_future_use
];

if (!oipfObjectFactory) {
    var oipfObjectFactory = new OipfObjectFactory();
}

if (!Date.now) {
  Date.now = function now() {
    return new Date().getTime();
  };
}

