/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
/*
 * Description: An OITF SHALL support a non-visual embedded object of type “ application/oipfApplicationManager ”, with
 * the following JavaScript API, to enable applications to access the privileged functionality related to application lifecycle
 * and management that is provided by the application model defined in this section.
 * If one of the methods on the application/oipfApplicationManager is called by a webpage that is not a
 * privileged DAE application, the OITF SHALL throw an error as defined in section 10.1.1.
 */
class ApplicationManagerObject {

    addEventListener(type, listener) {
        this._eventManager.addEventListener(type, listener, this);
    }

    removeEventListener(type, listener) {
        this._eventManager.removeEventListener(type, listener, this);
    }

    _getCallback(type) {
        if (this._callbacks) {
            return this._callbacks[type];
        }
    }

    _setCallback(type, callback) {
        if (this._callbacks) {
            this._callbacks[type] = callback;
        }
    }

    /*
     * Description:
     * The function that is called when the OITF is running low on available
     * memory for running DAE applications.
     * The exact criteria determining when to generate such an event is
     * implementation specific.
     */
    get onLowMemory() {
        return this._getCallback("LowMemory");
    }

    set onLowMemory(callback) {
        this._setCallback("LowMemory", callback);
    }

    /*
     * Description:
     * The function that is called immediately prior to a load event being
     * generated in the affected application. The specified function is called
     * with one argument appl , which provides a reference to the affected
     * application.
     */
    get onApplicationLoaded() {
        return this._getCallback("ApplicationLoaded");
    }

    set onApplicationLoaded(callback) {
        this._setCallback("ApplicationLoaded", callback);
    }

    /*
     * Description:
     * The function that is called immediately prior to an unload event being
     * generated in the affected application. The specified function is called
     * with one argument appl , which provides a reference to the affected
     * application.
     */
    get onApplicationUnloaded() {
        return this._getCallback("ApplicationUnloaded");
    }

    set onApplicationUnloaded(callback) {
        this._setCallback("ApplicationUnloaded", callback);
    }

    /*
     * Description:
     * The function that is called when the OITF fails to load either the file
     * containing the initial HTML document of an application or an XML AIT file
     * (e.g. due to an HTTP 404 error, an HTTP timeout, being unable to load the
     * file from a DSM-CC object carousel or due to the file not being either an
     * HTML file or a XML AIT file as appropriate), All properties of
     * the Application object referred to by appl SHALL have the value undefined
     * and calling any methods on that object SHALL fail.
     */
    get onApplicationLoadError() {
        return this._getCallback("ApplicationLoadError");
    }

    set onApplicationLoadError(callback) {
        this._setCallback("ApplicationLoadError", callback);
    }

    /*
     * Description:
     * The callback function that is called during the installation process of
     * a Widget. The function is called with three arguments:
     *
     * WidgetDescriptor wd - the WidgetDescriptor for the installed Widget.
     * Some attributes of this argument may not have been initialised and may be
     * null when the function is called until the Widget is successfully
     * installed.
     *
     * Integer state - the state of the installation; valid values are:
     *  WIDGET_INSTALLATION_STARTED
     *  WIDGET_INSTALLATION_COMPLETED
     *  WIDGET_INSTALLATION_FAILED
     * as defined in section 7.2.1.1.
     *
     * Integer reason: indicates the reason for installation failure.
     * This is only valid if the value of the state argument is
     * WIDGET_INSTALLATION_FAILED otherwise this argument SHALL be null.
     * Valid values for this field are:
     *  WIDGET_ERROR_STORAGE_AREA_FULL
     *  WIDGET_ERROR_DOWNLOAD
     *  WIDGET_ERROR_INVALID_ZIP_ARCHIVE
     *  WIDGET_ERROR_INVALID_SIGNATURE
     *  WIDGET_ERROR_GENERIC
     *  WIDGET_ERROR_SIZE_EXCEEDED
     *  WIDGET_ERROR_PERMISSION_DENIED
     * as defined in section 7.2.1.1.
     */
    get onWidgetInstallation() {
        return this._getCallback("WidgetInstallation");
    }

    set onWidgetInstallation(callback) {
        this._setCallback("WidgetInstallation", callback);
    }

    /*
     * Description:
     * The function that is called during the uninstallation process of
     * a Widget. The function is called with two arguments, defined below:
     *
     * WidgetDescriptor wd - the WidgetDescriptor of the Widget
     * to be uninstalled.
     *
     * Integer state - the state of the installation; valid values are:
     *  WIDGET_UNINSTALLATION_STARTED
     *  WIDGET_UNINSTALLATION_COMPLETED
     *  WIDGET_UNINSTALLATION_FAILED
     * as defined in section 7.2.1.1.
     */
    get onWidgetUninstallation() {
        return this._getCallback("WidgetUninstallation");
    }

    set onWidgetUninstallation(callback) {
        this._setCallback("WidgetUninstallation", callback);
    }

    constructor(applicationVisualizationMode) {
        /*
         * Description:
         * A collection of WidgetDescriptor objects for the Widgets
         * currently installed on the OITF.
         *
         * Visibility type: readonly WidgetDescriptorCollection
         */
        this.widgets = null;

        this._currentApplication = null;
        this._rootApplication = null;
        this._hiddenSystemRootNode = [];
        this._activeApplicationsList = new ApplicationCollection();
        this._visibleApplicationsList = new ApplicationCollection();

        this._applicationVisualizationMode = applicationVisualizationMode;

        this._listeners = {};
        this._callbacks = {};
        this._eventManager = new EventManager();
        this._timerManager = new TimerManager();
        this._timeout = this._timerManager.createTimer.bind(this._timerManager, 0);

        this._createRootApplication();
    }

    _createRootApplication() {
        this._rootApplication = new Application(window, this);
    }

    _fireEvent(type, params) {
        var event = this._eventManager.createCustomEvent(type, params);
        this._eventManager.fireEvent(event, this);
    }

    getApplicationVisualizationMode() {
        return this._applicationVisualizationMode;
    }

    _removeFromActiveList(app) {
        this._activeApplicationsList = this._activeApplicationsList.filter(function(itemList) {
            return itemList.window != app.window;
        });
    }

    _removeFromVisibleList(app) {
        this._visibleApplicationsList = this._visibleApplicationsList.filter(function(itemList) {
            return itemList.window != app.window;
        });
    }

}
