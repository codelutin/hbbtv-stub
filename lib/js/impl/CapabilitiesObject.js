/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
/*
 * The OITF SHALL support following non-visual embedded object with
 * the mime type 'application/oipfCapabilities'.
 */
class CapabilitiesObject {

    constructor(modelFactory) {
        /*
         * Description:
         * Returns the OITF’s capability description as an XML Document object using
         * the syntax as defined in Annex F without using any namespace
         * definitions.
         *
         * Visibility Type: readonly Document
         */
        this.xmlCapabilities = null;

        /*
         * Description:
         * This property holds the number of possible additional decodes for
         * SD video. Depending on the current usage of system resources this value
         * may vary. The value of this property is likely to change if an HD video
         * is started.
         *
         * Adding an A/V Control object or video/broadcast object may still fail,
         * even if extraSDVideoDecodes is larger than 0. For A/V Control objects,
         * in case of failure the play state for the A/V Control object shall be set
         * to 6 ('error') with a detailed error code of 3
         * ('insufficient resources'). For video/broadcast objects, in case of
         * failure the play state of the video/broadcast object shall be set to 0
         * ('unrealized') with a detailed error code of 11
         * ('insufficient resources').
         *
         * Visibility Type: readonly Number
         */
        this.extraSDVideoDecodes = null;

        /*
         * Description:
         * This property holds the number of possible additional decodes for
         * HD video. Depending on the current usage of system resources this value
         * may vary. The value of this property is likely to change if an SD video
         * is started.
         *
         * Adding an A/V Control object or video/broadcast object may still fail,
         * even if extraSDVideoDecodes is larger than 0. For A/V Control objects,
         * in case of failure the play state for the A/V Control object shall be set
         * to 6 ('error') with a detailed error code of 3
         * ('insufficient resources'). For video/broadcast objects, in case of
         * failure the play state of the video/broadcast object shall be set to 0
         * ('unrealized') with a detailed error code of 11
         * ('insufficient resources').
         *
         * Visibility Type: readonly Number
         */
        this.extraHDVideoDecodes = null;

        this.xmlParser = modelFactory.getXmlParser();
        this._defaultCapabilities = modelFactory.getCapabilitiesProperties();
        this.xmlCapabilities = this.xmlParser
                .getXmlDocument(this._defaultCapabilities);
    }

    /*
     * Description:
     * Check if the OITF supports the passed capability.
     * Returns true if the OITF supports the passed capability, false otherwise.
     *
     * Arguments:
     * - profileName:
     * An OIPF base UI profile string or a UI Profile name fragment string as
     * defined in section 9.2.
     * Examples of valid profileName: “ OITF_HD_UIPROF ” or “ +PVR ”.
     *
     * Return: Boolean
     */
    hasCapability(profileName) {
        return this._defaultCapabilities.hasCapablity(profileName);
    }

}
