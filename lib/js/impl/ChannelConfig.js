/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*The ChannelConfig class provides the entry point for applications to get information about the list of channels
 * available. It can be obtained in two ways:
 * -By calling the method getChannelConfig() of the video/broadcast embedded object as defined in
 *  section 7.13.1.3.
 * -By calling the method createChannelConfig() of the object factory API as defined in section 7.1.1.
 * The availability of the properties and methods are dependent on the capabilities description as specified in section 9.3.
 * The following table provides a list of the capabilities and the associated properties and methods. If the capability is false
 * the properties and methods SHALL NOT be available to the application. Properties and methods not listed in the
 * following table SHALL be available to all applications as long as the OITF has indicated support for tuner control (i.e.
 * <video_broadcast>true</video_broadcast> as defined in section 9.3.1) in their capability.
 * ------------------------------------------------------------------------
 * Capability                       | Properties    | Methods
 * --------------------------------- --------------- ----------------------
 * Element <extendedAVControl>      | onChannelScan | startScan()
 * is set to “ true ” as defined in |               | stopScan()
 * section 9.3.6.                   |               |
 * ---------------------------------|---------------|----------------------
 * Element <video_broadcast         |               |createChannelList()
 * type="ID_IPTV_SDS"> is set as    |               |
 * defined in section 9.3.6.        |               |
 */
class ChannelConfig {

    constructor(channelService) {
        this._callbacks = {};
        this._listeners = {};
        this._channelService = channelService;
        this._eventManager = new EventManager();
        this._timerManager = new TimerManager();

        this._timeout = this._timerManager.createTimer.bind(this._timerManager, 0);
        /*
         * Description:
         * The current channel of the OITF if the user has given permission
         * to share this information, possibly through a
         * mechanism outside the scope of this specification.
         * If no channel is being presented, or if this information is
         * not visible to the caller, the value of this property SHALL be null.
         * In an OITF where exactly one video/broadcast object is in any state other than
         * Unrealized and the
         * channel being presented by that video/broadcast object is the only broadcast
         * channel being presented by
         * the OITF then changes to the channel presented by that video/broadcast object
         * SHALL result in changes
         * to the current channel of the OITF.
         * In an OITF which is presenting more than one broadcast channel at the same time,
         * the current channel of
         * the OITF is the channel whose audio is being presented (as defined in the
         * bindToCurrentChannel()
         * method). If that current channel is under the control of a DAE application via a
         * video/broadcast object
         * then changes to the channel presented by that video/broadcast object SHALL
         * result in changes to the
         * current channel of the OITF
         *
         * Visibility Type : readonly Channel
         */
        this.currentChannel = this._channelService && this._channelService.currentChannel || {};

            /*
        * Description:
        * The list of channels.
        * If an OITF includes a platform-specific application that enables the end-user to choose a channel to be
        * presented from a list then all the channels in the list offered to the user by that application SHALL be
        * included in this ChannelList.
        * The list of channels will be a subset of all those available to the OITF. The precise algorithm by which this
        * subset is selected will be market and/or implementation dependent. For example;
        *   •   If an OITF with a DVB-T tuner receives multiple versions of the same channel, one would be
        *       included in the list and the duplicates discarded
        *   •   An OITF with a DVB tuner will often filter services based on service type to discard those which are
        *       obviously inappropriate or impossible for that device to present to the end-user, e.g. firmware
        *       download services.
        * The order of the channels in the list corresponds to the channel ordering as managed by the OITF. SHALL
        * return the value null if the channel list is not (partially) managed by the OITF (i.e., if the channel list
        * information is managed entirely in the network).
        * The properties of channels making up the channel list SHALL be set by the OITF to the appropriate values
        * as determined by the tables in section 8.4.3. The OITF SHALL store all these values as part of the channel
        * list.
        * Some values are set according to the data carried in the broadcast stream. In this case, the OITF MAY set
        * these values to undefined until such time as the relevant data has been received by the OITF, for example
        * after tuning to the channel. Once the data has been received, the OITF SHALL update the properties of the
        * channel in the channel list according to the received data.
        * Note: There is no requirement for the OITF to pro-actively tune to every channel to gather such data.
        *
        * Visibility Type : readonly ChannelList
        */
        this.channelList = this._channelService && this._channelService.channelList || {};

        /* Visibility : readonly FavouriteListCollection
        */
        this.favouriteLists = this._channelService && this._channelService.favouriteLists || {};

        /* Visibility : readonly FavouriteList
        */
        this.currentFavouriteList = this._channelService && this._channelService.currentFavouriteList || {};

    }

    _getCallback(type) {
        if (this._callbacks) {
            return this._callbacks[type];
        }
    }

    _setCallback(type, callback) {
        if (this._callbacks) {
            this._callbacks[type] = callback;
        }
    }

    /*
    * Description:
    * This function is the DOM 0 event handler for events relating to channel list updates. Upon receiving a
    * ChannelListUpdate event, if an application has references to any Channel objects then it SHOULD
    * dispose of them and rebuild its references. Where possible Channel objects are updated rather than
    * removed, but their order in the ChannelConfig.all collection MAY have changed. Any lists created with
    * ChannelConfig.createFilteredList() SHOULD be recreated in case channels have been removed.
    */
    get onChannelListUpdate() {
        return this._getCallback("ChannelListUpdate");
    }

    set onChannelListUpdate(callback) {
        this._setCallback("ChannelListUpdate", callback);
    }

    /*
    * \param Integer scanEvent
    * \param Interger progress
    * \param Interger frequency
    * \param Interger signalStrength
    * \param Interger channelNumber
    * \param Interger channelType
    * \param Interger channelCount
    * \param Interger transponderCount
    * \param Channel newChannel
    */
    get onChannelScan() {
        return this._getCallback("ChannelScan");
    }

    set onChannelScan(callback) {
        this._setCallback("ChannelScan", callback);
    }

    addEventListener(type, listener) {
        this._eventManager.addEventListener(type, listener, this);
    }

    _fireEvent(event) {
        this._eventManager.fireEvent(event, this);
    }

    _fireEventChannelListUpdate() {
        this._fireEvent(this._eventManager.createCustomEvent("ChannelListUpdate", []));
    }

    removeEventListener(type, listener) {
        this._eventManager.removeEventListener(type, listener, this);
    }
}
