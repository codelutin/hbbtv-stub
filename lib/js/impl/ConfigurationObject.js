/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * Description:
 * The OITF SHALL implement the “ application/oipfConfiguration ” object as defined below. This object
 * provides an interface to the configuration and user settings facilities within the OITF.
 */
class ConfigurationObject {

    constructor() {
        /*
        * Description:
        * Accesses the configuration object that sets defaults and shows system settings.
        *
        * Visibility Type: readonly Configuration
        */
        this.configuration = null;

        /*
        * Description:
        * Accesses the object representing the platform hardware.
        *
        * Visibility Type: readonly LocalSystem
        */
        this.localSystem = null;

        this._listeners = {};
        this._callbacks = {};
        var configDefaultProperties = new ConfigDefaultProperties();
        this._eventManager = new EventManager();
        this.localSystem = new LocalSystem(configDefaultProperties);
        this.configuration = new Configuration(configDefaultProperties.configuration);
    }

    _getCallback(type) {
        if (this._callbacks) {
            return this._callbacks[type];
        }
    }

    _setCallback(type, callback) {
        if (this._callbacks) {
            this._callbacks[type] = callback;
        }
    }

   /*
    * Description:
    * The function that is called when the IP address of a network interface has changed. The specified function
    * is called with two arguments (NetworkInterface)item and (String)ipAddress. The ipAddress may have the value undefined if a previously assigned address has been         * lost.
    *
    */
    get onIpAddressChange() {
        return this._getCallback("IpAddressChange");
    }

    set onIpAddressChange(callback) {
        this._setCallback("IpAddressChange", callback);
    }

    addEventListener(type, listener) {
        this._eventManager.addEventListener(type, listener, this);
    }

    _fireEvent(event) {
        this._eventManager.fireEvent(event, this);
    }

    removeEventListener(type, listener) {
        this._eventManager.removeEventListener(type, listener, this);
    }

}
