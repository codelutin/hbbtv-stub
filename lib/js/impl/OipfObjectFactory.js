/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*!
 * \class OipfObjectFactory
 * \brief Provide access to components of a terminal supporting OIFP specification.
 */
class OipfObjectFactory {

    constructor() {
        console.log("[INFO] constructor of OipfObjectFactory class called.");
        this._setChannelServicePromise();
        var modelFactory = new ModelFactory();
        var dateUtils = new DateUtils();
        this._oipfConfiguration = new OipfConfiguration();
        this._channelService = new ChannelService();
        this._setInitPromise();

            /*The ChannelConfig class provides the entry point for applications to get information about the list of channels
         * available. It can be obtained in two ways:
         * -By calling the method getChannelConfig() of the video/broadcast embedded object as defined in
         *  section 7.13.1.3.
         * -By calling the method createChannelConfig() of the object factory API as defined in section 7.1.1.
         * The availability of the properties and methods are dependent on the capabilities description as specified in section 9.3.
         * The following table provides a list of the capabilities and the associated properties and methods. If the capability is false
         * the properties and methods SHALL NOT be available to the application. Properties and methods not listed in the
         * following table SHALL be available to all applications as long as the OITF has indicated support for tuner control (i.e.
         * <video_broadcast>true</video_broadcast> as defined in section 9.3.1) in their capability.
         * ------------------------------------------------------------------------
         * Capability                       | Properties    | Methods
         * --------------------------------- --------------- ----------------------
         * Element <extendedAVControl>      | onChannelScan | startScan()
         * is set to “ true ” as defined in |               | stopScan()
         * section 9.3.6.                   |               |
         * --------------------------------- --------------- ----------------------
         * Element <video_broadcast         |               |createChannelList()
         * type="ID_IPTV_SDS"> is set as    |               |
         * defined in section 9.3.6.        |               |
         * ------------------------------------------------------------------------
         */
        this.channelConfig = new ChannelConfig();

       /*
        * Description: Access to the functionality of the application/oipfRemoteManagement embedded object SHALL adhere to the
        * security requirements as defined in section 10.
        */
        this.remoteManagementObject = null;


       /*
        * Description: OITFs SHALL implement the application/oipfSearchManager embedded object. This object provides a
        * mechanism for applications to create and manage metadata searches.
        */
        this.searchManagerObject = new SearchManagerObject(new ProgrammeService());

               /*
        * Description: An OITF SHALL support a non-visual embedded object of type “ application/oipfApplicationManager ”, with
        * the following JavaScript API, to enable applications to access the privileged functionality related to application lifecycle
        * and management that is provided by the application model defined in this section.
        * If one of the methods on the application/oipfApplicationManager is called by a webpage that is not a
        * privileged DAE application, the OITF SHALL throw an error as defined in section 10.1.1.
        */
        this.applicationManagerObject =
            new ApplicationManagerObject(
                this._oipfConfiguration.applicationVisualizationMode);
        this.capabilitiesObject = new CapabilitiesObject(modelFactory);
        //this.codManagerObject = new CodManagerObject();
        //this.imsObject = new IMSObecjet();
        //this.mdtfObject = new MDTFObject();
        //this.notifSocketObject = new NotifSocket();
        this.parentalControlManagerObject = new ParentalControlManagerObject();
        this.configurationObject = new ConfigurationObject();
        this.recordingSchedulerObject = new RecordingSchedulerObject(
            this.configurationObject.configuration, modelFactory, dateUtils);
        //this.downloadManagerObject= new DownloadManagerObject();
        //this.downloadTriggerObject= new DownloadTriggerObject();
        //this.drmAgentObject= new DrmAgentObject();
        //this.gatewayInfoObject= new GatewayInfoObject();
        //this.remoteManagementObject = new RemoteManagementObject();
        //this.remoteControlFunctionObject = new RemoteControlFunctionObject();
    }

    _setInitPromise() {
        var self = this;

        var programmesUrl = this._oipfConfiguration.programmes;

        this._initPromise = this._createDocument(programmesUrl)
            .then(this._setChannelConfig.bind(this))
            .then(this._setSearchManagerObject.bind(this));
    }

    _getInitPromise() {
        return this._initPromise;
    }

    _createDocument(filename) {
        var self = this;

        var promise = new Promise(function(resolve, reject) {
            if (window.XMLHttpRequest) {
                var xmlHttpRequest = new XMLHttpRequest();

                xmlHttpRequest.open("GET", filename);

                xmlHttpRequest.send();

                xmlHttpRequest.onload = function() {

                    if (this.status == 200 && this.responseXML != null) {
                        var xmlToJson = new XmlToJson(this.responseXML);

                        self._jsonProgrammesAndChannels = xmlToJson.getJson();

                        if (self._jsonProgrammesAndChannels) {
                            console.log("Channels and programmes data have been received.")
                        }

                        console.log("A new doument has been created.");

                        resolve();

                    } else {
                        console.log("An error occured during the document creation.");

                        reject();
                    }
                };
            }
        });

        return promise;

    }

    /*
     * Description:
     * This method SHALL return true if and only if an object of the specified type is supported by
     * the OITF, otherwise it SHALL return false.
     *
     * Argument:
     * - mimeType : If the value of the argument is one of the MIME types defined in tables 1
     * to 4 of [OIPF_MEDIA2] or one of the DAE defined mime types listed
     * below then an accurate indication of the OITF’s support for that MIME
     * type SHALL be returned. For other values, it is recommended that an OITF returns a value which
     * accurately reflects its support for the specified MIME type.
     * DAE MIME Type:
     * application/notifsocket, application/oipfApplicationManager,
     * application/oipfCapabilities, application/oipfCodManager,
     * application/oipfCommunicationServices, application/oipfConfiguration,
     * application/oipfDownloadManager, application/oipfDownloadTrigger,
     * application/oipfDrmAgent, application/oipfGatewayInfo,
     * application/oipfCommunicationServices, application/oipfMDTF,
     * application/oipfParentalControlManager, application/oipfRecordingScheduler,
     * application/oipfRemoteControlFunction, application/oipfRemoteManagement
     *
     */
    isObjectSupported(mimeType) {

        var mimeType_method = {
            channelConfig: true,
            "application/notifsocket": false,
            "application/oipfApplicationManager": true,
            "application/oipfCapabilities": true,
            "application/oipfCodManager": false,
            "application/oipfCommunicationServices": false,
            "application/oipfConfiguration": true,
            "application/oipfDownloadManager": false,
            "application/oipfDownloadTrigger": false,
            "application/oipfDrmAgent": false,
            "application/oipfGatewayInfo": false,
            "application/oipfMDTF": false,
            "application/oipfParentalControlManager": true,
            "application/oipfRecordingScheduler": true,
            "application/oipfRemoteControlFunction": false,
            "application/oipfRemoteManagement": false,
            "application/oipfSearchManager": true,
            "application/oipfStatusView": false,
            "video/broadcast": true,
            "video/mpeg": false
        };

        return mimeType_method[mimeType];
    }

   /*!
    * Description:
    * If the object type is supported, each of these methods shall return an instance of the
    * corresponding embedded object.
    * Since objects do not claim scarce resources when they are instantiated, instantiation shall
    * never fail if the object type is supported. If the method name to create the object is not
    * supported, the OITF SHALL throw an error with the error.name set to the value
    * " TypeError ".
    * If the object type is supported, the method shall return an HTMLObjectElement equivalent
    * to the specified object. The value of the type attribute of the HTMLObjectElement SHALL
    * match the mimetype of the instantiated object, for example " video/broadcast " in case of
    * method oipfObjectFactory.createVideoBroadcastObject() .
    *
    * Arguments:
    * -requiredCapabilities :
    * An optional argument indicating the formats to be supported by
    * the resulting player. Each item in the argument SHALL be one of
    * the formats specified in [OIPF_MEDIA2]. Scarce resources will
    * be claimed by the object at the time of instantiation. The
    * allocationMethod property SHALL be set
    * STATIC_ALLOCATION . If the OITF is unable to create the player
    * object with the requested capabilities, the method SHALL return
    * null .
    * If this argument is omitted, objects do not claim scarce resources
    * so instantiation shall never fail if the object type is supported. The
    * allocationMethod property SHALL be set to DYNAMIC_ALLOCATION .
    */
    createVideoBroadcastObject() {

        console.log("[INFO] createVideoBroadcastObject() of OipfObjectFactory class called.");
        if (this.isObjectSupported("video/broadcast")) {
            return new VideoBroadcastObject();
        } else {
            throw new TypeError("This object type is not supported.");
        }

    }

   /*
    * Description
    * If the object type is supported, each of these methods SHALL return an instance of the
    * corresponding embedded object. This may be a new instance or existing instance. For
    * example, the object will likely be a global singleton object and calls to this method may
    * return the same instance.
    * Since objects do not claim scarce resources when they are instantiated, instantiation
    * SHALL never fail if the object type is supported. If the method name to create the object is
    * not supported, the OITF SHALL throw an error with the name property set to the value
    * " TypeError ".
    * If the object is supported, the method SHALL return a JavaScript Object which
    * implements the interface for the specified object.
    */
    createSearchManagerObject() {
        if (this.isObjectSupported(SEARCH_MANAGER_DAE_MIME_TYPE)) {
            console.log("[INFO] createSearchManagerObject() of OipfObjectFactory class called.");
            return this.searchManagerObject;
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

   /*
    * Description
    * If the object type is supported, each of these methods SHALL return an instance of the
    * corresponding embedded object. This may be a new instance or existing instance. For
    * example, the object will likely be a global singleton object and calls to this method may
    * return the same instance.
    * Since objects do not claim scarce resources when they are instantiated, instantiation
    * SHALL never fail if the object type is supported. If the method name to create the object is
    * not supported, the OITF SHALL throw an error with the name property set to the value
    * " TypeError ".
    * If the object is supported, the method SHALL return a JavaScript Object which
    * implements the interface for the specified object.
    */
    createApplicationManagerObject() {
        if (this.isObjectSupported(APPLICATION_MANAGER_DAE_MIME_TYPE)) {
                console.log("[INFO] createApplicationManagerObject() of OipfObjectFactory class called.");
                return this.applicationManagerObject;
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

   /*
    * Description
    * If the object type is supported, each of these methods SHALL return an instance of the
    * corresponding embedded object. This may be a new instance or existing instance. For
    * example, the object will likely be a global singleton object and calls to this method may
    * return the same instance.
    * Since objects do not claim scarce resources when they are instantiated, instantiation
    * SHALL never fail if the object type is supported. If the method name to create the object is
    * not supported, the OITF SHALL throw an error with the name property set to the value
    * " TypeError ".
    * If the object is supported, the method SHALL return a JavaScript Object which
    * implements the interface for the specified object.
    */
    createCapabilitiesObject() {
        if (this.isObjectSupported(CAPABILITIES_DAE_MIME_TYPE)) {
            console.log("[INFO] createCapabilitiesObject() of OipfObjectFactory class called.");
            return this.capabilitiesObject;
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

    /*
    * Description
    * If the object type is supported, each of these methods SHALL return an instance of the
    * corresponding embedded object. This may be a new instance or existing instance. For
    * example, the object will likely be a global singleton object and calls to this method may
    * return the same instance.
    * Since objects do not claim scarce resources when they are instantiated, instantiation
    * SHALL never fail if the object type is supported. If the method name to create the object is
    * not supported, the OITF SHALL throw an error with the name property set to the value
    * " TypeError ".
    * If the object is supported, the method SHALL return a JavaScript Object which
    * implements the interface for the specified object.
    */
    createChannelConfig() {
        if (this.isObjectSupported(CHANNEL_CONFIG_TYPE)) {
            console.log("[INFO] createChannelConfigObject() of OipfObjectFactory class called.");
            return this.channelConfig;
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

    /*
    * Description
    * If the object type is supported, each of these methods SHALL return an instance of the
    * corresponding embedded object. This may be a new instance or existing instance. For
    * example, the object will likely be a global singleton object and calls to this method may
    * return the same instance.
    * Since objects do not claim scarce resources when they are instantiated, instantiation
    * SHALL never fail if the object type is supported. If the method name to create the object is
    * not supported, the OITF SHALL throw an error with the name property set to the value
    * " TypeError ".
    * If the object is supported, the method SHALL return a JavaScript Object which
    * implements the interface for the specified object.
    */
    createCodManagerObject() {
        if (this.isObjectSupported(COD_MANAGER_DAE_MIME_TYPE)) {
            console.log("[INFO] createCodManagerObject() of OipfObjectFactory class called.");
            return this.codManagerObject;
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

    /*
    * Description
    * If the object type is supported, each of these methods SHALL return an instance of the
    * corresponding embedded object. This may be a new instance or existing instance. For
    * example, the object will likely be a global singleton object and calls to this method may
    * return the same instance.
    * Since objects do not claim scarce resources when they are instantiated, instantiation
    * SHALL never fail if the object type is supported. If the method name to create the object is
    * not supported, the OITF SHALL throw an error with the name property set to the value
    * " TypeError ".
    * If the object is supported, the method SHALL return a JavaScript Object which
    * implements the interface for the specified object.
    */
    createConfigurationObject() {
        if (this.isObjectSupported(CONFIGURATION_DAE_MIME_TYPE)) {
            console.log("[INFO] createConfigurationObject() of OipfObjectFactory class called.");
            return this.configurationObject;
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

    /*
    * Description
    * If the object type is supported, each of these methods SHALL return an instance of the
    * corresponding embedded object. This may be a new instance or existing instance. For
    * example, the object will likely be a global singleton object and calls to this method may
    * return the same instance.
    * Since objects do not claim scarce resources when they are instantiated, instantiation
    * SHALL never fail if the object type is supported. If the method name to create the object is
    * not supported, the OITF SHALL throw an error with the name property set to the value
    * " TypeError ".
    * If the object is supported, the method SHALL return a JavaScript Object which
    * implements the interface for the specified object.
    */
    createDownloadManagerObject() {
        if (this.isObjectSupported(DOWNLOAD_MANAGER_DAE_MIME_TYPE)) {
            console.log("[INFO] createDownloadManagerObject() of OipfObjectFactory class called.");
            return this.downloadManagerObject;
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

    /*
    * Description
    * If the object type is supported, each of these methods SHALL return an instance of the
    * corresponding embedded object. This may be a new instance or existing instance. For
    * example, the object will likely be a global singleton object and calls to this method may
    * return the same instance.
    * Since objects do not claim scarce resources when they are instantiated, instantiation
    * SHALL never fail if the object type is supported. If the method name to create the object is
    * not supported, the OITF SHALL throw an error with the name property set to the value
    * " TypeError ".
    * If the object is supported, the method SHALL return a JavaScript Object which
    * implements the interface for the specified object.
    */
    createDownloadTriggerObject() {
        if (this.isObjectSupported(DOWNLOAD_TRIGGER_DAE_MIME_TYPE)) {
            console.log("[INFO] createDownloadTriggerObject() of OipfObjectFactory class called.");
            return this.downloadTriggerObject;
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

    /*
    * Description
    * If the object type is supported, each of these methods SHALL return an instance of the
    * corresponding embedded object. This may be a new instance or existing instance. For
    * example, the object will likely be a global singleton object and calls to this method may
    * return the same instance.
    * Since objects do not claim scarce resources when they are instantiated, instantiation
    * SHALL never fail if the object type is supported. If the method name to create the object is
    * not supported, the OITF SHALL throw an error with the name property set to the value
    * " TypeError ".
    * If the object is supported, the method SHALL return a JavaScript Object which
    * implements the interface for the specified object.
    */
    createDrmAgentObject() {
        if (this.isObjectSupported(DRM_AGENT_DAE_MIME_TYPE)) {
            console.log("[INFO] createDrmAgentObject() of OipfObjectFactory class called.");
            return this.drmAgentObject;
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

    /*
    * Description
    * If the object type is supported, each of these methods SHALL return an instance of the
    * corresponding embedded object. This may be a new instance or existing instance. For
    * example, the object will likely be a global singleton object and calls to this method may
    * return the same instance.
    * Since objects do not claim scarce resources when they are instantiated, instantiation
    * SHALL never fail if the object type is supported. If the method name to create the object is
    * not supported, the OITF SHALL throw an error with the name property set to the value
    * " TypeError ".
    * If the object is supported, the method SHALL return a JavaScript Object which
    * implements the interface for the specified object.
    */
    createGatewayInfoObject() {
        if (this.isObjectSupported(GATEWAY_INFO_DAE_MIME_TYPE)) {
            console.log("[INFO] createGatewayInfoObject() of OipfObjectFactory class called.");
            return this.gatewayInfoObject;
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

    /*
    * Description
    * If the object type is supported, each of these methods SHALL return an instance of the
    * corresponding embedded object. This may be a new instance or existing instance. For
    * example, the object will likely be a global singleton object and calls to this method may
    * return the same instance.
    * Since objects do not claim scarce resources when they are instantiated, instantiation
    * SHALL never fail if the object type is supported. If the method name to create the object is
    * not supported, the OITF SHALL throw an error with the name property set to the value
    * " TypeError ".
    * If the object is supported, the method SHALL return a JavaScript Object which
    * implements the interface for the specified object.
    */
    createIMSObject() {
        if (this.isObjectSupported(COMMUNICATION_SERVICES_DAE_MIME_TYPE)) {
            console.log("[INFO] createIMSObject() of OipfObjectFactory class called.");
            return this.imsObject;
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

    /*
    * Description
    * If the object type is supported, each of these methods SHALL return an instance of the
    * corresponding embedded object. This may be a new instance or existing instance. For
    * example, the object will likely be a global singleton object and calls to this method may
    * return the same instance.
    * Since objects do not claim scarce resources when they are instantiated, instantiation
    * SHALL never fail if the object type is supported. If the method name to create the object is
    * not supported, the OITF SHALL throw an error with the name property set to the value
    * " TypeError ".
    * If the object is supported, the method SHALL return a JavaScript Object which
    * implements the interface for the specified object.
    */
    createMDTFObject() {
        if (this.isObjectSupported(MDTF_DAE_MIME_TYPE)) {
            console.log("[INFO] createMDTFObject() of OipfObjectFactory class called.");
            return this.mdtfObject;
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

    /*
    * Description
    * If the object type is supported, each of these methods SHALL return an instance of the
    * corresponding embedded object. This may be a new instance or existing instance. For
    * example, the object will likely be a global singleton object and calls to this method may
    * return the same instance.
    * Since objects do not claim scarce resources when they are instantiated, instantiation
    * SHALL never fail if the object type is supported. If the method name to create the object is
    * not supported, the OITF SHALL throw an error with the name property set to the value
    * " TypeError ".
    * If the object is supported, the method SHALL return a JavaScript Object which
    * implements the interface for the specified object.
    */
    createNotifSocketObject() {
        if (this.isObjectSupported(NOTIF_SOCKET_DAE_MIME_TYPE)) {
            console.log("[INFO] createNotifSocketObject() of OipfObjectFactory class called.");
            return this.notifSocketObject;
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

    /*
    * Description
    * If the object type is supported, each of these methods SHALL return an instance of the
    * corresponding embedded object. This may be a new instance or existing instance. For
    * example, the object will likely be a global singleton object and calls to this method may
    * return the same instance.
    * Since objects do not claim scarce resources when they are instantiated, instantiation
    * SHALL never fail if the object type is supported. If the method name to create the object is
    * not supported, the OITF SHALL throw an error with the name property set to the value
    * " TypeError ".
    * If the object is supported, the method SHALL return a JavaScript Object which
    * implements the interface for the specified object.
    */
    createParentalControlManagerObject() {
        if (this.isObjectSupported(PARENTAL_CONTROL_MANAGER_DAE_MIME_TYPE)) {
            console.log("[INFO] createParentalControlManangerObject() of OipfObjectFactory class called.");
            return this.parentalControlManagerObject;
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

    /*
    * Description
    * If the object type is supported, each of these methods SHALL return an instance of the
    * corresponding embedded object. This may be a new instance or existing instance. For
    * example, the object will likely be a global singleton object and calls to this method may
    * return the same instance.
    * Since objects do not claim scarce resources when they are instantiated, instantiation
    * SHALL never fail if the object type is supported. If the method name to create the object is
    * not supported, the OITF SHALL throw an error with the name property set to the value
    * " TypeError ".
    * If the object is supported, the method SHALL return a JavaScript Object which
    * implements the interface for the specified object.
    */
    createRecordingSchedulerObject() {
        if (this.isObjectSupported(RECORDING_SCHEDULER_DAE_MIME_TYPE)) {
            console.log("[INFO] createRecordingSchedulerObject() of OipfObjectFactory class called.");
            return this.recordingSchedulerObject;
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

    /*
    * Description
    * If the object type is supported, each of these methods SHALL return an instance of the
    * corresponding embedded object. This may be a new instance or existing instance. For
    * example, the object will likely be a global singleton object and calls to this method may
    * return the same instance.
    * Since objects do not claim scarce resources when they are instantiated, instantiation
    * SHALL never fail if the object type is supported. If the method name to create the object is
    * not supported, the OITF SHALL throw an error with the name property set to the value
    * " TypeError ".
    * If the object is supported, the method SHALL return a JavaScript Object which
    * implements the interface for the specified object.
    */
    createRemoteControlFunctionObject() {
        if (this.isObjectSupported(REMOTE_CONTROL_FUNCTION_DAE_MIME_TYPE)) {
            console.log("[INFO] createRemoteControlFunctionObject() of OipfObjectFactory class called.");
            return this.remoteControlFunctionObject;
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

    /*
    * Description
    * If the object type is supported, each of these methods SHALL return an instance of the
    * corresponding embedded object. This may be a new instance or existing instance. For
    * example, the object will likely be a global singleton object and calls to this method may
    * return the same instance.
    * Since objects do not claim scarce resources when they are instantiated, instantiation
    * SHALL never fail if the object type is supported. If the method name to create the object is
    * not supported, the OITF SHALL throw an error with the name property set to the value
    * " TypeError ".
    * If the object is supported, the method SHALL return a JavaScript Object which
    * implements the interface for the specified object.
    */
    createRemoteManagementObject() {
        if (this.isObjectSupported(REMOTE_MANAGEMENT_DAE_MIME_TYPE)) {
            console.log("[INFO] createRemoteManagementObject() of OipfObjectFactory class called.");
            return this.remoteManagementObject;
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

    /*!
    * Description:
    * If the object type is supported, each of these methods shall return an instance of the
    * corresponding embedded object.
    * Since objects do not claim scarce resources when they are instantiated, instantiation shall
    * never fail if the object type is supported. If the method name to create the object is not
    * supported, the OITF SHALL throw an error with the error.name set to the value
    * " TypeError ".
    * If the object type is supported, the method shall return an HTMLObjectElement equivalent
    * to the specified object. The value of the type attribute of the HTMLObjectElement SHALL
    * match the mimetype of the instantiated object, for example " video/broadcast " in case of
    * method oipfObjectFactory.createVideoBroadcastObject() .
    *
    * Arguments:
    * -requiredCapabilities :
    * An optional argument indicating the formats to be supported by
    * the resulting player. Each item in the argument SHALL be one of
    * the formats specified in [OIPF_MEDIA2]. Scarce resources will
    * be claimed by the object at the time of instantiation. The
    * allocationMethod property SHALL be set
    * STATIC_ALLOCATION . If the OITF is unable to create the player
    * object with the requested capabilities, the method SHALL return
    * null .
    * If this argument is omitted, objects do not claim scarce resources
    * so instantiation shall never fail if the object type is supported. The
    * allocationMethod property SHALL be set to DYNAMIC_ALLOCATION .
    */
    createStatusViewObject() {
        if (this.isObjectSupported(STATUS_VIEW_DAE_MIME_TYPE)) {
            console.log("[INFO] createStatusViewObject() of OipfObjectFactory class called.");
            return new StatusViewObject();
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

    /*!
    * Description:
    * If the object type is supported, each of these methods shall return an instance of the
    * corresponding embedded object.
    * Since objects do not claim scarce resources when they are instantiated, instantiation shall
    * never fail if the object type is supported. If the method name to create the object is not
    * supported, the OITF SHALL throw an error with the error.name set to the value
    * " TypeError ".
    * If the object type is supported, the method shall return an HTMLObjectElement equivalent
    * to the specified object. The value of the type attribute of the HTMLObjectElement SHALL
    * match the mimetype of the instantiated object, for example " video/broadcast " in case of
    * method oipfObjectFactory.createVideoBroadcastObject() .
    *
    * Arguments:
    * -requiredCapabilities :
    * An optional argument indicating the formats to be supported by
    * the resulting player. Each item in the argument SHALL be one of
    * the formats specified in [OIPF_MEDIA2]. Scarce resources will
    * be claimed by the object at the time of instantiation. The
    * allocationMethod property SHALL be set
    * STATIC_ALLOCATION . If the OITF is unable to create the player
    * object with the requested capabilities, the method SHALL return
    * null .
    * If this argument is omitted, objects do not claim scarce resources
    * so instantiation shall never fail if the object type is supported. The
    * allocationMethod property SHALL be set to DYNAMIC_ALLOCATION .
    */
    createVideoMpegObject() {
        if (this.isObjectSupported("video/mpeg")) {
            console.log("[INFO] createVideoMpegObject() of OipfObjectFactory class called.");
            return new VideoMpegObject();
        } else {
            throw new TypeError("This object type is not supported.");
        }
    }

    _setChannelConfig() {
        var channelHelper = new ChannelHelper(this._jsonProgrammesAndChannels.tv.channel);
        var oipfChannels = channelHelper.getOipfChannels();

        this._channelService =
                new ChannelService(this._oipfConfiguration.currentChannel,
                                    this._oipfConfiguration.channelNumber,
                                    this._oipfConfiguration.channelOrigin,
                                    oipfChannels
                );
        this._channelServicePromise.resolve(this._channelService);

        this.channelConfig = new ChannelConfig(this._channelService);
        this.channelConfig._fireEventChannelListUpdate();
    }

    _setSearchManagerObject() {
        var somesChannelInfo = this._jsonProgrammesAndChannels.tv.channel;

        var oipfProgrammes = new ProgrammeHelper(this._channelService, somesChannelInfo)
            .initProgrammes(this._jsonProgrammesAndChannels.tv.programme);

        this._programmeService = new ProgrammeService(oipfProgrammes);

        this.searchManagerObject._setProgrammeService(this._programmeService);
        this.searchManagerObject._setMetadata(this._programmeService);

        var self = this;

        this.searchManagerObject._initPromise = this._initPromise
            .then(function() {
                self.searchManagerObject._programmeService = self._programmeService;
            });
    }

    _getChannelServicePromise() {

        return this._channelServicePromise;
    }

    _setChannelServicePromise() {
        this._channelServicePromise = {
            promise: null,
            resolve: null,
            reject: null
        };

        var self = this;

        this._channelServicePromise.promise = new Promise(function(resolve, reject) {
            self._channelServicePromise.resolve = resolve;
            self._channelServicePromise.reject = reject;
        });
    }
}
