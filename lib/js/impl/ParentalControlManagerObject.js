/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
class ParentalControlManagerObject {

    /*
     * FIXME:
     * -Yannis 18/05/2015 The properties "_currentPIN" and "_invalidPINAttemps"
     * don't have to be innitialized here.
     */
    constructor() {

        /*
         * Descrioption:
         * A reference to the collection of rating schemes known by the OITF.
         *
         * Visibilty Type:readonly ParentalRatingSchemeCollection
         */
        this.parentalRatingSchemes = null;

        /*
         * Description:
         * The lockout status of the parental control PIN. If the incorrect PIN has
         * been entered too many times in the configured timeout period, parental
         * control PIN entry SHALL be locked out for a period of time determined by
         * the OITF.
         *
         * Visibility Type: readonly Boolean
         */
        this.isPINEntryLocked = null;

        //The parental control PIN set.
        this._currentPIN = null;

        //Indicate the number of invalid PIN attemps.
        this._invalidPINAttemptsMax = null;

        //Contain the number of invalid entry PIN attempts.
        this._currentInvalidPINAttempts = null;

        //When the PIN entry attempt has been reached this PIN entry must be locked.
        this._PINLockedTime = null;

        /*
         * Let to manage the locking of programmes and/or channels  which are
         * initially unrated.
         */
        this._blockUnrated = null;

        /*
         * Represent a status indicating if a temporarily authorization for
         * the comsumption of any blocked content have been set or not.
         *
         */
        this._parentalControlStatus = null;

        this._timerManager = new TimerManager();
        this._timeout = this._timerManager.createTimer.bind(this._timerManager, 0);

        this.isPINEntryLocked = false;
        this._blockUnrated = false;
        this._parentalControlStatus = false;

        this._currentPIN = "1234";
        this._invalidPINAttemptsMax = 4;
        this._currentInvalidPINAttempts = 0;
        this._PINLockedTime = 2000;

        this._PIN_CORRECT = parentalControlManagerConstants.pin.CORRECT;
        this._PIN_INCORRECT = parentalControlManagerConstants.pin.INCORRECT;
        this._PIN_LOCKED = parentalControlManagerConstants.pin.LOCKED;
    }

    /*
     * As defined in [OIPF_CSP2], the OITF shall prevent the consumption of a
     * programme when its parental rating doesn't meet the parental rating
     * criterion currently defined in the OITF. Calling this method with enable
     * set to false will temporarily allow the consumption of any blocked
     * programme. Setting the parental control status using this method SHALL
     * set the status until the consumption of any of all the blocked programmes
     * terminates (e.g. until the content item being played is changed),
     * or another call to the setParentalControlStatus() method is made.
     * Setting the parental control status using this method has the following
     * effect; for the Programme and Channel objects as defined in sections
     * 7.16.2 and 7.13.11, the blocked property of a programme or channel SHALL
     * be set to true for programmes whose parental rating does not meet the
     * applicable parental rating criterion, but the locked property SHALL be
     * set to false .This operation to temporarily disable parental rating
     * control SHALL be protected by the parental control PIN (i.e. through the
     * pcPIN argument). The return value indicates the success of the operation,
     * and SHALL take one of the following values:
     * ------------------------------------------------------------------------
     * Value |                     Description
     * ------ -----------------------------------------------------------------
     * 0     |   The PIN is correct.
     * ------ -----------------------------------------------------------------
     * 1     |   The PIN is incorrect.
     * ------ -----------------------------------------------------------------
     * 2     |  PIN entry is locked because an invalid PIN has been entered too
     *       |  many times. The number of invalid PIN attempts before PIN entry
     *       |  is locked is outside the scope of this specification.
     * ------ -----------------------------------------------------------------
     *
     * Arguments:
     * - pcPIN: The parental control PIN.
     *
     * - enable: Flag indicating whether parental control should be enabled.
     *
     * Return: Integer
     *
     * FIXME:
     * - Yannis 19/05/2015 The unblocking of programme should be done here or
     * outside of this method, if the second case is true,
     * how notify the programme? Who have to achieve this notification?
     */
    setParentalControlStatus(pcPIN, enable) {
         if (this._currentPIN == pcPIN) {
            this._parentalControlStatus = enable;
            return this._PIN_CORRECT;
        }
        this._currentInvalidPINAttempts++;

        if (this._currentInvalidPINAttempts > this._invalidPINAttemptsMax) {
            this._lockPINEntry();
            return this._PIN_LOCKED;
        }

        return this._PIN_INCORRECT;
    }

    /*
     * Description:
     * Set the parental control PIN. This operation SHALL be protected by the
     * parental control PIN (if PIN entry is enabled). The return value
     * indicates the success of the operation, and SHALL take one of the
     * following values:
     * ------------------------------------------------------------------------
     * Value |                     Description
     * ------ -----------------------------------------------------------------
     * 0     |   The PIN is correct.
     * ------ -----------------------------------------------------------------
     * 1     |   The PIN is incorrect.
     * ------ -----------------------------------------------------------------
     * 2     |  PIN entry is locked because an invalid PIN has been entered too
     *       |  many times. The number of invalid PIN attempts before PIN entry
     *       |  is locked is outside the scope of this specification.
     * ------ -----------------------------------------------------------------
     *
     * Arguments:
     * - oldPcPIN: The current parental control PIN.
     * - newPcPIN: The new value for the parental control PIN.
     *
     * Return: Integer
     */
    setParentalControlPIN(oldPcPIN, newPcPIN) {

        if (this._currentPIN == oldPcPIN) {
            this._currentPIN = newPcPIN;
            return this._PIN_CORRECT;
        }
        this._currentInvalidPINAttempts++;

        if (this._currentInvalidPINAttempts > this._invalidPINAttemptsMax) {
            this._lockPINEntry();
            return this._PIN_LOCKED;
        }

        return this._PIN_INCORRECT;

    }

    /*
     * Description:
     * Unlock the object specified by target for viewing if pcPIN contains the
     * correct parental control PIN.
     *
     * The object type of target can be one of the following:
     * -video/broadcast object, in which case the content being
     * presented through this object SHALL be unlocked until a new channel is
     * selected.
     * -A/V Control object, in which case the content being presented
     * through this object. SHALL be unlocked until a new item of content is
     * played using this object.
     *
     * Otherwise an Invalid Object error SHALL be returned.
     *
     * The return value indicates the success of the operation, and
     * SHALL take the following values:
     *
     * ------------------------------------------------------------------------
     * Value |                     Description
     * ------ -----------------------------------------------------------------
     * 0     |   The PIN is correct.
     * ------ -----------------------------------------------------------------
     * 1     |   The PIN is incorrect.
     * ------ -----------------------------------------------------------------
     * 2     |  PIN entry is locked because an invalid PIN has been entered too
     *       |  many times. The number of invalid PIN attempts before PIN entry
     *       |  is locked is outside the scope of this specification.
     * ------ -----------------------------------------------------------------
     *
     * Arguments:
     * - pcPIN: The parental control PIN.
     *
     * - target: The object to be unlocked.
     */
    unlockWithParentalControlPIN(pcPIN, target) {

    }

    verifyParentalControlPIN(pcPIN) {
        if (this._currentPIN == pcPIN) {
            return this._PIN_CORRECT;
        }
        this._currentInvalidPINAttempts++;

        if (this._currentInvalidPINAttempts > this._invalidPINAttemptsMax) {
            this._lockPINEntry();
            return this._PIN_LOCKED;
        }

        return this._PIN_INCORRECT;

    }

    /*
     * Description:
     * Set whether programmes for which no parental rating has been retrieved
     * from the metadata client nor defined by the service provider should be
     * blocked automatically by the terminal.
     *
     * This operation SHALL be protected
     * by the parental control PIN (if PIN entry is enabled).The return value
     * indicates the success of the operation, and SHALL take one of
     * the following values:
     * ------------------------------------------------------------------------
     * Value |                     Description
     * ------ -----------------------------------------------------------------
     * 0     |   The PIN is correct.
     * ------ -----------------------------------------------------------------
     * 1     |   The PIN is incorrect.
     * ------ -----------------------------------------------------------------
     * 2     |  PIN entry is locked because an invalid PIN has been entered too
     *       |  many times. The number of invalid PIN attempts before PIN entry
     *       |  is locked is outside the scope of this specification.
     * ------ -----------------------------------------------------------------
     *
     * Arguments:
     * - pcPIN: The parental control PIN.
     *
     * - block: Flag indicating whether programmes SHALL be blocked.
     *
     */
    setBlockUnrated(pcPIN, block) {
        if (this._currentPIN == pcPIN) {
            this._blockUnrated = block;
            return this._PIN_CORRECT;
        }
        this._currentInvalidPINAttempts++;

        if (this._currentInvalidPINAttempts > this._invalidPINAttemptsMax) {
            this._lockPINEntry();
            return this._PIN_LOCKED;
        }

        return this._PIN_INCORRECT;
    }

    /*
     * Description:
     * Returns a flag indicating whether or not the OITF has been configured by
     * the user to block content for which a parental rating is absent.
     */
    getBlockUnrated() {
        return this._blockUnrated;
    }

    /*
     * Description:
     * Returns a flag indicating the temporary parental control status set by
     * setParentalControlStatus() . Note that the returned status covers
     * parental control functionality related to all rating schemes, not only
     * the rating scheme upon which the method is called.
     */
    getParentalControlStatus() {
        return this._parentalControlStatus;
    }

    _lockPINEntry() {
        this.isPINEntryLocked = true;

        //After a periode of time, the PIN entry will be unlocked.
        this._timeout(this._PINLockedTime)
                .then(this._setIsPINEntryLocked.bind(this, false));
    }

   _setIsPINEntryLocked(enable) {
       this.isPINEntryLocked = enable;
       this._currentInvalidPINAttempts = 0;
   }

    _setCurrentInvalidPINAttempts(value) {
        this._currentInvalidPINAttempts = value;
    }

}
