/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
class RecordingSchedulerObject {

    /*
     * FIXME:
     * Yannis - 29/014/2015 - Use of a utils class in future to manage 'Date'
     * object.
     */
    constructor(subConfiguration, modelFactory, dateUtils) {
        /*
         * Description:
         * Provides a list of scheduled and recorded programmes in the system.
         * This property may only provide access to a subset of the full list
         * of recordings, as determined by the value of the manageRecordings
         * attribute of the <recording> element in the client capability
         * description (see section 9.3.3).
         *
         * Visibility Type: readonly ScheduledRecordingCollection
         */
        this.recordings = null;

        /*
         * Description:
         * Get information about the status of the local storage device.
         * The DiscInfo class is defined in section 7.16.4.
         *
         * Visibility Type: readonly ScheduledRecordingCollection
         */
        this.discInfo = null;

        this._dateUtils = dateUtils;
        this._modelFactory = modelFactory;
        this.SCHEDULED = recordingConstants.state.SCHEDULED;
        this.STARTED = recordingConstants.state.STARTED;
        this.COMPLETED = recordingConstants.state.COMPLETED;
        this.REMOVED = recordingConstants.state.REMOVED;
        this.UPDATED = recordingConstants.state.UPDATED;
        this.BEFORE_STARTED = recordingConstants.state.BEFORE_STARTED;
        this._subConfiguration = subConfiguration;
        this._eventManager = new EventManager();
        this._timerManager = new TimerManager();
        this._timeout = this._timerManager.createTimer.bind(this._timerManager);
        this._timerManagerRecording = new TimerRecordingManager();
        this._timeoutRecording = this._timerManagerRecording
            .createTimer.bind(this._timerManagerRecording);
        this._callbacks = {};
        this._listeners = {};
        /*
         * Description:
         * Contain the ojects of type Recording in-progress and completed.
         * Actually only one recording will be authorized.
         */
        this._recordings = {};
        this.recordings = new ScheduledRecordingCollection();
        this.discInfo = null;
    }

    /*
     * Description:
     *
     * FIXME:
     * Yannis - 27/04/2015 - Do verification on parameter
     * Yannis - 27/04/2015 - Verify pvr capability in order to manage
     * recordings to store them into properties "recordings".
     * Yannis - 27/04/2015 - Verify parental control settings about recording
     * right.
     *
     */
    record() {
        var parameters = arguments;
        if (parameters.length == 1) {
            var scheduledRecording = new ScheduledRecording(parameters[0]);
        } else if (parameters.length == 4) {
            scheduledRecording = new ScheduledRecording(parameters[0],
                parameters[1],
                parameters[2],
                parameters[3]);
        } else {
            return null;
        }

        scheduledRecording.startPadding = this._subConfiguration.pvrStartPadding;
        scheduledRecording.endPadding = this._subConfiguration.pvrEndPadding;

        if (this._recordingConflict(scheduledRecording)) {
            return null;
        }

        this._fireEventScheduled(scheduledRecording);

        var recording = this._createRecording(scheduledRecording);
        this._recordings[recording.id] = recording;
        this.recordings.push(recording);

        this._planifyStartRecording(recording)
            .then(this._planifyEndRecording.bind(this, recording));

        return recording;
    }

    /*
     * Description:
     *
     * FIXME:
     * Yannis - 27/04/2015 - Do verification on parameters.
     * Yannis - 27/04/2015 - Verify pvr capability in order to know
     * if this properties "recordings" can be used.
     * Yannis - 27/04/2015 - Verify parental control settings about recording
     * right.
     */
    recordAt(startTime, duration, repeatDays, channelID) {
        this.record(startTime, duration, repeatDays, channelID);
    }

    /*
     *
     * @param {ScheduledRecording} recording
     * @returns {undefined}
     */
    remove(recording) {
        if (recording) {
            this._timerManagerRecording.clearTimer(recording.id);
            this.recordings.splice(this.recordings.indexOf(recording), 1);
            delete this._recordings[recording.id];

            this._timeoutRecording()
                .then(this._fireEventRemoved.bind(this, recording));

            return;
        }
        console.log("The recording can't be remove.");
    }

    /*
     * Description:
     * Returns a subset of all the recordings that are scheduled but which have
     * not yet started. The subset SHALL include only scheduled recordings that
     * were scheduled using a service from the same FQDN as the domain of the
     * service that calls the method.
     *
     * @returns { ScheduledRecordingCollection}
     */
    getScheduledRecordings() {
        if (this.recordings) {
            return this.recordings;
        }
    }

    /*
     * Description:
     * Returns the channel line-up of the OITF in the form of a ChannelConfig
     * object as defined in section 7.13.9. The ChannelConfig object returned
     * from this function SHALL be identical to the ChannelConfig object
     * returned from the getChannelConfig() method on the video/broadcast object
     * as defined in section 7.13.1.3.
     *
     * @returns {ChannelConfig}
     */
    getChannelConfig() {
        return oipfObjectFactory.createChannelConfig();
    }

    createProgrammeObject() {

    }

    /*
     * Description:
     * Returns the Recording object for which the value of the Recording.id
     * property corresponds to the given id parameter. If such a Recording does
     * not exist, the method returns null.
     *
     * Arguments:
     * - id: Identifier corresponding to the id property of a Recording object.
     *
     * @returns {Recording}
     */
    getRecording(id) {
        return this._recordings[id] || null;
    }

    /*
     * Description:
     * Stop an in-progress recording. The recording SHALL NOT be deleted.
     *
     * Arguments:
     * - recording: The recording to be stopped.
     *
     * @returns {undefined}
     */
    stop(recording) {
        console.log("Method stop called");
        if (recording && recording.state == this.STARTED) {
            this._timerManagerRecording.clearTimer(recording.id);
            return;
        }
        console.log("The recording can't be stop.");
    }

    /*
     * Description:
     * Update the recordings property to show the current status of all
     * recordings.
     *
     * @returns {undefined}
     */
    refresh() {

    }

    /*
     * Description:
     * For scheduled recordings the properties startTime, duration and
     * repeatDays can be modified. For ongoing recordings only the duration
     * property may be modified. This method SHALL return true if the operation
     * succeeded, or false if for any reason it rescheduling is not possible
     * (e.g. the updated recording overlaps with another scheduled recording and
     * there are insufficient system resources to do both). If the method
     * returns false then no changes SHALL be made to the recording.
     *
     * Arguments:
     * - id: The id of the recording to update.
     * - startTime: The new start time of the recording, or undefined if the
     *   start time is not to be updated.
     * - duration: The new duration of the recording, or undefined if the
     *   duration is not to be updated.
     * - repeatDays: The new set of days on which the recording is to be
     *   repeated, or undefined if this is not to be updated.
     *
     * @returns {undefined}
     */
    update(id, startTime, duration, repeatDays) {
        var recording = this._recordings[id];

        if (!recording) {
            return false;
        }

        if (recording.state == this.STARTED) {
            if (duration) {
                return this._updateDuration(id, duration);
            }

        } else if (recording.state == this.SCHEDULED) {
            this._updateFields(id, startTime, duration, repeatDays);
        }

        return false;
    }

    get onPVREvent() {
        return this._getCallback("PVREvent");
    }

    set onPVREvent(callback) {
        this._setCallback("PVREvent", callback);
    }

    _getCallback(type) {
        if (this._callbacks) {
            return this._callbacks[type];
        }
    }

    _setCallback(type, callback) {
        if (this._callbacks) {
            this._callbacks[type] = callback;
        }
    }

    addEventListener(type, listener) {
        this._eventManager.addEventListener(type, listener, this);
    }

    _fireEvent(state, recording) {
        var event = this._eventManager.createCustomEvent("PVREvent", [state, recording]);
        this._eventManager.fireEvent(event, this);
    }

    _fireEventScheduled(recording) {
        this._fireEvent(this.SCHEDULED, recording);
    }

    _fireEventBeforeStarted(recording) {
        this._fireEvent(this.BEFORE_STARTED, recording);
    }

    _fireEventStarted(recording) {
        this._fireEvent(this.STARTED, recording);
    }

    _fireEventCompleted(recording) {
        this._fireEvent(this.COMPLETED, recording);
    }

    _fireEventUpdated(recording) {
        this._fireEvent(this.UPDATED, recording);
    }

    _fireEventRemoved(recording) {
        this._fireEvent(this.REMOVED, recording);
    }

    removeEventListener(type, listener) {
        this._eventManager.removeEventListener(type, listener, this);
    }

    _recordingConflict(recording) {

        if (this._isProgrammeFinished(recording)) {
            return true;
        }

        for (var i = 0, l = this.recordings.length; i < l; i++) {

            var startPadding = this.recordings[i].startPadding;
            var endPadding = this.recordings[i].endPadding;
            var currentStartTime = this.recordings[i].startTime + startPadding;
            var currentDuration = this.recordings[i].duration + endPadding;
            var currentStopTime = currentStartTime + currentDuration;

            var recordingStartTime = recording.startTime + recording.startPadding;
            var recordingStopTime = recordingStartTime + recording.duration
                + recording.endPadding;

            /*
             * First, verify that two recordings won't schedules at same time.
             * Second, verify that two recordings don't overlap.
             */
            if (currentStartTime == recordingStartTime ||
                    (recordingStartTime >= currentStartTime &&
                        recordingStartTime <= currentStopTime) ||
                    (recordingStopTime >= currentStartTime &&
                        recordingStopTime <= currentStopTime)) {

                return true;
            }
        }
        return false;
    }

    _createRecording(recording) {
        return new Recording(recording);
    }

    /*
     * FIXME:
     * Yannis - 29/04/2015 - Management of negative padding.
     * Yannis - 29/04/2015 - Modify recordingStartTime of concerned recording.
     */
    _planifyStartRecording(recording) {
        if (this._isProgrammeStarted(recording)) {

            var timer = this._dispatchBeforeStartedEventAndStartedEvent(null, recording);
            console.log("The recording start now because the programme already had started.");

        } else {

            var timeToStart = this._getDurationFromNowToSpecificTime(recording.startTime +
                recording.startPadding);

            console.log("The recording will start in",
                this._dateUtils.secondsToMinutes(timeToStart), "minutes.");

            this._dispatchBeforeStartedEventAndStartedEvent(
                    this._dateUtils.secondsToMilliseconds(timeToStart),
                    recording);

        }

        this._timerManagerRecording
            .recordingTimers[recording.id] = {
                "start": timer,
                "end": null
            };

        return timer;
    }

    /*
     * FIXME:
     * Yannis - 29/04/2015 - Managing of negative padding.
     * Yannis - 29/04/2015 - Modify recordingEndTime of concerned recording.
     */
    _planifyEndRecording(recording) {
        var timeToEnd = this._getDurationFromNowToSpecificTime(recording.startTime +
            recording.duration + recording.endPadding);

        var timer = this._timeoutRecording(this._dateUtils.secondsToMilliseconds(timeToEnd))
                .then(recording._setState.bind(recording, this.COMPLETED))
                .then(this._fireEventCompleted.bind(this, recording));

        console.log("The recording will end in",
            this._dateUtils.secondsToMinutes(timeToEnd),
            "minutes after its starting.");

        this._timerManagerRecording
            .recordingTimers[recording.id].end = timer;

        return timer;
    }

    _isProgrammeStarted(recording) {
        var startTime = recording.startTime;
        var endTime = startTime + recording.duration;

        var currentTime = this._dateUtils.nowInSeconds();

        return currentTime >= startTime;
    }

    /*
     * Description :
     * Avoid schedule recording whose the programme already has been broadcasted.
     *
     * Return: Boolean
     */
    _isProgrammeFinished(recording) {
        var startTime = recording.startTime;
        var endTime = startTime + recording.duration;

        var currentTime = this._dateUtils.nowInSeconds();
        console.log(currentTime);
        return endTime <= currentTime;
    }

    _getDurationFromNowToSpecificTime(time) {
        return time - this._dateUtils.nowInSeconds();
    }

    _updateDuration(id, duration) {
        console.log("Try updating duration");
        var recording = this._recordings[id];

        if (!recording) {
            return false;
        }

//        var newStopTime = recording.startTime + duration + recording.endPadding;
        var newStopTime = this._dateUtils.nowInSeconds() + duration + recording.endPadding;

        if (newStopTime <= this._dateUtils.nowInSeconds()) {
            return false;
        }

        var recordings = this.recordings.filter(function(recording) {
            return recording.id != id;
        });


        for (var i = 0, l = recordings.length; i < l; i++) {
            var startPadding = recordings[i].startPadding;
            var endPadding = recordings[i].endPadding;
            var currentStartTime = recordings[i].startTime + startPadding;
            var currentDuration = recordings[i].duration + endPadding;
            var currentStopTime = currentStartTime + currentDuration;

            /*
             * First, verify that two recordings won't schedules at same time.
             * Second, verify that two recordings don't overlap.
             */
            if (newStopTime >= currentStartTime) {

                return false;
            }
        }
        recording._setDuration(duration);

        this._timerManagerRecording.clearTimer(recording.id);

        this._planifyEndRecording(recording);

        return true;

    }

    _updateFields(id, startTime, duration, repeatDays) {
        console.log("Try updating duration");
        var recording = this._recordings[id];

        if (!recording || (!startTime && !duration && !repeatDays)) {
            return false;
        }

        if (startTime) {
            var recordingStartTime = startTime + recording.startPadding;
            var recordingStopTime = recordingStartTime + recording.duration
                + recording.endPadding;
        }

        if (duration && startTime) {
            var recordingStopTime = recordingStartTime + duration
                + recording.endPadding;
        } else if (duration && !startTime) {
            var recordingStopTime = recording.startTime + recording.duration
                + recording.endPadding;
        }

        if (recordingStartTime <= this._dateUtils.nowInSeconds() ||
                recordingStopTime <= this._dateUtils.nowInSeconds()) {
            return false;
        }

        var recordings = this.recordings.filter(function(recording) {
            return recording.id != id;
        });

        for (var i = 0, l = recordings.length; i < l; i++) {
            var startPadding = recordings[i].startPadding;
            var endPadding = recordings[i].endPadding;
            var currentStartTime = recordings[i].startTime + startPadding;
            var currentDuration = recordings[i].duration + endPadding;
            var currentStopTime = currentStartTime + currentDuration;

            /*
             * First, verify that two recordings won't schedules at same time.
             * Second, verify that two recordings don't overlap.
             */
            if (recordingStartTime == currentStartTime ||
                    (recordingStartTime >= currentStartTime &&
                        recordingStartTime <= currentStopTime) ||
                    (recordingStopTime >= currentStartTime &&
                        recordingStopTime <= currentStopTime)) {

                return false;
            }
        }

        repeatDays && recording._setRepeatDays(repeatDays);

        if (startTime) {
            this._timerManagerRecording.clearTimer(recording.id);
            startTime && recording._setStartTime(startTime);

            this._planifyStartRecording(recording)
                .then(this._planifyEndRecording.bind(this, recording));

            this._timeoutRecording()
                .then(this._fireEventUpdated.bind(this, recording));

            return true;

        }

        if (duration) {
            this._timerManagerRecording.clearTimer(recording.id);
            duration && recording._setDuration(duration);

            this._planifyEndRecording(recording);

            this._timeoutRecording()
                .then(this._fireEventUpdated.bind(this, recording));

            return true;

        }

        return false;
    }

    _dispatchBeforeStartedEventAndStartedEvent(timer, recording) {
        return this._timeoutRecording(timer)
                    .then(recording._setState.bind(recording,
                        this.BEFORE_STARTED))
                    .then(this._fireEventBeforeStarted.bind(this, recording))
                    .then(recording._setState.bind(recording,
                        this.STARTED))
                    .then(this._fireEventStarted.bind(this, recording));
    }

}
