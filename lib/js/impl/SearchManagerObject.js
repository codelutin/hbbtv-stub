/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * Description: OITFs SHALL implement the application/oipfSearchManager embedded object. This object provides a
 * mechanism for applications to create and manage metadata searches.
 */
class SearchManagerObject {

    constructor(programmeService) {
        this._setProgrammeService(programmeService);
        this._listeners = {};
        this._callbacks = {};
        this._eventManager = new EventManager();
        this._setMetadata(this._programmeService);
    }

    _setProgrammeService(programmeService) {
        if (programmeService) {
            this._programmeService = programmeService;
        }
    }

    _setMetadata(programmeService) {
        if (programmeService.programmes) {
            this._metadata = this._programmeService.programmes;
        }
    }

    /*
    * Description:
    * This function is the DOM 0 event handler for events indicating changes in metadata. This SHALL be raised
    * under the following circumstances:
    * 1) When a new version of the metadata is discovered. Note that new versions of metadata can be made
    * available without any of the individual items of metadata changing. It is an application's responsibility to
    * determine what, if anything, has changed.
    *
    * 2) When the values of the blocked or locked properties on a content item change due to changes in the
    * parental control subsystem (e.g. parental control being enabled or disabled, or a content item being
    * unlocked with a PIN).
    *
    *
    * The specified function is called with the arguments action, info and object. These arguments are defined as follows:
    *
    *   • Integer action – the type of update that has taken place. This field will take one of the following values:
    * ---------------------------------------------------------------------------------------------------------------------------------------------------------------
    * Value |                                                                  Description
    * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
    * 1     |   A new version of metadata is available (see section 4.1.2.1.2 of [OIPF_META2]) and applications SHOULD discard all references to
    *       |   Programme objects immediately and re-acquire them.
    * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
    * 2     |   A change to the parental control flags for a content item has occurred (e.g. the user has unlocked the parental control features of the receiver,
    *       |   allowing a blocked item to be played).
    * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
    * 3     |   A flag affecting the filtering criteria of a channel has changed. Applications MAY listen for events with this action code to update lists of favourite
    *       |   channels, for instance.
    * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
    *
    *   • Integer info – extended information about the type of update that has taken place.
    *     If the action argument is set to the value 3, the value of this field SHALL be one or more of the following:
    * -----------------------------------------------------------------
    * Value | Description
    * ------ ----------------------------------------------------------
    * 1     | The list of blocked channels has changed.
    * ------ ----------------------------------------------------------
    * 2     | A list of favourite channels has changed.
    * ------ ----------------------------------------------------------
    * 4     | The list of hidden channels has changed.
    * -----------------------------------------------------------------
    * If the action argument is set to the value 2, the value of this field SHALL be one or more of:
    * -----------------------------------------------------------------
    * Value | Description
    * ------ ----------------------------------------------------------
    * 1     | The block status of a content item has changed.
    * ------ ----------------------------------------------------------
    * 2     | The lock status of a content item has changed.
    * -----------------------------------------------------------------
    * This field is treated as a bitfield, so values MAY be combined to allow multiple reasons to be passed.
    *
    *   • Object object – the affected channel, programme, or CoD asset prior to the change. If more than
    *     one is affected, then this argument SHALL take the value null.
    */
    get onMetadataUpdate() {
        return this._getCallback("MetadataUpdate");
    }

    set onMetadataUpdate(callback) {
        this._setCallback("MetadataUpdate", callback);
    }

    /*
    * Description:
    * This function is the DOM 0 event handler for events relating to metadata searches. The specified function is called with the arguments search and state.
    * These arguments are defined as follows:
    * • MetadataSearch search – the affected search
    *
    * • Integer state – the new state of the search
    * ---------------------------------------------------------------------------------------------------------------------------------------------------------------
    * Value |                                                                  Description
    * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
    * 0     |   Search has finished. This event SHALL be generated when a search has completed.
    * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
    * 1     |   This value is not used.
    * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
    * 2     |   This value is not used.
    * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
    * 3     |   The MetadataSearch object has returned to the idle state, either because of a call to SearchResults.abort() or because the parameters for the search
    *       |   have been modified (e.g. the query, constraints or search target).
    * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
    * 4     |   The search cannot be completed due to a lack of resources or any other reason (e.g. insufficient memory is available to cache all of the requested
    *       |   results).
    * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
    */
    get onMetadataSearch() {
        return this._getCallback("MetadataSearch");
    }

    set onMetadataSearch(callback) {
        this._setCallback("MetadataSearch", callback);
    }

    _getCallback(type) {
        if (this._callbacks) {
            return this._callbacks[type];
        }
    }

    _setCallback(type, callback) {
        if (this._callbacks) {
            this._callbacks[type] = callback;
        }
    }


    /*
    * Description :
    * Create a MetadataSearch object that can be used to search the metadata.
    *
    * Argument:
    * -searchTarget: The metadata that should be searched.
    * Valid values of the searchTarget parameter are:
    * -----------------------------------------------------------------
    * Value | Description
    * ------ ----------------------------------------------------------
    * 1     | Metadata relating to scheduled content SHALL be searched.
    * ------ ----------------------------------------------------------
    * 2     | Metadata relating to on-demand content SHALL be searched.
    * -----------------------------------------------------------------
    *
    * These values are treated as a bitfield, allowing searches to be carried out across multiple search targets.
    *
    * Return MetadataSearch
    */
    createSearch(searchTarget) {

        if (searchTarget === 2 || searchTarget === 1) {
            return new MetadataSearch(searchTarget, this);
        }
    }

    /*
    * Description :
    * Create a MetadataSearch object that can be used to search the metadata.
    * Returns the channel line-up of the tuner in the form of a ChannelConfig object as defined
    * in section 7.13.9. This includes the favourite lists.
    * The ChannelConfig object returned from this function SHALL be identical to the
    * ChannelConfig object returned from the getChannelConfig() method on the
    * video/broadcast object as defined in section 7.13.1.3.
    *
    * Return MetadataSearch
    */
    getChannelConfig() {
        return oipfObjectFactory.createChannelConfig();
    }

    addEventListener(type, listener) {
        this._eventManager.addEventListener(type, listener, this);
    }

    _fireEvent(state, search) {
        var event =
            this._eventManager.createCustomEvent("MetadataSearch", [search, state]);
        this._eventManager.fireEvent(event, this);
    }

    _fireEventFinished(search) {
        this._fireEvent(metadataSearchConstants.state.FINISHED, search);
    }

    _fireEventModified(search) {
        this._fireEvent(metadataSearchConstants.state.MODIFIED, search);
    }

    _fireEventUncompleted(search) {
        this._fireEvent(metadataSearchConstants.state.UNCOMPLETED, search);
    }

    removeEventListener(type, listener) {
        this._eventManager.removeEventListener(type, listener, this);
    }

}
