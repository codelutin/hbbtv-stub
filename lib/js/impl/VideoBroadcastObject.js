/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * Description:
 * If the object type is supported, this method shall return an instance of the
 * corresponding embedded object.
 * Since objects do not claim scarce resources when they are instantiated, instantiation shall
 * never fail if the object type is supported. If the method name to create the object is not
 * supported, the OITF SHALL throw an error with the error.name set to the value
 * " TypeError ".
 * If the object type is supported, the method shall return an HTMLObjectElement equivalent
 * to the specified object. The value of the type attribute of the HTMLObjectElement SHALL
 * match the mimetype of the instantiated object, for example " video/broadcast " in case of
 * method oipfObjectFactory.createVideoBroadcastObject() .
 *
 * Arguments:
 * -requiredCapabilities :
 * An optional argument indicating the formats to be supported by
 * the resulting player. Each item in the argument SHALL be one of
 * the formats specified in [OIPF_MEDIA2]. Scarce resources will
 * be claimed by the object at the time of instantiation. The
 * allocationMethod property SHALL be set
 * STATIC_ALLOCATION . If the OITF is unable to create the player
 * object with the requested capabilities, the method SHALL return
 * null .
 * If this argument is omitted, objects do not claim scarce resources
 * so instantiation shall never fail if the object type is supported. The
 * allocationMethod property SHALL be set to DYNAMIC_ALLOCATION .
 */
class VideoBroadcastObject {

    constructor() {
        this.init();
    }

    init() {
        console.log("[INFO] constructor of VideoBroadcast class called.");

        this._UNREALIZED = 0;

        this._CONNECTING = 1;

        this._PRESENTING = 2;

        this._STOPPED = 3;

            /*
         * Description:
         * The width of the area used for rendering the video object. This property is only writable if property
         * fullScreen has value false . Changing the width property corresponds to changing the width property
         * through the HTMLObjectElement interface, and must have the same effect as changing the width through
         * the DOM Level 2 Style interfaces (i.e. CSS2Properties interface style.width ), at least for values
         * specified in pixels.
         */
        this.width = "100%";
        /*
         * Description:
         * The height of the area used for rendering the video object. This property is only writable if property
         * fullScreen has value false . Changing the height property corresponds to changing the height property
         * through the HTMLObjectElement interface, and must have the same effect as changing the height through
         * the DOM Level 2 Style interfaces (i.e. CSS2Properties interface style.height ), at least for values
         * specified in pixels.
         */
        this.height = "100%";

        /*
         * Description:
         * Returns true if this video object is in full-screen mode, false otherwise. The default value is false .
         * Visibilité Type: readonly Boolean
         */
        this.fullScreen = null;

        /*
         * Description:
         * Setting the value of the data property SHALL have no effect on the video/broadcast object. If this property
         * is read, the value returned SHALL always be the empty string.
         * Type: String
         */
        this.data = "";

        /*!
         * Description:
         * Visibility Type: readonly Integer
         * The current play state of the video/broadcast object. Valid values are:
         * ---------------------------------------------------------------------------------------------------------------------------------------------------------------
         * Value |                                                                  Description
         * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
         * 0     |   unrealized; the application has not made a request to start presenting a channel or has stopped presenting a channel and released any resources. The
         *       |   content of the video/broadcast object should be transparent but if not shall be an opaque black rectangle. Control of media presentation is under the       *       |   control of the OITF, as defined in section H.2.
         * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
         * 1     |   connecting; the terminal is connecting to the media source in order to begin playback. Objects in this state may be buffering data in order to start
         *       |   playback. Control of media the control of the OITF, as defined in section H.2. The content of the video/broadcast object is implementation dependent.
         * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
         * 2     |   presenting; the media is currently being presented to the user. The object is in this state regardless of whether the media is playing at normal speed,
         *       |   paused, or playing in a trick mode (e.g. at a speed other than normal speed). Control of media presentation is under the control of the application,
         *       |   as defined in section H.2. The video/broadcast object contains the video being presented.
         * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
         * 3     |   stopped; the terminal is not presenting media, either inside the video/broadcast object or in the logical video plane. The logical video plane is
         *       |   disabled. The content of the video/broadcast object SHALL be an opaque black rectangle. Control of media presentation is under the control of the
         *       |   application, as defined in section H.2
         * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
         * See section 7.13.1.1 for a description of the state model for a video/broadcast object.
         * NOTE: Implementations where the content of the video/broadcast object is transparent in the unrealized state will give a better user experience than
         * ones where it is black. This happens for an application with video in the background between when it includes a video/broadcast object in the page and when a
         * call to bindToCurrentChannel() completes. Applications which do not need to call bindToCurrentChannel() should not do so. The current channel can
         * be obtained from the currentChannel property on the ApplicationPrivateData object which is the same as that on the video/broadcast object under most normal           * conditions.
         */
        this.playState = null;

        /*
         * Description:
         * The list of media formats that are supported by the object. Each item SHALL contain a format label according
         * to [OIPF_MEDIA2].
         * If scarce resources are not claimed by the object, the value of this property SHALL be null.
         *
         * Visibilité Type: readonly StringCollection
         */
        this.playerCapabilities = null;

        /*
         * Description:
         * Returns the resource allocation method currently in use by the object. Valid values as defined in section 7.14.13.1 are:
         * to [OIPF_MEDIA2].
         * • STATIC_ALLOCATION
         * • DYNAMIC_ALLOCATION
         *
         * Visibilité Type: readonly Integer
         */
        this.allocationMethod = null;

        /*
         * Description:
         * The channel currently being presented by this embedded object if the user has given permission to share this
         * information, possibly through a mechanism outside the scope of this specification. If no channel is being
         * presented, or if this information is not visible to the caller, the value of this property SHALL be null.
         * The value of this property is not affected during timeshift operations and SHALL reflect the value prior to the
         * start of a timeshift operation, for both local and network timeshift resources.
         *
         * Extensions to video/broadcast for current channel information:
         * If an OITF has indicated support for extended tuner control (i.e. by giving value true to element
         * <extendedAVControl> as specified in section 9.3.6 in its capability description), the OITF SHALL support the
         * following additional properties and methods on the video/broadcast object.
         * The functionality as described in this section is subject to the security model of section 10.1.3.8.
         * Note the property onChannelScan and methods startScan and stopScan have been moved to section 7.13.9.
         *
         * Visibility Type: readonly Channel
         */
        this.currentChannel = null;

        this.playState = 0;
        this._listeners = {};
        this._callbacks = {};

        this._channelServicePromise = oipfObjectFactory._getChannelServicePromise();
        this._channelServicePromise.promise.then(this._setChannelService.bind(this));
        this._setChannelService();

        this._localSystem = oipfObjectFactory.createConfigurationObject().localSystem;

        this._eventManager = new EventManager();
        this._timerManager = new TimerManager();

        this._timeout = this._timerManager.createTimer.bind(this._timerManager, 0);
        this._storage = new Storage();
    }

    createdCallback() {
        console.log("[INFO] createdCallback called.");
        this.init();

        // If multiple video/bcast lives in different windows they should be kept in sync
        var changePlayState = this.changePlayState.bind(this);
        this._storage.addListener("playState", function(event) {
            changePlayState(parseInt(event.newValue));
        })
    }

    _setChannelService() {
        this._channelService = oipfObjectFactory._channelService;
    }


    /*!
     * Description:
     * The function that is called when a request to switch a tuner to another channel resulted in an error preventing
     * the broadcasted content from being rendered. The specified function is called with the arguments channel
     * and errorState . This function may be called either in response to a channel change initiated by the
     * application, or a channel change initiated by the OITF (see section 7.13.1.1).
     *
     * These arguments are defined as follows:
     *
     * • Channel channel:
     *  the Channel object to which a channel switch was requested, but for which the
     *  error occurred. This object SHALL have the same properties as the channel that was requested,
     *  except that for channels of type ID_DVB_* the values for the onid and tsid properties SHALL be
     *  extracted from the transport stream when one was found (e.g. when errorState is 12).
     *
     * • Number errorState: error code detailing the type of error:
     * ---------------------------------------------------------------------------------------------------------------------------------------------------------------
     * Value |                                                                  Description
     * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
     * 0     |  channel not supported by tuner.
     * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
     * 1     |  cannot tune to given transport stream (e.g. no signal)
     * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
     * 2     |  tuner locked by other object.
     * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
     * 3     |  parental lock on channel.
     * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
     * 4     |  encrypted channel, key/module missing.
     * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
     * 5     |  unknown channel (e.g. can’t resolve DVB or ISDB triplet).
     * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
     * 6     |  channel switch interrupted (e.g. because another channel switch was activated before the previous one completed).
     * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
     * 7     |  channel cannot be changed, because it is currently being recorded.
     * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
     * 8     |  cannot resolve URI of referenced IP channel.
     * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
     * 9     |  insufficient bandwidth.
     * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
     * 10    |  channel cannot be changed by nextChannel()/prevChannel() methods either because the OITF does not maintain a favourites or channel list or because the
     *       |  video/broadcast object is in the Unrealized state.
     * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
     * 11    |  insufficient resources are available to present the given channel (e.g. a lack of available codec resources).
     * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
     * 12    |  specified channel not found in transport stream.
     * ------ --------------------------------------------------------------------------------------------------------------------------------------------------------
     * 100   |  unidentified error.
     * ---------------------------------------------------------------------------------------------------------------------------------------------------------------
     */
    get onChannelChangeError() {
        return this._getCallback("ChannelChangeError");
    }

    set onChannelChangeError(callback) {
        this._setCallback("ChannelChangeError", callback);
    }

    /*!
     * Description:
     * The function that is called when the play state of the video/broadcast object changes. This function may
     * be called either in response to an action initiated by the application, an action initiated by the OITF or an
     * error (see section 7.13.1.1).
     *
     * The specified function is called with the arguments state and error . These arguments are defined as follows:
     *
     * • Number state:
     *  the new state of the video/broadcast object. Valid values are given in the definition of the playState property above.
     *
     * • Number error – if the state has changed due to an error, this field contains an error code detailing the type of error.
     * See the definition of onChannelChangeError above for valid values. If no error has occurred, this argument SHALL take the value undefined .
     */
    get onPlayStateChange() {
        return this._getCallback("PlayStateChange");
    }

    set onPlayStateChange(callback) {
        this._setCallback("PlayStateChange", callback);
    }

    /*
     * Description:
     * The function that is called when a request to switch a tuner to another channel has successfully completed.
     * This function may be called either in response to a channel change initiated by the application, or a channel
     * change initiated by the OITF (see section 7.13.1.1). The specified function is called with argument channel,
     * which is defined as follows:
     *
     * • Channel channel – the channel to which the tuner switched. This object SHALL have the same properties with the same values as the currentChannel object (see       *    section 7.13.7).
     */
    get onChannelChangeSucceeded() {
        return this._getCallback("ChannelChangeSucceeded");
    }

    set onChannelChangeSucceeded(callback) {
        this._setCallback("ChannelChangeSucceeded", callback);
    }

    get onDRMRightsError() {
        return this._getCallback("DRMRightsError");
    }

    set onDRMRightsError(callback) {
        this._setCallback("DRMRightsError", callback);
    }
    /*
     * Description:
     * The function that is called when the value of fullScreen changes.
     */
    get onFullScreenChange() {
        return this._getCallback("FullScreenChange");
    }

    set onFullScreenChange(callback) {
        this._setCallback("FullScreenChange", callback);
    }

    /*
     * Description:
     * The function that is called when the video object gains focus.
     */
    get onfocus() {
        return this._getCallback("focus");
    }

    set onfocus(callback) {
        this._setCallback("focus", callback);
    }

    /*
     * Description:
     * The function that is called when the video object loses focus.
     */
    get onblur() {
        return this._getCallback("blur");
    }

    set onblur(callback) {
        this._setCallback("blur", callback);
    }

    _getCallback(type) {
        if (this._callbacks) {
            return this._callbacks[type];
        }
    }

    _setCallback(type, callback) {
        if (this._callbacks) {
            this._callbacks[type] = callback;
        }
    }

    /*
     *
     * Description:
     * Returns the channel line-up of the tuner in the form of a ChannelConfig object as defined
     * in section 7.13.9. The method SHALL return the value null if the channel list is not
     * (partially) managed by the OITF (i.e., if the channel list information is managed entirely in the network).
     */
    getChannelConfig() {
        return oipfObjectFactory.createChannelConfig();
    }

    /*
     * Description:
     * If the video/broadcast object is in the unrealized state and video from exactly one
     * channel is currently being presented by the OITF then this binds the video/broadcast
     * object to that video.
     * If the video/broadcast object is in the stopped state then this restarts presentation of
     * video and audio from the current channel under the control of the video/broadcast object.
     * If video from more than one channel is currently being presented by the OITF then this binds
     * the video/broadcast object to the channel whose audio is being presented.
     * If there is no channel currently being presented, or binding to the necessary resources to
     * play the channel through the video/broadcast object fails for whichever reason, the OITF
     * SHALL dispatch an event to the onPlayStateChange listener(s) whereby the state
     * parameter is given value 0 (“ unrealized ”) and the error parameter is given the
     * appropriate error code.
     * Calling this method from any other states than the unrealized or stopped states SHALL have
     * no effect.
     * See section 7.13.1.1 for more information of its usage.
     *
     * NOTE: Returning a Channel object from this method does not guarantee that video or audio
     * from that channel is being presented. Applications should listen for the video/broadcast
     * object entering state 2 (“ presenting ”) in order to determine when audio or video is being
     * presented.
     *
     * TODO : binding of necessary ressources
     */
    bindToCurrentChannel() {
        var channelConfig = this._channelService;
        var currentChannel = channelConfig.currentChannel;

        switch (this.playState) {

            case this._UNREALIZED:
                //claimed scarce resources
                //Call method to claim scarces resources
                if (currentChannel) {
                    this._timeout()
                    .then(this.changePlayState.bind(this, this._CONNECTING))
                    .then(this.setCurrentChannel.bind(this, currentChannel))
                    .then(this._timeout)
                    .then(this.changePlayState.bind(this, this._PRESENTING));
                } else {
                    this._timeout()
                    .then(this.changePlayState.bind(this, this._UNREALIZED, 100));
                }
                break;

            case this._CONNECTING:
                if (!ChannelUtils.verifyASuitableTunerAvailable(currentChannel.idType)) {
                    this._timeout()
                    .then(this.changePlayState.bind(this, this._CONNECTING));
                } else {
                    this._timeout()
                    .then(this.setCurrentChannel.bind(this, currentChannel))
                    .then(this.changePlayState.bind(this, this._PRESENTING));
                }
                //Method documentation indicate it don't have effects in this state.
                break;

            case this._PRESENTING:
                /*if (!verifyASuitableTunerAvailable(currentChannel.idType)) {
                    this.timer = OipfUtils.this._timeout()
                            .then(this.changePlayState.bind(this, this._PRESENTING));
                }*/
                //Method documentation indicate it don't have effects in this state.
                break;

            case this._STOPPED:
                //FIX-ME :Make enable video and audio presentation
                if (!ChannelUtils.verifyASuitableTunerAvailable(currentChannel.idType, this)) {
                    this._timeout()
                    .then(this.changePlayState.bind(this, this._STOPPED, 0));
                } else {
                    this._timeout()
                    .then(this.changePlayState.bind(this, this._CONNECTING))
                    .then(this.setCurrentChannel.bind(this, currentChannel))
                    .then(this._timeout)
                    .then(this.changePlayState.bind(this, this._PRESENTING));
                }
                break;
        }
        return this.currentChannel;
    }

    addEventListener(type, listener) {
        this._eventManager.addEventListener(type, listener, this);
    }

    _fireEvent(event) {
        this._eventManager.fireEvent(event, this);
    }

    removeEventListener(type, listener) {
        this._eventManager.removeEventListener(type, listener, this);
    }

    changePlayState(state, error) {
        console.log("changeState", state);
        this.playState = state;
        this._fireEvent(this._eventManager.createCustomEvent("PlayStateChange", [this.playState, error]));
    }

    noTransientError(channel) {

        /*if (channel.locked) {
            return true;
        } else {
            return false;
        }*/
        return true;
    }

    noPermanentError(channel) {
        return true;
    }

    changeStateToConnectingWhenSwitching(channel) {

        if (this.noTransientError(this._CONNECTING) && this.noPermanentError(this._CONNECTING, channel)) {
            this.playState = this._CONNECTING;
            this._fireEvent(this._eventManager.createCustomEvent("PlayStateChange", [this.playState]));
            this._timeout()
            .then(this.changeStateToPresentingWhenSwitching.bind(this, channel));
        }
    }

    changeStateToPresentingWhenSwitching(channel) {

        if (this.noTransientError(this._PRESENTING)) {
            this.setCurrentChannel(channel);
            this.playState = this._PRESENTING;
            this._fireEvent(this._eventManager.createCustomEvent("PlayStateChange", [this.playState]));
            this._timeout()
            .then(this._fireEvent.bind(this, this._eventManager.createCustomEvent("ChannelChangeSucceeded", [channel])));
        }
    }

    setCurrentChannel(channel) {
        this.currentChannel = channel;
    }

    /*
     * Description:
     * Creates a Channel object of the specified idType . This method is typically used to create a
     * Channel object of type ID_DVB_SI_DIRECT or any other type. The Channel object can subsequently be
     * used by the setChannel() method to switch a tuner to this channel, which may or may not
     * be part of the channel list in the OITF. The resulting Channel object represents a locally
     * defined channel which, if not already present there, does not get added to the channel list
     * accessed through the ChannelConfig class (see section 7.13.9).
     *
     * If the channel of the given type cannot be created or the delivery system descriptor is not
     * valid, the method SHALL return null .
     *
     * If the channel of the given type (ID_DVB_SI_DIRECT) can be created and the delivery system descriptor is valid,
     * the method SHALL return a Channel object whereby at a minimum the properties with the
     * same names (i.e. idType , dsd and sid ) are given the same value as argument idType ,
     * dsd and sid of the createChannelObject method.
     *
     * Else, if the channel of the given type can be created and arguments are considered valid and
     * complete, then either:
     *
     * 1. If the channel is in the channel list then a new object of the same type and with
     * properties with the same values SHALL be returned as would be returned by calling
     * getChannelWithTriplet() with the same parameters as this method.
     *
     * 2. Otherwise, the method SHALL return a Channel object whereby at a minimum the
     * properties with the same names are given the same value as the given arguments of
     * the createChannelObject() method. The values specified for the remaining
     * properties of the Channel object are set to undefined .
     *
     * Arguments:
     *
     * Either (if idType ID_DVB_SI_DIRECT):
     * - idType: The type of channel, as indicated by one of the ID_* constants defined in
     * section 7.13.11.1. Valid values for idType include : ID_DVB_SI_DIRECT . For
     * other values this behaviour is not specified.
     *
     * - dsd: The delivery system descriptor (tuning parameters) represented as a string
     * whose characters shall be restricted to the ISO Latin-1 character set. Each
     * character in the dsd represents a byte of a delivery system descriptor as defined
     * by DVB-SI [EN 300 468] section 6.2.13, such that a byte at position "i" in the
     * delivery system descriptor is equal the Latin-1 character code of the character at
     * position "i" in the dsd.
     *
     * - sid: The service ID, which must be within the range of 1 to 65535.
     *
     * Or (Any other type):
     * - idType : The type of channel, as indicated by one of the ID_* constants defined in
     * section 7.13.11.1. Valid values for idType include : ID_DVB_SI_DIRECT . For
     * other values this behaviour is not specified.
     *
     * - onid : The original network ID. Optional argument that SHALL be specified
     * when the idType specifies a channel of type ID_DVB_* , ID_IPTV_URI ,
     * or ID_ISDB_* and SHALL otherwise be ignored by the OITF.
     *
     * - tsid : The transport stream ID. Optional argument that MAY be specified when
     * the idType specifies a channel of type ID_DVB_* , ID_IPTV_URI , or
     * ID_ISDB_* and SHALL otherwise be ignored by the OITF.
     *
     * - sid : The service ID. Optional argument that SHALL be specified when the
     * idType specifies a channel of type ID_DVB_* , ID_IPTV_URI , or
     * ID_ISDB_* and SHALL otherwise be ignored by the OITF.
     *
     * - sourceID : The source_ID. Optional argument that SHALL be specified when the
     * idType specifies a channel of type ID_ATSC_T and SHALL otherwise be
     * gnored by the OITF.
     *
     * - ipBroadcastID : The DVB textual service identifier of the IP broadcast service, specified in
     * the format “ ServiceName.DomainName ” when idType specifies a
     * channel of type ID_IPTV_SDS , or the URI of the IP broadcast service
     * when idType specifies a channel of type ID_IPTV_URI . Optional
     * argument that SHALL be specified when the idType specifies a channel
     * of type ID_IPTV_SDS or ID_IPTV_URI and SHALL otherwise be ignored
     * by the OITF.
     */
    createChannelObject() {

        if (arguments.length == 3 && arguments[0] == 13) {
            this._createDvbSiChannelObject(arguments);
        } else {
            if (arguments.length >= 2) { // Some argments can be optional but a minimum of 2 is required.
                this._createAnyDvbChannelObject(arguments);
            }
        }
        return null;
    }

    _createDvbSiDirectChannelObject(idType, dsd, sid) {

        if (idType == 13) { //Identify a channel of identifier type : ID_DVB_SI_DIRECT.

            if (CHANNEL_CREATION[idType]) { //Verify a channel of this idType (ID_DVB_SI_DIRECT = 13).


                if (ChannelUtils.isAValidTerrestrialDsd (dsd)) { //Verify the dsd validity.

                    if ((sid >= 1) && (sid <= 65535)) { //Verify the rigth range of service id.

                        var newChannel = new Channel(idType, dsd, sid, "localChannel" + OipfUtils.createUUID());
                        var channelConfig = this._channelService;

                        if (ChannelUtils.getChannelByDsd(channelConfig.channelList, dsd)) {

                            /* Normally channelList is readonly but the method documentation say
                             * that the channel must be add if it not already present.
                             */
                            channelConfig.channelList.push(newChannel);
                        }

                        return newChannel;

                    }
                }
            }
        }

    }

    /*
     * Description:
     * Creates a Channel object of the specified idType . This method is typically used to create a
     * Channel object of type ID_DVB_SI_DIRECT . The Channel object can subsequently be
     * used by the setChannel() method to switch a tuner to this channel, which may or may not
     * be part of the channel list in the OITF. The resulting Channel object represents a locally
     * defined channel which, if not already present there, does not get added to the channel list
     * accessed through the ChannelConfig class (see section 7.13.9).
     * Valid value for idType include : ID_DVB_SI_DIRECT . For other values this behaviour is not
     * specified.
     * If the channel of the given type cannot be created or the delivery system descriptor is not
     * valid, the method SHALL return null .
     * If the channel of the given type can be created and the delivery system descriptor is valid,
     * the method SHALL return a Channel object whereby at a minimum the properties with the
     * same names (i.e. idType , dsd and sid ) are given the same value as argument idType ,
     * dsd and sid of the createChannelObject method.
     *
     * Arguments:
     * - idType : The type of channel, as indicated by one of the ID_* constants defined in
     * section 7.13.11.1. Valid values for idType include : ID_DVB_SI_DIRECT . For
     * other values this behaviour is not specified.
     *
     * - onid : The original network ID. Optional argument that SHALL be specified
     * when the idType specifies a channel of type ID_DVB_* , ID_IPTV_URI ,
     * or ID_ISDB_* and SHALL otherwise be ignored by the OITF.
     *
     * - tsid : The transport stream ID. Optional argument that MAY be specified when
     * the idType specifies a channel of type ID_DVB_* , ID_IPTV_URI , or
     * ID_ISDB_* and SHALL otherwise be ignored by the OITF.
     *
     * - sid : The service ID. Optional argument that SHALL be specified when the
     * idType specifies a channel of type ID_DVB_* , ID_IPTV_URI , or
     * ID_ISDB_* and SHALL otherwise be ignored by the OITF.
     *
     * - sourceID : The source_ID. Optional argument that SHALL be specified when the
     * idType specifies a channel of type ID_ATSC_T and SHALL otherwise be
     * gnored by the OITF.
     *
     * - ipBroadcastID : The DVB textual service identifier of the IP broadcast service, specified in
     * the format “ ServiceName.DomainName ” when idType specifies a
     * channel of type ID_IPTV_SDS , or the URI of the IP broadcast service
     * when idType specifies a channel of type ID_IPTV_URI . Optional
     * argument that SHALL be specified when the idType specifies a channel
     * of type ID_IPTV_SDS or ID_IPTV_URI and SHALL otherwise be ignored
     * by the OITF.
     *
     * Important : We place us into a world without error. And no validation will be done like
     * this documentation mention it. This criterion not very useful in our implementation.
     */
    _createAnyDvbChannelObject(idType, onid, tsid, sid, sourceID, ipBroadcastID) {

        if (CHANNEL_CREATION[idType]) {
            if (idType == 13 || idType ==0) {
                return null;
            }

            if (idType == 30) {
                var equivalentChannel = this._channelService.channelList.getChannelBySourceID(sourceID);
            } else if (idType == 40) {
                equivalentChannel = ChannelUtils.getChannelByIpBroadcastID(ipBroadcastID);
            } else {
                equivalentChannel = this._channelService.channelList.getChannelByTriplet(onid, tsid, sid);
            }

            if (equivalentChannel) {
                var newChannel = new Channel(arguments.push(equivalentChannel.name));
            } else {
                newChannel = new Channel(arguments.push("localChannel:" + OipfUtils.createUUID()));
            }

            this._channelService.channelList.push(newChannel);
        }

        return null;
    }

    /*
     * Description: (Validation on Transport Stream won't be done here because this criteria not relevant)
     * Requests the OITF to switch a (logical or physical) tuner to the channel specified by
     * channel and render the received broadcast content in the area of the browser allocated for
     * the video/broadcast object.
     *
     * If the channel specifies an idType attribute value which is not supported by the OITF or a
     * combination of properties that does not identify a valid channel, the request to switch
     * channel SHALL fail and the OITF SHALL trigger the function specified by the
     * onChannelChangeError property, specifying the value 0 (“Channel not supported by
     * tuner”) for the errorState , and dispatch the corresponding DOM event (see below).
     *
     * FiX-ME: Yannis - 25/03/2015 - No take account trickplay
     * FiX-ME: Yannis - 25/03/2015 - No verification about Transport Stream
     * FiX-ME: Yannis - 25/03/2015 - No verification about possiblity of a local channel argument
     * FiX-ME: Yannis - 25/03/2015 - No verification about same tuning
     * parameters of argument channel with
     */
    setChannel(channel, trickplay, contentAccessDescriptorURL) {

        if (this.playState >= this._UNREALIZED && this.playState <= this._STOPPED && channel) {

            if (ChannelUtils.verifyASuitableTunerAvailable(channel.idType, this) && channel.idType != 41) {
                this._timeout()
                .then(this.changeStateToConnectingWhenSwitching.bind(this, channel));

            } else if (channel.idType == 40 || channel.idType == 41) {
                this._timeout()
                .then(this._fireEvent.bind(this, this._eventManager.createCustomEvent("ChannelChangeError", [null, 8])));
            } else {
                this._timeout()
                .then(this._fireEvent.bind(this, this._eventManager.createCustomEvent("ChannelChangeError", [null, 0])));
            }
        } else {
            if (this.playState != this._UNREALIZED) {
                this._timeout()
                .then(this.changePlayState.bind(this, this._UNREALIZED));
            }
        }
    }

    /*
     * Description:
     * Requests the OITF to switch the tuner that is currently in use by the video/broadcast
     * object to the channel that precedes the current channel in the active favourite list, or, if no
     * favourite list is currently selected, to the previous channel in the channel list. If it has reached
     * the start of the favourite/channel list, it SHALL cycle to the last channel in the list.
     *
     * If the current channel is not part of the channel list, it is implementation dependent whether
     * the method call succeeds or fails and, if it succeeds, which channel is selected. In both
     * cases, all appropriate functions SHALL be called and DOM events dispatched.
     *
     * If the previous channel is a channel that cannot be received over the tuner currently used by
     * the video/broadcast object, the OITF SHALL relay the channel switch request to a local
     * physical or logical tuner that is not in use and that can tune to the specified channel. The
     * behaviour is defined in more detail in the description of the setChannel method.
     *
     * If an error occurs during switching to the previous channel, the OITF SHALL trigger the
     * function specified by the onChannelChangeError property with the appropriate channel
     * and errorState value, and dispatch the corresponding DOM event (see below).
     *
     * If the OITF does not maintain the channel list and favourite list by itself, the request SHALL
     * fail and the OITF SHALL trigger the onChannelChangeError function with the channel
     * property having the value null , and errorState=10 (“channel cannot be changed by
     * nextChannel()/prevChannel() methods”).
     *
     * If successful, the OITF SHALL trigger the function specified by the
     * onChannelChangeSucceeded property with the appropriate channel value, and also
     * dispatch the corresponding DOM event.
     * Calls to this method are valid in the Connecting, Presenting and Stopped states. They are
     * not valid in the Unrealized state and SHALL fail.
     *
     */
    prevChannel() {
        this._channelSwitch(-1);
    }

    /*
     * Description:
     * Requests the OITF to switch the tuner that is currently in use by the video/broadcast
     * object to the channel that precedes the current channel in the active favourite list, or, if no
     * favourite list is currently selected, to the previous channel in the channel list. If it has reached
     * the start of the favourite/channel list, it SHALL cycle to the last channel in the list.
     *
     * If the current channel is not part of the channel list, it is implementation dependent whether
     * the method call succeeds or fails and, if it succeeds, which channel is selected. In both
     * cases, all appropriate functions SHALL be called and DOM events dispatched.
     *
     * If the next channel is a channel that cannot be received over the tuner currently used by
     * the video/broadcast object, the OITF SHALL relay the channel switch request to a local
     * physical or logical tuner that is not in use and that can tune to the specified channel. The
     * behaviour is defined in more detail in the description of the setChannel method.
     *
     * If an error occurs during switching to the next channel, the OITF SHALL trigger the
     * function specified by the onChannelChangeError property with the appropriate channel
     * and errorState value, and dispatch the corresponding DOM event (see below).
     *
     * If the OITF does not maintain the channel list and favourite list by itself, the request SHALL
     * fail and the OITF SHALL trigger the onChannelChangeError function with the channel
     * property having the value null , and errorState=10 (“channel cannot be changed by
     * nextChannel()/prevChannel() methods”).
     *
     * If successful, the OITF SHALL trigger the function specified by the
     * onChannelChangeSucceeded property with the appropriate channel value, and also
     * dispatch the corresponding DOM event.
     * Calls to this method are valid in the Connecting, Presenting and Stopped states. They are
     * not valid in the Unrealized state and SHALL fail.
     *
     */
    nextChannel() {
        this._channelSwitch(1);
    }

    /*
     * Description:
     * Sets the rendering of the video content to full-screen ( fullscreen = true ) or windowed
     * ( fullscreen = false ) mode (as per [Req. 5.7.1.c] of [CEA-2014-A]). If this indicates a
     * change in mode, this SHALL result in a change of the value of property fullScreen .
     * Changing the mode SHALL NOT affect the z-index of the video object.
     *
     * Arguments:
     * - fullScreen : Boolean to indicate whether video content should be rendered full-screen or not.
     */
    setFullScreen(fullscreen) {
    }

    /*
     * Description:
     * Adjusts the volume of the currently playing media to the volume as indicated by volume.
     * Allowed values for the volume argument are all the integer values starting with 0 up to and
     * including 100.
     * A value of 0 means the sound will be muted.
     * A value of 100 means that the
     * volume will become equal to current “master” volume of the device, whereby the “master”
     * volume of the device is the volume currently set for the main audio output mixer of the
     * device.
     * All values between 0 and 100 define a linear increase of the volume as a percentage
     * of the current master volume, whereby the OITF SHALL map it to the closest volume level
     * supported by the platform.
     * The method returns true if the volume has changed. Returns false if the volume has not
     * changed. Applications MAY use the getVolume() method to retrieve the actual volume set.
     *
     * Arguments:
     * - volume: Integer value between 0 up to and including 100 to indicate volume level.
     *
     * Actually the norm don't talk about the way to obtain the current master volume for the main
     * audio output mixer; So we'll base on the system volume.
     * FIX-ME: Yannis - 24/03/2015 - Bindind with the configuration object.
     */
    setVolume(volume) {

        if (!Number.isInteger(volume) || volume < 0 || volume > 100 ) {
            return false;
        }

        this._localSystem.volume = volume;

        return true;
    }

    /*
     * Description:
     * Returns the actual volume level set; for systems that do not support individual volume
     * control of players, this method will have no effect and will always return 100.
     *
     */
    getVolume() {
        return this._localSystem && this._localSystem.volume || 0;
    }

    /*
     * Description:
     * Releases the decoder/tuner used for displaying the video broadcast inside the
     * video/broadcast object, stopping any form of visualization of the video inside the
     * video/broadcast object and releasing any other associated resources.
     * If the object was created with an allocationMethod of STATIC_ALLOCATION ,
     * the releasing of resources shall change this to DYNAMIC_ALLOCATION .
     */
    release() {
        if (this.playState >= this._CONNECTING && this.playState <= this._STOPPED) {
            /* Call method to :
             * - release tuner used by this video/broadcast object.
             * - stop visualization of the video.
             * - and scarce ressources loaded previously.
             */


            this._timeout()
            .then(this.changePlayState.bind(this, this._UNREALIZED));
        }
    }

    /*
     * Description:
     * Stop presenting broadcast video. If the video/broadcast object is in any state other than the
     * unrealized state, it SHALL transition to the stopped state and stop video and audio
     * presentation. This SHALL have no effect on access to non-media broadcast resources such
     * as EIT information.
     * Calling this method from the unrealized state SHALL have no effect.
     * See section 7.13.1.1 for more information of its usage.
     *
     */
    stop() {
        if (this.playState == this._CONNECTING || this.playState == this._PRESENTING) {
            /* Call method to :
             * - stop the presentation of the video and audio.
             * - and scarce ressources loaded previously.
             */


            this._timeout()
            .then(this.changePlayState.bind(this, this._STOPPED));
        }
    }

    _channelSwitch(step) {
        //claimed scarce resources
        //Call method to claim scarce resources

        var channelConfig = this._channelService;
        var favouriteLists = channelConfig.favouriteLists;

        if ((this.playState > 0) && (this.playState < 4)) {
            //Verify that OITF maintain channel list or favourite list by itself
            if (channelConfig && favouriteLists) {

                //Verify that the currentChannel exists in the channel list of channel configuration
                if (!channelConfig.channelList.getChannel(this.currentChannel.channelId)) {
                    this._timeout()
                    .then(this._fireEvent.bind(this, this._eventManager.createCustomEvent("ChannelChangeError", [null, 100])));
                    /*setTimeout(function () {
                        self.onChannelChangeError && self.onChannelChangeError(null, 100);
                    } 0);*/

                    if (this.playState == 1) {
                        this._timeout()
                        .then(this.changePlayState.bind(this, this._UNREALIZED, 100));
                        /*setTimeout(function () {
                            self.playState =  0;
                            self.onPlayStateChange && self.onPlayStateChange(self.playState, 100);
                        } 0);*/
                    }
                } else {
                    /*
                     * The norm affirm that the value "undefined" for the property "currentFavouriteList"
                     * means no current favourite list is activating.
                     * But test on this value for "currentFavouriteList" can cause confusion
                     * and when her value equals null, we have not the wanted behaviours.
                     */
                    if (channelConfig.currentFavouriteList.length == 0) {
                        //Find the previous channel into channel list
                        var channelCollection = channelConfig.channelList;
                    } else {
                        //Find the previous channel into the current favourite list
                        channelCollection = channelConfig.currentFavouriteLists;
                    }

                    var channelToChange = ChannelUtils.findChannel(channelCollection, this.currentChannel, step);

                    if (ChannelUtils.verifyASuitableTunerAvailable(channelToChange.idType, this)) {
                        this._timeout()
                        .then(this.changeStateToConnectingWhenSwitching.bind(this, channelToChange));
                    } else if (channelToChange.idType == 40 || channelToChange.idType == 41) {
                        this._timeout()
                        .then(this._fireEvent.bind(this, this._eventManager.createCustomEvent("ChannelChangeError", [null, 8])));
                    } else {

                        this._timeout()
                        .then(this._fireEvent.bind(this, this._eventManager.createCustomEvent("ChannelChangeError", [null, 0])));
                    }

                }

            } else {
                //When the OITF does not maintain channel list or favourite list by itself
                this._timeout()
                .then(this._fireEvent.bind(this, this._eventManager.createCustomEvent("ChannelChangeError", [null, 10])));

            }
        }
    }

    /*
     * Description:
     * Creates a ChannelList object from the specified SD&S Broadcast Discovery
     * Record. Channels in the returned channel list will not be included in the
     * channel list that can be retrieved via calls to getChannelConfig().
     *
     * Arguments:
     * - bdr: An XML-encoded string containing an SD&S Broadcast Discovery
     * Record as specified in [OIPF_META2]. If the string is not a valid
     * Broadcast Discovery Record, this method SHALL return null.
     *
     * Return: ChannelList
     *
     * FIXME
     * - Yannis 21/05/2015 Validate the parameter bdr
     */
    createChannelList(bdr) {
        var channelListArray = bdr.split(",");
        var channelList = new ChannelList();
        var channelInfoNumber = {
            3: true,
            4: true,
            5: true,
            6: true
        };

        if (channelListArray.length > 1) {

            for (var i = 0, li = channelListArray.length; i < li; i++) {
                var channelInfo = channelListArray[i];
                var channelInfoArray = channelInfo.split("-");

                var isChannelInfoNumberExist =
                        channelInfoNumber[channelInfoArray.length];

                if (!isChannelInfoNumberExist) {
                    return null;
                }
                //Validation of channel information return null if false

                //Create the channel
                var channelObject = this._createChannel(channelInfoArray);
                channelList.push(channelObject);
            }

            if (!this._localChannelList) {
                this._localChannelList = [];
            }

            this._localChannelList.push(channelList);

            return channelList;
        }

        return null;
    }

    /*
     * We consider that the parameter of this method is valid concerning
     * the type of channel information it contains but they are string-encoded.
     * And call the associated method which must process a specific
     * group of channel information according to the number of information.
     */
    _createChannel(minimalChannelInfoArray) {
        var mapping = {
            3: "_createChannelATSC_T",
            4: "_createChannelDVB_SI_DIRECT",
            5: "_createChannelDVBOrISDB",
            6: "_createChannelIPTV "
        };
        var channelInfoNumber = minimalChannelInfoArray.length;
        return this[mapping[channelInfoNumber]].call(minimalChannelInfoArray);
    }

    /*
     * This method must transform the information which must be of type Integer.
     * Then create and return a Channel object.
     * The parameter contain in this order:
     * 0:Integer, 1:Integer, 2:String
     *
     * Return: Channel
     */
    _createChannelATSC_T(channelInfoArray) {
        var i = 0;

        return new Channel(
                this._convertInInteger(channelInfoArray[i++]),
                this._convertInInteger(channelInfoArray[i++]),
                channelInfoArray[i++]);
    }

    /*
     * This method must transform the information which must be of type Integer.
     * Then create and return a Channel object.
     * The parameter contain in this order:
     * 0:Integer, 1:Integer, 2:Integer, 3:Integer, 4:String, 5:String
     *
     * Return: Channel
     */
    _createChannelIPTV(channelInfoArray) {
        var i = 0;

        return new Channel(
                this._convertInInteger(channelInfoArray[i++]),
                this._convertInInteger(channelInfoArray[i++]),
                this._convertInInteger(channelInfoArray[i++]),
                this._convertInInteger(channelInfoArray[i++]),
                channelInfoArray[i++],
                channelInfoArray[i++]);
    }

    /*
     * This method must transform the information which must be of type Integer.
     * Then create and return a Channel object.
     * The parameter contain in this order:
     * 0:Integer, 1:Integer, 2:Integer, 3:Integer, 4:String
     *
     * Return: Channel
     */
    _createChannelDVBOrISDB(channelInfoArray) {
        var i = 0;

        return new Channel(
                this._convertInInteger(channelInfoArray[i++]),
                this._convertInInteger(channelInfoArray[i++]),
                this._convertInInteger(channelInfoArray[i++]),
                this._convertInInteger(channelInfoArray[i++]),
                channelInfoArray[i++]);
    }

    /*
     * This method must transform the information which must be of type Integer.
     * Then create and return a Channel object.
     * The parameter contain in this order:
     * 0:Integer, 1:String, 2:Integer, 3:String
     *
     * Return: Channel
     */
    _createChannelDVB_SI_DIRECT(channelInfoArray) {
        var i = 0;

        return new Channel(
                this._convertInInteger(channelInfoArray[i++]),
                channelInfoArray[i++],
                this._convertInInteger(channelInfoArray[i++]),
                channelInfoArray[i++]);
    }

    _convertInInteger(stringValue) {
        return Number.parseInt(stringValue);
    }

}
