/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * The AVOutput class represents an audio or video output on the local platform.
 */

class AVOutput {

    constructor(name, type, enabled) {
        /*
         * Description:
         * The name of the output. Each output SHALL have a name that is unique on the local system. At least one
         * of the outputs SHALL have the name " all " and SHALL represent all available outputs on the platform. The
         * results of reading properties from the " all " AVOutput are implementation specific.constructor
         *
         * Visibility Type : readonly String
         */
        this.name = name;

        /*
         * Description:
         * The type of the output. Valid values are “ audio ”, “ video ”, or “ both ”.
         *
         * Visibility Type : readonly String
         */
        this.type = type;

        /*
         * Description:
         * Flag indicating whether the output is enabled. Setting this property SHALL enable or disable the output.
         *
         * Type : Boolean
         */
        this.enabled = enabled;

        /*
         * Description:
         * Flag indicating whether the subtitles are enabled. The language of
         * the displayed subtitles is determined by a combination of the value of
         * the Configuration.preferredSubtitleLanguage property (see section 7.3.2)
         * and the subtitles available in the stream.
         * For audio outputs, setting this property will have no effect.
         *
         * Type : Boolean
         */
        this.subtitleEnabled = null;

        this.videoMode = null;

        /*
         * Description:
         * Read or set the output mode for digital audio outputs for which hardware
         * support MAY be available on the device. Valid values are shown below.
         * ------------------------------------------------------------------------
         * Value        |              Behaviour
         * ------------- ----------------------------------------------------------
         * ac3          |   Output AC-3 audio.
         * ------------- ----------------------------------------------------------
         * uncompressed |   Output uncompressed PCM audio.
         * ------------- ----------------------------------------------------------
         * For video-only outputs, setting this property SHALL have no effect.
         *
         * Type: String
         *
         */
        this.digitalAudioMode = null;

        /*
         * Description:
         * Read or set the range for digital audio outputs for which hardware
         * support MAY be available on the device.
         * Valid values are shown below
         * ------------------------------------------------------------------------
         * Value    |              Behaviour
         * --------- --------------------------------------------------------------
         * normal   |   Output AC-3 audio.
         * --------- --------------------------------------------------------------
         * narrow   |   Output uncompressed PCM audio.
         * --------- --------------------------------------------------------------
         * wide     |   Output uncompressed PCM audio.
         * ------------------------------------------------------------------------
         * For video-only outputs, setting this property SHALL have no effect.
         *
         * Type: String
         *
         */
        this.audioRange = null;

        /*
         * Description:
         * Read or set the video format for HD and 3D video outputs for which
         * hardware support MAY be available on the device. Valid values are:
         * 480i
         * 480p
         * 576i
         * 576p
         * 720i
         * 720p
         * 1080i
         * 1080p
         * 720p_TaB
         * 720p_SbS
         * 1080i_SbS
         * 1080p_TaB
         * 1080p_SbS
         * For audio-only or standard-definition outputs, setting this property
         * SHALL have no effect.
         *
         * Type: String
         *
         */
        this.hdVideoFormat = null;

        /*
         * Description:
         * Indicates the output display aspect ratio of the display device connected
         * to this output for which hardware support MAY be available on the device.
         * Valid values are:
         * 4:3
         * 16:9
         * For audio-only outputs, setting this property SHALL have no effect.
         *
         * Type: String
         *
         */
        this.tvAspectRatio = null;

        /*
         * Description:
         * Read the video format conversion modes that may be used when
         * displaying a 4:3 input video on a 16:9 output display or 16:9 input video
         * on a 4:3 output display. The assumption is that the hardware supports
         * conversion from either format and there is no distinction between the
         * two. See the definition of the 'videoMode' property for valid values.
         * For audio outputs, this property will have the value null .
         *
         * Visibility Type: readonly StringCollection
         */
        this.supportedVideoModes = null;

        /*
         * Description:
         * Read the supported output modes for digital audio outputs.
         * See the definition of the 'digitalAudioMode' property for valid values.
         * For video outputs, this property will have the value null.
         *
         * Visibility Type: readonly StringCollection
         */
        this.supportedDigitalAudioModes = null;

        /*
         * Description:
         * Read the supported ranges for digital audio outputs.
         * See the definition of the 'audioRange' property for valid values.
         * For video outputs, this property will have the value null.
         *
         * Visibility Type: readonly StringCollection
         */
        this.supportedAudioRanges = null;

        /*
         * Description:
         * Read the supported HD and 3D video formats.
         * See the definition of the 'hdVideoFormat' property for valid values.
         * For audio outputs, this property will have the value null.
         *
         * Visibility Type: readonly StringCollection
         */
        this.supportedHdVideoFormats = null;

        /*
         * Description:
         * Read the supported TV aspect ratios. See the definition of the
         * 'tvAspectRatio' property for valid values.
         * For audio outputs, this property will have the value null .
         *
         * Visibility Type: readonly StringCollection
         */
        this.supportedAspectRatios = null;

        /*
         * Description:
         * Read whether the display is currently in a 2D or 3D mode.
         * Return values are:
         * ------------------------------------------------------------------------
         * Value        |              Behaviour
         * ------------- ----------------------------------------------------------
         * 0            |   The display is in a 2D video mode
         * ------------- ----------------------------------------------------------
         * 1            |   The display is in a 3D video mode
         * ------------- ----------------------------------------------------------
         *
         * Type: Integer
         */
        this.current3DMode = null;
    }
}
