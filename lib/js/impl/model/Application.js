/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * The Application class is used to implement the characteristics of a
 * DAE application. If the document of an application is modified
 * (or even replaced entirely), the Application object SHALL be retained.
 * This means that the permission set granted when the application is created
 * applies to all “edits” of the document or other pages in the application,
 * until the application is destroyed.
 */

class Application {

    constructor(windowApp, applicationManager) {
        /*
         * Description:
         * true if the application is visible, false otherwise. The value of
         * this property is not affected by the application's Z-index or position
         * relative to other applications. Only calls to the show() and hide()
         * methods will affect its value.
         *
         * Visibility Type: readonly Boolean
         */
        this.visible = false;

        /*
         * Description:
         * true if the application is in the list of currently active applications,
         * false otherwise (as defined in section 4.3.8).
         *
         * Visibility Type: readonly Boolean
         */
        this.active = false;

        /*
         * Descrioption:
         * StringCollection object containing the names of the permissions granted
         * to this application.
         *
         * Visibilty Type: readonly StringCollection
         */
        this.permissions = null;

        /*
         * Description:
         * true if the application receives cross application events before any
         * other application, false otherwise.
         *
         * Visibility Type: readonly Boolean
         */
        this.isPrimaryReceiver = null;

        /*
         * Description:
         * Access the current application’s private data object.
         * If an application attempts to access the privateData property of an
         * Application object for a different application, the OITF SHALL throw
         * an error as defined in section 10.1.1.
         *
         * Visibility Type: readonly ApplicationPrivateData
         */
        this.privateData = null;

        /*
         * Description:
         * A strict subset of the DOM Window object representing the application.
         * No symbols from the Window object are accessible through this property
         * except the following:
         *
         * void postMessage (any message, String targetOrigin)
         *
         * Visibility Type: readonly Window
         */
        this.window = null;

        this._applicationManager = applicationManager;
        this._setWindow(windowApp);

        this._listeners = {};
        this._callbacks = {};
        this._eventManager = new EventManager();

        this._timerManager = new TimerManager();
        this._timeout = this._timerManager.createTimer.bind(this._timerManager, 0);
    }

    /*
     * Description:
     * Create a new application and add it to the application tree. Calling
     * this method does not automatically show the newly-created application.
     * This call is asynchronous and may return before the new application is
     * fully loaded. An ApplicationLoaded event will be targeted at
     * the Application object when the new application has fully loaded. If
     * the application cannot be created, this method SHALL return null .
     *
     * Arguments:
     * - uri: The URI of the first page of the application to be created or the
     * localURI of a Widget as defined in section 7.2.8.1.1.
     *
     * - createChild: Flag indicating whether the new application is a child of
     * the current application. A value of true indicates that the new
     * application should be a child of the current application; a value of
     * false indicates that it should be a sibling.
     *
     */
    createApplication(uri, createChild) {
        var iframeApp = document.createElement("iframe");

        iframeApp.setAttribute("src", uri);

        if (!iframeApp.window) {
            return null;
        }

        var app = new Application(iframeApp.window, this._applicationManager);

        if (app) {
            this._applicationManager._fireEvent("ApplicationLoaded", [app]);
        }

        return app;
    }

    /*
     * Description:
     * If the application visualization mode as defined by method
     * getApplicationVisualizationMode() in section 7.2.1.3, is:
     *
     * 1 : Make the application visible.
     *
     * 2 : Make the application visible. Calling this method from the
     * application itself may have no effect.
     *
     * 3 : Request to make the application visible.
     *
     * This method only affects the visibility of an application. In the case
     * where more than one application is visible, calls to this method will
     * not affect the z-index of the application with respect to any other
     * visible applications.
     */
    show() {
        this.visible = true;
    }

    /*
     * Description:
     * If the application visualization mode as defined by method
     * getApplicationVisualizationMode() in section 7.2.1.3, is:
     *
     * 1 : Make the application invisible.
     *
     * 2 : Make the application invisible. Calling this method from the
     * application itself may have no effect.
     *
     * 3 : Request to make the application invisible.
     *
     * Calling this method has no effect on the lifecycle of the application.
     * Note: Broadcast independent applications should not call this method.
     * Doing so may result in only the background being visible to the user.
     */
    hide() {
        this.visible = false;
    }

    /*
     * Desription
     * Move the application to the front of the active applications list. If the
     * application has been hidden using Application.hide(), this method does
     * not cause the application to be shown.
     *
     * If the application visualization mode as defined by method
     * getApplicationVisualizationMode() in section 7.2.1.3, is:
     *
     * 1: The application's Window object SHALL be moved to the top of
     * the stack of visible applications. In addition, the application's Window
     * object SHALL gain input focus if argument gainFocus has value true.
     *
     * 2 : The application's Window object SHALL be moved to the top of
     * the stack of visible applications. In addition, the application's Window
     * object SHALL gain input focus if argument gainFocus has value true.
     * Calling this method from the application itself MAY have no effect.
     *
     * 3 : Request to make the application’s Window object visible.
     * Once visible, the application SHALL be given input focus, irrespective of
     * the value for argument gainFocus.
     */
    activateInput(gainFocus) {
        if (this._applicationManager.getApplicationVisualizationMode() == 1 ||
                this._applicationManager.getApplicationVisualizationMode() == 2) {

            this.active = true;
        }
    }

    /**
     * [[Description]]
     * Remove the application from the active applications list. This has no effect on the
     * lifecycle of the application and MAY have no effect on the resources it uses. Applications
     * which are not active will receive no cross-application events, unless their Application
     * object is the target of the event (as for the events defined in section 7.2.6). Applications
     * may still be manipulated via their Application object or their DOM tree.
     */
    deactivateInput() {
        this.active = false;
        this._applicationManager._removeFromActiveList(this);
    }

    _setWindow(windowApp) {
        this.window = {};
        if (windowApp && windowApp.postMessage) {
            this.window.postMessage = windowApp.postMessage.bind(windowApp);
        }
    }

    _fireEvent(eventName) {
        var event = this._eventManager.createCustomEvent(eventName, null);
        this._eventManager.fireEvent(event, this);
    }
}
