/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
class BookmarkCollection extends Collection{

    constructor(...args) {
        super(...args);
    }

    /*
     * Description:
     * Add a new bookmark to the collection. If the bookmark cannot be added
     * (e.g. because the value given for time lies outside
     * the length of the recording), this method SHALL return null.
     *
     * Argument:
     * - time: The time at which the bookmark is set, in seconds since
     * the start of the recording.
     *
     * - name: The name of the bookmark.
     */
    addBookmark(time, name) {
        var newBookmark = new Bookmark(time, name);
        this.push(newBookmark);
    }

    /*
     * Description:
     * Remove a bookmark from the collection.
     *
     * Argument:
     * - bookmark: The bookmark to be removed.
     */
    removeBookmark(bookmark) {
        var indexOfBookmark = this.indexOf(bookmark);
        var lastBookmark = this[this.length-1];

        if (indexOfBookmark) {
            this[indexOfBookmark] = lastBookmark;
            this.pop();
        }
    }

}
