/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * Description:
 * The Channel object represents a broadcast stream or service.
 * Channel objects typically represent channels stored in the channel list (see section 7.13.10). Channel objects may also
 * represent locally defined channels created by an application using the createChannelObject() methods on the
 * video/broadcast embedded object or the ChannelConfig class or the createChannelList() method on the
 * ChannelConfig class. Accessing the channel property of a ScheduledRecording object or Recording object
 * which is scheduled on a locally defined channel SHALL return a Channel object representing that locally defined
 * channel.
 * Except for the hidden property, writing to the writable properties on a Channel object SHALL have no effect for
 * Channel objects representing channels stored in the channel list. Applications SHOULD only change these writable
 * properties of a locally defined channel before the Channel object is referenced by another object or passed to an API
 * call as an input parameter. The effects of writing to these properties at any other time is implementation dependent.
 * The LocalSystem object allows hardware settings related to the local device to be read and modified.
 *
 * Note: The standbyState property has been removed from this class.
 */
class Channel {

    constructor() {
        /*
         * Description:
         * The type of channel. The value MAY be indicated by one of the TYPE_* constants defined above. If the
         * type of the channel is unknown then the value SHALL be “ undefined ”.
         * NOTE: Values of this type between 256 and 511 are reserved for use by related specifications on request by
         * liaison.
         *
         * Visibility Type: readonly Integer
         */
        this.channelType = null;
        /*
         * Description:
         * The type of identification for the channel, as indicated by one of the ID_* constants defined above.
         *
         * Visibility Type: readonly Integer
         */
        this.idType = null;
        /*
         * Description:
         * Unique identifier of a channel within the scope of the OITF. The ccid is defined by the OITF and SHALL have
         * prefix ‘ ccid ’ : e.g. ‘ccid:{tunerID.}majorChannel{.minorChannel}’.
         *
         * Note: the format of this string is platform-dependent.
         *
         * Visibility Type: readonly String
         */
        this.ccid = null;
        /*
         * Description:
         * The name of the channel. Can be used for linking analog channels without CNI. Typically, it will contain the
         * call sign of the station (e.g. 'HBO').
         *
         * Type: String
         */
        this.name = null;
        /*
         * Description:
         * Optional unique identifier of the tuner within the scope of the OITF that is able to receive the given channel.
         *
         * Visibility Type: readonly String
         */
        this.tunerID = null;

        /*
         * Description:
         * DVB or ISDB original network ID.
         *
         * Visibility Type: readonly Integer
         */
        this.onid = null;

        /*
         * Description:
         * DVB or ISDB transport stream ID.
         *
         * Visibility Type: readonly Integer
         */
        this.tsid = null;

        /*
         * Description:
         * DVB or ISDB service ID.
         *
         * Visibility Type: readonly Integer
         */
        this.sid = null;

        /*
         * Description:
         * For channels of type ID_DVB_SI_DIRECT created through createChannelObject() , this property defines
         * the delivery system descriptor (tuning parameters) as defined by DVB-SI [EN 300 468] section 6.2.13.
         *
         * The dsd property provides a string whose characters shall be restricted to the ISO Latin-1 character set.
         * Each character in the dsd represents a byte of a delivery system descriptor as defined by DVB-SI [EN 300
         * 468] section 6.2.13, such that a byte at position "i" in the delivery system descriptor is equal the Latin-1
         * character code of the character at position "i" in the dsd.
         *
         * Described in the syntax of JavaScript: let sdd[] be the byte array of a system delivery descriptor, in which
         * sdd[0] is the descriptor_tag, then, dsd is its equivalent string, if :
         * dsd.length==sdd.length and
         * for each integer i : 0<=i<dsd.length holds: sdd[i] == dsd.charCodeAt(i).
         */
        this.dsd = null;

        /*
         * Description:
         * ATSC source_ID value.
         *
         * Visibility Type: readonly Integer
         */
        this.sourceId = null;

        /*
         * Description:
         * If the channel has an idType of ID_IPTV_SDS, this property denotes the DVB textual service identifier of the
         * IP broadcast service, specified in the format “ServiceName.DomainName” with the ServiceName and
         * DomainName as defined in [DVB-IPTV].
         * If the Channel has an idType of ID_IPTV_URI, this element denotes a URI of the IP broadcast service.
         *
         * Visibility Type: readonly String
         */
        this.ipBroadcastID = null;

        this.locked = null;

        var dispatcher = {
            1: "_createChannelFromConf",
            3: "_createChannelATSC_T",
            4: "_createChannelDVB_SI_DIRECT",
            5: "_createChannelDVBOrISDB",
            6: "_createChannelIPTV"
        };

        (arguments.length !== 6) && this._setCcid(OipfUtils.createUUID());
        this[dispatcher[arguments.length]].apply(this, arguments);

    }

    _createChannelATSC_T(idType, sourceID, name) {
        this.idType = idType;
        this.sourceID = sourceID;
        this.name = name;
    }

    _createChannelIPTV(idType, onid, tsid, sid, ipBroadcastID, name) {
        this.idType = idType;
        this.ipBroadcastID = ipBroadcastID;
        this.name = name;
        if (idType === 41) {
            this.onid = onid;
            this.tsid = tsid;
            this.sid = sid;
        }
    }

    _createChannelDVBOrISDB(idType, onid, tsid, sid, name) {
        this.idType = idType;
        this.onid = onid;
        this.tsid = tsid;
        this.sid = sid;
        this.name = name;
    }

    _createChannelDVB_SI_DIRECT(idType, dsd, sid, name) {
        this.idType = idType;
        this.dsd = dsd;
        this.sid = sid;
        this.name = name;
    }

    _setCcid(ccid) {
        this.ccid = ccid.toString();
    }

    _createChannelFromConf(channelConf) {
        OipfUtils.initProperties.call(this, channelConf);
    }

}
