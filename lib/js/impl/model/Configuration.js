/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
class Configuration {

    constructor(defaultProperties) {
       /*
        * Description:
        * A comma-separated set of languages to be used for audio playback, in order of preference. Each language SHALL be
        * indicated by its ISO 639-2 language code as defined in [ISO 639-2].
        *
        * Type: String
        */
        this.preferredAudioLanguage = null;

       /*
        * Description:
        * A comma-separated set of languages to be used for subtitle playback, in order of preference. The subtitle
        * component (see section 7.16.5.5) that matches the highest ordered language SHALL be activated
        * (equivelant to the selectComponent method) and all other subtitle components SHALL be deactivated
        * (equivelant to the unselectComponent method).
        * Each language SHALL be indicated by its ISO 639-2 language code as defined in [ISO 639-2] or as a
        * wildcard specifier " *** ".
        * If the wildcard is included it SHALL be the last item in the set. If no subtitle component in the content
        * matches a language in this property and the wildcard is included then the first (lowest) subtitle component
        * SHALL be selected.
        *
        * Type: String
        */
        this.preferredSubtitleLanguage = null;

       /*
        * Description:
        * A comma-separated set of languages to be used for the user interface of a service, in order of preference.
        * Each language SHALL be indicated by its ISO 639-2 language code as defined in [ISO 639-2].
        * If present, the HTTP Accept-language header shall contain the same languages as the
        * preferredUILanguage property with the same order of preference. NOTE: The order of preference in the
        * Accept-language header is indicated using the quality factor.
        *
        * Type: String
        */
        this.preferredUILanguage = null;

       /*
        * Description:
        * An ISO-3166 three character country code identifying the country in which the receiver is deployed.
        *
        * Type: String
        */
        this.countryId = null;

       /*
        * Description:
        * An integer indicating the time zone within a country in which the receiver is deployed. A value of 0 SHALL
        * represent the eastern-most time zone in the country, a value of 1 SHALL represent the next time zone to
        * the west, and so on. Valid values are in the range 0 – 60.
        *
        * Type: Integer
        */
        this.regionId = null;

        /*
        * Description:
        * The policy dictates what mechanism the system should use when storage space is exceeded.
        * Valid values are shown in the table below.
        * ------------------------------------------------------------------------------------------------
        * Value |                                   Description
        * ------ -----------------------------------------------------------------------------------------
        * 0     |   Indicates a recording management policy where no recordings are to be deleted.
        * ------ -----------------------------------------------------------------------------------------
        * 1     |   Indicates a recording management policy where only watched recordings MAY be deleted.
        * ------ -----------------------------------------------------------------------------------------
        * 2     |   Indicates a recording management policy where only recordings older than the specified
        *       |   threshold (given by the pvrSaveDays and pvrSaveEpisodes properties) MAY be deleted.
        * ------------------------------------------------------------------------------------------------
        *
        * Type: Integer
        */
        this.pvrPolicy = null;


       /*
        * Description:
        * When the pvrPolicy property is set to the value 2, this property indicates the minimum number of
        * episodes that SHALL be saved for series-link recordings.
        *
        * Type: Integer
        */
        this.pvrSaveEpisodes = null;


         /*
        * Description:
        * When the pvrPolicy property is set to the value 2, this property indicates the minimum save time (in
        * days) for individual recordings. Only recordings older than the save time MAY be deleted.
        *
        * Type: Integer
        */
        this.pvrSaveDays = null;


       /*
        * Description:
        * The default padding (measured in seconds) to be added at the start of a recording.
        *
        * Type: Integer
        */
        this.pvrStartPadding = null;

       /*
        * Description:
        * The default padding (measured in seconds) to be added at the end of a recording.
        *
        * Type: Integer
        */
        this.pvrEndPadding = null;


       /*
        * Description:
        * The time shift mode indicates the preferred mode of operation for support of timeshift playback in the
        * video/broadcast object. Valid values are defined in the timeShiftMode property in section 7.13.2.2. The
        * default value is 0, timeshift is turned off.
        *
        * Type: Integer
        */
        this.preferredTimeShiftMode = null;

        OipfUtils.initProperties.call(this, defaultProperties);
        this._keys = {};
    }

    /*
    * Description:
    * Get the system text string that has been set for the specified key.
    *
    * Argument:
    * -key: A key identifying the system text string to be retrieved.
    *
    * Return: String
    */
    getText(key) {
        return this._keys[key];
    }

    /*
    * Description:
    * Set the system text string that has been set for the specified key. System text strings are
    * used for automatically-generated messages in certain cases, e.g. parental control messages.
    *
    * Arguments:
    * -key: The key for the text string to be set. Valid keys are:
    * -----------------------------------------------------------------------------------------------------------------------------------------------------
    * Key               |                                   Description
    * ------------------ ----------------------------------------------------------------------------------------------------------------------------------
    * no_title          |   Text string used as the title for programmes and channels where no guide information is available. Defaults to “No information”
    * ------------------ ----------------------------------------------------------------------------------------------------------------------------------
    * no_synopsis       |   Text string used as the synopsis for programmes where no guide information is available. Defaults to “No further information
    *                   |   available”
    * ------------------ ----------------------------------------------------------------------------------------------------------------------------------
    * manual_recording  |   Text string used to identify a manual recording. Defaults to “Manual Recording”
    * -----------------------------------------------------------------------------------------------------------------------------------------------------
    *
    * -value: The new value for the system text string.
    *
    */
    setText(key, value) {
        this._keys[key] = value;
    }

}
