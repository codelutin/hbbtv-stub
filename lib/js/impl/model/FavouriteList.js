/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * The FavouriteList class represents a list of favourite channels. See Annex K for the definition of the collection
 * template. In addition to the methods and properties defined for generic collections, the FavouriteList class supports
 * the additional properties and methods defined below.
 * In order to preserve backwards compatibility with already existing DAE content the JavaScript toString() method
 * SHALL return the FavouriteList.id for FavouriteList objects.
 */
class FavouriteList extends Array {

    constructor(...args) {
        super(...args);
    }
}
