/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/* Description;
 * The FavouriteListCollection class represents a collection of FavouriteList objects. See Annex K for the
 * definition of the collection template. In addition to the methods and properties defined for generic collections, the
 * FavouriteListCollection class supports the additional methods defined below.
 */
class FavouriteListCollection extends Collection {

    constructor(...args) {
        super(...args);
    }

    /*
    * Description:
    * Return the first favourite list in the collection with the given favListID.
    *
    * Arguments:
    * - favID: The ID of a favourite list.
    *
    * Return: FavouriteList
    */
    getFavouriteList(favID) {

    }


}
