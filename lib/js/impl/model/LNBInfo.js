/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * The LNBInfo object provides details on the LNB attached to a tuner.
 * Setting any of the properties in this class results in an immediate update of
 * the LNB configuration that is active for the associated Tuner.
 * The LNB configuration is stored persistently.
 */
class LNBInfo {

    constructor(defaultProperties) {
        /*
         * A universal LNB that has two local oscillator frequency
         * settings available. The selection between the frequencies is
         * done by the presence of a 22 kHz control signal.
         */
        this.DUAL_LO_FREQ_LNB = 30;

        /*
         * Only a single local oscillator frequency is available in the LNB.
         */
        this.SINGLE_LO_FREQ_LNB = 31;

        /*
         * Description:
         * The type of LNB connected to the frontend.
         * Valid values are listed in section 7.3.11.1.
         *
         * Type: Integer
         */
        this.lnbType = null;

        /*
         * Description:
         * The low or only, if a single local oscillator frequency LNB is used,
         * LNB local oscillator frequency in MHz.
         *
         * Type: Number
         */
        this.lnbLowFreq = null;

        /*
         * Description:
         * If a dual local oscillator frequency LNB is used this is
         * the high LNB local oscillator frequency in MHz. If a single
         * local oscillator frequency LNB is used this argument shall be set to 0.
         *
         * Type: Number
         */
        this.lnbHighFreq = null;

        /*
         * Description:
         * Indicates the frequency (in MHz) when to switch between the high-
         * and low-band oscillator frequencies (lnbLowFreq and
         * lnbHighFreq respectively).
         *
         * Type: Number
         */
        this.crossoverFrequency = null;

        /*
         * Description:
         * Indicates the lowest frequency, in MHz, that the LNB can be used for.
         *
         * Type: Number
         */
        this.lnbStartFrequency = null;

        /*
         * Description:
         * Indicates the highest frequency, in MHz, that the LNB can be used for.
         *
         * Type: Number
         */
        this.lnbStopFrequency = null;

        /*
         * Description:
         * Indicates the orbital position of the satellite in degrees, negative
         * value for west, positive value for east.
         * For example, Astra 19.2 East would have orbitalPosition 19.2.
         * Thor 0.8 West would have orbitalPosition -0.8.
         * This property, if provided, will be used to select a Tuner instance
         * (when scanning and tuning). Setting any value which is not a valid
         * orbital position (an absolute value greater than 180) indicates
         * that the orbital position need not be considered when using
         * the associated tuner.
         *
         * Type: Number
         */
        this.orbitalPosition = null;

        OipfUtils.initProperties.call(this, defaultProperties);
    }

}
