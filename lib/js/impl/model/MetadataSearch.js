/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
* Description:
* A MetadataSearch object represents a query of the metadata about available programmes. Applications can create
* MetadataSearch objects using the createSearch() method on the application/oipfSearchManager
* object. When metadata queries are performed on a remote server, the protocol used is defined in section 4.1.2.2 of
* [OIPF_META2].
*
* Each search consists of three steps:
* 1. Definition of the query. The application creates a MetadataSearch object, and either creates its associated
*    Query object, or sets a query using the findProgrammesFromStream() method, and sets any applicable
*    constraints and result ordering.
*
* 2. Acquisition of results. The OITF acquires some or all of the items that match the specified query and constraints, and
*    caches the requested subset of the results. This is typically triggered by a call to getResults().
*
* 3. Retrieval. The application accesses the results via the SearchResults class.
*    The MetadataSearch and SearchResults classes work together to manage an individual search. For every search,
*    the MetadataSearch object and its corresponding SearchResults object SHALL be in one of three states as
*    described in table below:
*
* ---------------------------------------------------------------------------------------------------------------------------------------------------------------
* State     |                                                                  Description
* ---------- ----------------------------------------------------------------------------------------------------------------------------------------------------
* Idle      |   The search is idle; no results are available. This is the initial state of the search. In this
*           |   state, the application can set or modify the query, constraints or ordering rules that are
*           |   applied to the search.
*           |   No search results are available in this state – any calls to SearchResults.item()
*           |   SHALL return undefined and the values of the length and totalSize properties on the
*           |   SearchResults object SHALL return zero. Any search results that have been cached by
*           |   the terminal SHALL be discarded when the Idle state is entered.
*           |   Calling the SearchResults.getResults() method SHALL cause a state transition to
*           |   the Searching state.
* ---------- ---------------------------------------------------------------------------------------------------------------------------------------------------
* Searching |   Results are being retrieved and are not yet available to applications.
*           |
*           |   If the terminal has not previously cached the full set of search results, the terminal
*           |   performs the search to gather the requested results.
*           |
*           |   If a new version of the metadata is detected (e.g. due to an EIT update) while the search
*           |   is in this state, results SHALL be retrieved from either the new or original version of the
*           |   metadata but SHALL NOT be retrieved from a combination of the two versions.
*           |
*           |   Calls to SearchResults.item() SHALL return undefined.
*           |
*           |   Any modification of the search parameters (e.g. changing the query or adding/removing
*           |   constraints, or calling findProgrammesFromStream() ) by the application SHALL stop
*           |   the current search and cause a transition to the Idle state. The terminal SHALL dispatch a
*           |   MetadataSearch event with state =3.
*           |
*           |   When all requested results have been found, the terminal SHALL dispatch a
*           |   MetadataSearch event with state =0 and a state transition to the Found state SHALL
*           |   occur.
*           |
*           |   If the search cannot be completed due to a lack of resources or any other reason, the
*           |   terminal SHALL dispatch a MetadataSearch event with state =4 and a state transition to
*           |   the Idle state SHALL occur.
*           |
*           |   Calls to the SearchResults.getResults() method SHALL abort the retrieval of search
*           |   results and attempt to retrieve the newly-requested set of results instead.
*           |
*           |   NOTE: Calling getResults() when in the searching state may be used to fetch a group
*           |   of items starting at a different offset or with a different count.
* ---------- -------------------------------------------------------------------------------------------------------------------------------------------------------
* Found     |   Search results are available and can be retrieved by applications. The data exposed via
*           |   the SearchResults.item() method is static and never changes as a result of any
*           |   updates to the underlying metadata database until SearchResults.getResults() is
*           |   next called.
*           |
*           |   If a new version of the metadata is detected (e.g. due to an EIT update), a
*           |   MetadataUpdate event is dispatched with action =1. Subsequent calls to
*           |   SearchResult.getResults() SHALL return results based on the updated metadata.
*           |
*           |   Calls to SearchResults.getResults() SHALL cause a state transition to the
*           |   Searching state.
*           |
*           |   Any modification of the search parameters (e.g. changing the query or adding/removing
*           |   constraints, or calling findProgrammesFromStream() ) by the application SHALL cause
*           |   the current set of results to be discarded and SHALL cause a transition to the Idle state.
*           |   The terminal SHALL dispatch a MetadataSearch event with state =3.
* -------------------------------------------------------------------------------------------------------------------------------------------------------------------
*
* The findProgrammesFromStream() method acts as a shortcut for setting a query and a set of constraints on the
* MetadataSearch object . Regardless of whether the query and constraints are set explicitly by the application or via
* findProgrammesFromStream() , results are retrieved using the getResults() method.
*
* Changes to the search parameters (e.g. changing the query or adding/removing constraints or modifying the search target,
* or calling findProgrammesFromStream() ) SHALL be applied when the getResults() method on the
* corresponding SearchResults object is called. Due to the nature of metadata queries, searches are asynchronous and
* events are used to notify the application that results are available. MetadataSearch events SHALL be targeted at the
* application/oipfSearchManager object.
*
* The present document is intentionally silent about the implementation of the search mechanism and the algorithm for
* retrieving and caching search results except where described in Table 7 above. When performing a search, the receiver
* MAY gather all search results and cache them (or cache a set of pointers into the full database), or gather only the subset
* of search results determined by the getResults() parameters, or take an alternative approach not described here.
*/
class MetadataSearch {

    constructor(searchTarget, searchManager) {

        this._searchManager = searchManager;
        /*
        * Description :
        * The target(s) of the search. Valid values are:
        *
        * -----------------------------------------------------------------
        * Value | Description
        * ------ ----------------------------------------------------------
        * 1     | Metadata relating to scheduled content SHALL be searched.
        * 2     | Metadata relating to on-demand content SHALL be searched.
        *
        * These values SHALL be treated as a bitfield, allowing searches to be carried out across multiple search
        * targets.
        *
        * Visibility: readonly
        */
        this.searchTarget = searchTarget;

        this._currentQuery = null;

        /*
        * Description:
        * The subset of search results that has been requested by the application.
        *
        * Visibility Type: readonly SearchResults
        */
        this.result = new SearchResults(this);
        this._constraints = {
            channels: [],
            ratings: []
        };
        this._ordering = [];

        this._fireEventModified = this._searchManager
            ._fireEventModified.bind(this._searchManager);
    }

    /*
    * Description:
    * Set a query and constraints for retrieving metadata for programmes from a given channel
    * and given start time from metadata contained in the stream as defined in section 4.1.3 of
    * [OIPF_META2]. Setting the search parameters using this method will implicitly remove any
    * existing constraints, ordering or queries created by prior calls to methods on this object.
    * This method does not cause the search to be performed; applications must call
    * getResults() to retrieve the results.
    *
    * Arguments:
    * - channel: The channel for which programme information should be found.
    *
    * - startTime: The start of the time period for which results should be returned measured in
    * seconds since midnight (GMT) on 1/1/1970. The start time is inclusive; any
    * programmes starting at the start time, or which are showing at the start time,
    * will be included in the search results. If null , the search will start from the
    * current time.
    *
    * -count: Optional argument giving the maximum number of programmes for which
    * information should be fetched. This places an upper bound on the number of
    * results that will be present in the result set – for instance, specifying a value of
    * 2 for this argument will result in at most two results being returned by calls to
    * getResults() even if a call to getResults() requests more results.
    * If this argument is not specified, no restrictions are imposed on the number of
    * results which may be returned by calls to getResults().
    *
    */
    findProgrammesFromStream(channel, startTime, count) {
        var parameters = arguments;
        if (parameters.length > 3) {
            throw new TypeError("Excessive number of arguments.");
        }

        if (parameters.length < 1) {
            throw new TypeError("Insufficient number of arguments.");
        }

        if (!(channel instanceof Channel) ||
            (startTime && !Number.isInteger(startTime)) ||
            (count && !Number.isInteger(count))) {
            throw new TypeError("This function cannot be called with these arguments.");
        }

        this._abortIfNecessary();

        console.log("[INFO]: findProgrammesFromStream [IN]");
        this.addChannelConstraint(channel);
        this._currentQuery = new Query("startTime", 0, startTime, count);
        console.log("[INFO]: findProgrammesFromStream [Out]");
    }

    /*
    * Description:
    * Set the query terms to be used for this search,
    * discarding any previously-set query terms.
    * Setting the search parameters using this method will implicitly remove
    * any existing constraints, ordering or queries created
    * by prior calls to methods on this object.
    *
    * Arguments:
    * -query: The query terms to be used.
    *
    */
    setQuery(query) {
        var parameters = arguments;

        if (parameters.length > 1) {
            throw new TypeError("Excessive number of arguments.");
        }

        if (parameters.length < 1) {
            throw new TypeError("Insufficient number of arguments.");
        }

        if (!(query instanceof Query)) {
            throw new TypeError("This function cannot be called with this argument.");
        }

        this._abortIfNecessary();
        this._currentQuery = query;
    }

    /*
     * Description:
     * Constrain the search to only include results from
     * the specified channel(s).
     * If a channelconstraint has already been set, subsequent calls to
     * addChannelConstraint() SHALL add the specified channel(s) to the list of
     * channels from which results should be returned.
     * For CoD searches, adding a channel constraint SHALL have no effect.
     *
     * Arguments:
     * -channels: The channel(s) from which results SHALL be returned.
     * If the value of this argument is null, any existing channel
     * constraint SHALL be removed.
     *
     */
    addChannelConstraint(channels) {
        var parameters = arguments;

        if (parameters.length > 1) {
            throw new TypeError("Excessive number of arguments.");
        }

        if (parameters.length < 1) {
            throw new TypeError("Insufficient number of arguments.");
        }

        if (!channels) {
            this._constraints.channels.length = 0;

        } else if (!(channels instanceof ChannelList) &&
                   !(channels instanceof Channel)) {

            throw new TypeError("This function cannot be called with this argument.");

        }

        this._abortIfNecessary();

        if (this._constraints.channels) {
            this._constraints.channels =
            this._constraints.channels.concat(channels);

        }
    }

    /*
     * Description:
     * Description Constrain the search to only include results whose
     * parental rating value is below the specified threshold.
     *
     * Arguments:
     * -scheme: The parental rating scheme upon which the constraint
     * SHALL be based. If the value of this argument is null,
     * any existing parental rating constraints SHALL be cleared.
     *
     * -threshold: The threshold above which results SHALL NOT be returned.
     * If the value of this argument is null,
     * any existing constraint for the specified parental
     * rating scheme SHALL be cleared.
     */
    addRatingConstraint(scheme, threshold) {
        this._abortIfNecessary();

        if (scheme && threshold) {
            var rating = {
                scheme: scheme,
                threshold: threshold
            };

            this._constraints.ratings &&
            this._constraints.ratings.push(rating);

        } else if (this._constraints.ratings) {
            this._constraints.ratings.length = 0;
        }
    }

    /*
     * Description:
     * Description Constrain the search to only include results whose
     * parental rating value is below the specified threshold.
     */
    addCurrentRatingConstraint() {

    }

    /*
     * Description:
     * Create a metadata query for a specific value in a specific
     * field within the metadata. Simple queries MAY be combined to
     * create more complex queries. Applications SHALL follow the
     * JavaScript type conversion rules to convert non-string values
     * into their string representation, if necessary.
     */
    createQuery(field, comparison, value) {
        var parameters = arguments;
        if (parameters.length > 3) {
            throw new TypeError("Excessive number of arguments.");
        }

        if (!(typeof field === "string") ||
            !Number.isInteger(comparison) ||
            (!Number.isInteger(value) && !(typeof value === "string"))) {

            throw new TypeError("This function cannot be called with these arguments.");

        }
        return new Query(field, comparison, value);
    }

    /*
     * Description:
     * Set the order in which results SHOULD be returned in future.
     * Any existing search results SHALL not be re-ordered. Subsequent calls
     * to orderBy() will apply further levels of ordering within the order
     *  defined by previous calls.
     *  For example:
     *      orderBy("ServiceName", true);
     *      orderBy("PublishedStart", true);
     * will cause results to be ordered by service name and then by start time
     * for results with the same channel number.
     *
     * Arguments:
     * -field: The name of the field by which results SHOULD be sorted.
     * A value of null indicates that any currently-set order SHALL be cleared
     * and the default sort order should be used.
     *
     * -ascending: Flag indicating whether the results SHOULD
     * be returned in ascending or descending order.
     */
    orderBy(field, ascending) {
        var parameters = arguments;
        if (parameters.length !== 2) {
            throw new TypeError("Excessive number of arguments.");
        }

        if (!(typeof field === "string") &&
                field &&
                Number.isInteger(field) ||
            !(typeof ascending === "boolean")) {
            throw new TypeError("This function cannot be called with these arguments.");
        }

        if (this._ordering) {
            this._ordering.push({
                field: field,
                ascending: ascending
            });
        }
    }

    _removeConstraints() {
        var keys = Object.keys(this._constraints);
        for (var i = 0, l = keys.length; i < l; i++) {
            var constraint = this._constraints[keys[i]];
            if (constraint instanceof Array) {
                constraint.length = 0;
            }
        }
    }

    _removeQuery() {
        this._currentQuery = null;
    }

    _removeOrdering() {
        this._ordering = null;
    }

    _abortIfNecessary() {
        if (this.result._timerManager.timer) {
            this.result.abort();
            this._fireEventModified(this);
        }
    }

}
