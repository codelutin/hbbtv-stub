/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

class ModelFactory {

    constructor(metadataObject) {
        this.metadataObject = metadataObject;
        this.metadataManager = null;
        this.xmlTvData = null;
//        this.createXmlTvData();
//        this.createMetadataManager();
        this.capabilitiesProperties = new CapabilitiesProperties();
    }

    createXmlTvData() {
        console.log("Method createXmlTvData");
        return this.getProperty("xmlTvData") ||
                this.setProperty("xmlTvData",
                    new XmlTvData(this.metadataObject));
    }

    getXmlTvData() {
        console.log("Method createXmlTvData");
        return this.getProperty("xmlTvData");
    }

    /*
     * FIXME: Yannis - 06/05/2015 - Get a method to find an available number for
     * randomIndex.
     */
    getAProgrammeFromXmlTvData() {
        console.log("Method createXmlTvData");

        var randomIndex = OipfUtils.getRandomNumberBetweenMinMax(0,
            this.xmlTvData.programmes.length);

        if (this.xmlTvData && this.xmlTvData.programmes[randomIndex]) {
            return this.xmlTvData.programmes[15];
        }
    }

    createScheduledRecording() {
        console.log("Method createScheduledRecording");
        return new ScheduledRecording(this.getAPrgrammeFromXmlTvData());
    }

    createRecording() {
        console.log("Method createRecording");
        return new Recording(this.createScheduledRecording());
    }

    createMetadataManager() {
        console.log("Method createMetadataManager");

        if (this.getProperty("metadataManager")) {
            return this.getProperty("metadataManager");
        }

        this.setProperty("metadataManager", new MetadataManager());
        this.getProperty("metadataManager").addMetadata(this.getXmlTvData());

        return this.getProperty("metadataManager");
    }

    getXmlParser() {
        return new XmlParser();
    }

    getMetadataManager() {
        console.log("Method getMetadataManager");
        return this.getProperty("metadataManager");
    }

    getProperty(propertyName) {
        console.log("Method getProperty");
        return this[propertyName];
    }

    setProperty(propertyName, value) {
        console.log("Method setProperty");
        this[propertyName] = value;
    }

    getCapabilitiesProperties() {
        return this.capabilitiesProperties;
    }

}
