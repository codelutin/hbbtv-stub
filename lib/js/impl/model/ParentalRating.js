/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * A ParentalRating object describes a parental rating value for a programme or
 * channel. The ParentalRating object identifies both the rating scheme in use,
 * and the parental rating value within that scheme. In case of a BCG the values
 * of the properties in this object will be read from the ParentalGuidance
 * element that is the child of a programme’s BCG description.
 */
class ParentalRating {

    constructor(name, scheme, value, labels, region) {
        /*
         * Visibility Type: readonly String
         */
        this.name = name;

        /*
         * Description:
         * Unique name identifying the parental rating guidance scheme to which this
         * parental rating value refers.
         *
         * Valid strings include:
         * - the URI of one of the MPEG-7 classification schemes representing a
         * parental rating scheme as defined by the “ uri ” attribute of one of the
         * parental rating <ClassificationScheme> elements in [MPEG-7].
         *
         * - the string value “ urn:oipf:GermanyFSKCS ” to represent the GermanyFSK
         * rating scheme as defined in [OIPF_META2].
         *
         * - the string value “ dvb-si ”: this means that the scheme of a minimum
         * recommended age encoded as per [EN 300 468], is used to represent the
         * parental rating values.
         *
         * Visibility Type: readonly String
         */
        this.scheme = scheme;

        /*
         * Visibility Type: readonly Integer
         */
        this.value = value;

        /*
         * Visibility Type: readonly Integer
         */
        this.labels = labels;

        /*
         * Visibility Type: readonly String
         */
        this.region = region;
    }

}
