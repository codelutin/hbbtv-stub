/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * typedef Collection<ParentalRating> ParentalRatingCollection
 * A ParentalRatingCollection represents a collection of parental rating values.
 * See Annex K for the definition of the collection template. In addition to
 * the methods and properties defined for generic collections,
 * the ParentalRatingCollection class supports the additional properties and
 * methods defined below.
 */
class ParentalRatingCollection extends Collection {

    constructor(...args) {
        super(...args);
    }

    /*
     * Description:
     * Creates a ParentalRating object instance for a given parental rating
     * scheme and parental rating value, and adds it to
     * the ParentalRatingCollection for a programme or channel.
     *
     * Arguments:
     * - scheme A unique string identifying the parental rating scheme to which
     * this value refers. See property scheme in section 7.9.4.1 for more
     * information about possible values.
     *
     * -name: A string representation of the parental rating value. See property
     * name in section 7.9.4.1 for more information about possible values.
     * Values are not case sensitive.
     *
     * -value: The parental rating value represented as an Integer. See property
     *  value in section 7.9.4.1 for more information about possible values.
     *
     * -labels: A set of content rating labels that may provide additional
     * information about the rating. See property labels in section 7.9.4.1 for
     * more information about possible values.- region: The region to which the
     * parental rating value applies as an alpha-2 region code as defined in
     * ISO 3166-1. The value of this argument must be null or undefined if no
     * specific region has been identified. Values are not case sensitive.
     */
    addParentalRating(scheme, name, value, labels, region) {
        var newParentalRating =
                new ParentalRating(scheme, name, value, labels, region);

        this.push(newParentalRating);
    }
}
