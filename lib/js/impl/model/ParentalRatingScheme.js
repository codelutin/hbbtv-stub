/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * typedef Collection<String> ParentalRatingScheme
 * A ParentalRatingScheme describes a single parental rating scheme that may be
 * in use for rating content, e.g. the MPAA or BBFC rating schemes. It is a
 * collection of strings representing rating values, which next to
 * the properties and methods defined below SHALL support the array notation
 * to access the rating values in this collection. For the natively OITF
 * supported parental rating systems the values SHALL be ordered by the OITF
 * to allow the rating values to be compared in the manner as defined for
 * property threshold for the respective parental rating system.
 * Using a threshold as defined in this API may not necessarily be the proper
 * way in which parental rating filtering is applied on the OITF,
 * e.g. the US FCC requirements take precedence for device to be imported
 * to the US. The parental rating schemes supported by a receiver MAY vary
 * between deployments. See Annex K for the definition of the collection
 * template. In addition to the methods and properties defined for generic
 * collections, the ParentalRatingScheme class supports the additional
 * properties and methods defined below.
 */
class ParentalRatingScheme extends Collection {

    /*
     * FIXME:
     * - Yannis 15/05/2015 Initialization of property threshold must be bone,
     * when name unequal to "dvb-si" but i don't know with what values.
     */
    constructor(name, values) {
        super();
        this._iconUri = [];
        this.name = name;
        this.threshold = null;
        this._additionnalProcessing(name, values);
    }

    /*
     * Description:
     * Return the index of the rating represented by attribute ratingValue
     * inside the parental rating scheme string collection, or -1 if the rating
     * value cannot be found in the collection.
     *
     * Argument:
     * - ratingValue: The string representation of a parental rating value.
     * See property name in section 7.9.1.1 for more information about possible
     * values. Values are not case sensitive.
     */
    indexOf(ratingValue) {

        for (var i = 0, li = this.length; i < li; i++) {
            var rating = this[i];

            if (ratingValue == rating) {
                return i;
            }
        }

        return -1;
    }

    /*
     * Description:
     * Return the URI of the icon representing the rating at index in the rating
     * scheme, or undefined if no item is present at that position. If no icon
     * is available, this method SHALL return null.
     *
     * Argument:
     * - index: The index of the parental rating scheme.
     */
    iconUri(index) {
        return this._iconUri[index];
    }

    _additionnalProcessing(name, values) {
        switch (name) {
            case "dvb-si":
            break;

            default:
                var ratingValues = values.split(",");
                ratingValues && this._addSeveralRatingValues(ratingValues);
        }
    }

    _addRatingValue(ratingValue) {
        this.push(ratingValue);
    }

    _addSeveralRatingValues(ratingValues) {
        for (var i = 0, li = ratingValues.length; i < li; i++) {
            var currentRatingValue = ratingValues[i];
            this.addRatingValue(currentRatingValue);
        }
    }

}
