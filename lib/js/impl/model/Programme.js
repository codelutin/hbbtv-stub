/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
* Description:
*
* The Programme class represents an entry in a programme schedule.
* Note: as described in the record( Programme programme ) method of the
* application/oipfRecordingScheduler object, only the programmeID property of the programme object is
* used to determine the programme or series that will be recorded. The other properties are solely used for annotation of
* the (scheduled) recording with programme metadata. The use of these metadata properties is optional. If such programme
* metadata is provided, it is retained in the ScheduledRecording object that is returned if the recording of the
* programme was scheduled successfully.
*
* Metadata extensions to Programme:
* The OITF SHALL extend the Programme class defined in section 7.16.2 with the properties and methods described
* below.
* This subsection SHALL apply for OITFs that have indicated <clientMetadata> with value “ true ” and a “ type ”
* attribute with values “ bcg ”, “ eit-pf ” or “ dvb-si ” as defined in section 9.3.7 in their capability profile.
*
* DVB-SI extensions to Programme:
* The following method SHALL be added to the Programme object, if the OITF has indicated support for accessing
* DVB-SI information, by giving the value “ true ” to element <clientMetadata> and the value “ dvb-si ” or
* “ eit-pf ” to the “ type ” attribute of that element as defined in section 9.3.7 in their capability profile.
* StringCollection getSIDescriptors( Integer descriptorTag, Integer descriptorTagExtension, Integer privateDataSpecifier)
*
* Recording extensions to Programme:
* The OITF SHALL support the following extensions to the Programme class.
* Clients supporting the recording management APIs defined in this section SHALL indicate this by adding the attribute
* " manageRecordings " to the <recording> element with a value unequal to ‘ none ’ in the client capability
* description as defined in section 9.3.3.
*/
class Programme {

    /*
     * FIXME:
     * Yannis - 30/04/2015 - The properties initialized here must be disappear
     * in future.
     *
     * @param {Object} initObject
     * @returns {undefined}
     */
    constructor(initObject) {
        /*
        * Description:
        * The short name of the programme, e.g. 'Star Trek: DS9'.
        */
        this.name = null;

        /*
        * Description:
        * The long name of the programme, e.g. 'Star Trek: Deep Space Nine'. If the long name is not available, this property will be undefined.
        */
        this.longName = null;

        /*
        * Description:
        * The description of the programme, e.g. an episode synopsis. If no description is available, this property will be undefined.
        */
        this.description = null;

        /*
        * Description:
        * The long description of the programme. If no description is available, this property will be undefined .
        */
        this.longDescription = null;

        /*
        * Description:
        * The long description of the programme. If no description is available, this property will be undefined .
        */
        this.startTime = null;

        /*
        * Description:
        * The duration of the programme (in seconds).
        */
        this.duration = null;

        /*
        * Description:
        * The identifier of the channel from which the broadcasted content is to be recorded. Specifies either a ccid or
        * ipBroadcastID (as defined by the Channel object in section 7.13.11)
        */
        this.channelID = null;

        /*
        * Description:
        * The episode number for the programme if it is part of a series. This property is undefined when the
        * programme is not part of a series or the information is not available.
        */
        this.episode = null;

        /*
        * Description:
        * If the programme is part of a series, the total number of episodes in the series. This property is undefined
        * when the programme is not part of a series or the information is not available.
        */
        this.totalEpisodes = null;

        /*
        * Description:
        * Flag indicating whether the programme has 3D video.
        *
        * Visibility Type: readonly Boolean
        */
        this.is3D = null;

        /*
        * Description:
        * The unique identifier of the programme or series, e.g., a TV-Anytime CRID (Content Reference Identifier).
        */
        this.programmeID = null;

        /*
        * Description:
        * The type of identification used to reference the programme, as indicated by one of the ID_* constants
        * defined above.
        */
        this.programmeIDType = null;

        /*
        * Description:
        * The TV-Anytime Instance Metadata ID for this programme.
        *
        * Visibility Type: readonly String
        */
        this.IMI = null;

        /*
        * Description:
        * A collection of parental rating values for the programme for zero or more parental rating schemes supported
        * by the OITF. For instances of the Programme class created by the createProgramme() method defined in
        * section 7.10.1.1, the initial value of this property (upon creation of the Programme object) is an instance of
        * the ParentalRatingCollection object (as defined in section 7.9.5) with length 0. Parental rating values
        * can be added to this empty readonly parental rating collection by using the addParentalRating() method
        * of the ParentalRatingCollection object. The ParentalRatingCollection is defined in section 7.9.5.
        * The related ParentalRating and ParentalRatingScheme objects are defined in section 7.9.4 and 7.9.2
        * respectively.
        * For instances of the Programme class returned through the metadata APIs defined in section 7.12 or through
        * the programmes property of the video/broadcast object defined in section 7.13.3, the initial value of this
        * property SHALL include the parental rating value(s) carried in the metadata or DVB-SI entry describing the
        * programme, if this information is included.
        * Note that if the service provider specifies a certain parental rating (e.g. PG-13) through this property and the
        * actual parental rating extracted from the stream says that the content is rated PG-16, then the conflict
        * resolution is implementation dependent.
        *
        * Visibility Type: readonly ParentalRatingCollection
        */
        this.parentalRatings = null;

        /*
        * Description:
        * The group CRIDs associated with this programme.
        *
        * Visibility Type: readonly StringCollection
        */
        this.groupCRIDs = null;

        /*
        * Description:
        * Reference to the broadcast channel where the programme is available.
        * The value of this field is derived from the serviceIDref attribute of the Schedule element that refers to this
        * programme.
        *
        * Visibility Type: readonly Channel
        */
        this.channel = null;

         /*
        * Description:
        * Reference to the broadcast channel where the programme is available.
        * The value of this field is derived from the serviceIDref attribute of the Schedule element that refers to this
        * programme.
        * Flag indicating whether the programme is blocked due to parental control settings or conditional access
        * restrictions.
        * The blocked and locked properties work together to provide a tri-state flag describing the status of a
        * programme. This can best be described by the following table:
        * -------------------------------------------------------------------------------------------------------
        * Description                                                                   |   blocked |   locked
        * ------------------------------------------------------------------------------ ----------- ------------
        * No parental control applies.                                                  |   false   |   false
        * ------------------------------------------------------------------------------ ----------- ------------
        * Item is above the parental rating threshold (or manually blocked); no PIN has |   true    |   true
        * been entered to view it and so the item cannot currently be viewed.           |           |
        * ------------------------------------------------------------------------------ ----------- ------------
        * Item is above the parental rating threshold (or manually blocked); the PIN has|   true    |   false
        * been entered and so the item can be viewed.                                   |           |
        * ------------------------------------------------------------------------------ ----------- ------------
        * Invalid combination – OITFs SHALL NOT support this combination                |   false   |   true
        * -------------------------------------------------------------------------------------------------------
        *
        * Visibility Type: readonly Boolean
        */
        this.blocked = null;

        /*
        * Description:
        * Flag indicating the type of show (live, first run, rerun, etc,).
        * The value of this property is determined by the child elements of the programme’s BroadcastEvent or
        * ScheduleEvent element from the Program Location Table. Values are determined as follows:
        * programme.
        * Flag indicating whether the programme is blocked due to parental control settings or conditional access
        * restrictions.
        * The blocked and locked properties work together to provide a tri-state flag describing the status of a
        * programme. This can best be described by the following table:
        * ----------------------------------------------------------------------------------
        * Value |                                           Description
        * ----------------------------------------------------------------------------------
        *   1   |  The programme is live; indicated by the presence of a Live element with
        *       |  a value attribute set to true.
        * ----------------------------------------------------------------------------------
        *   2   |  The programme is a first-run show; indicated by the presence of a
        *       |  FirstShowing element with a value attribute set to true.
        * ----------------------------------------------------------------------------------
        *   3   |  The programme is a rerun; indicated by the presence of a Repeat
        *       |  element with a value attribute set to true.
        * ----------------------------------------------------------------------------------
        *
        * If none of the above conditions are met, the default value of this field SHALL be 2.
        *
        * Visibility Type: readonly Boolean
        */
        this.showType = null;

        /*
        * Description:
        * Flag indicating whether subtitles or closed-caption information is available.
        * This flag SHALL be true if one or more BCG CaptionLanguage elements are present in this programme’s
        * description, false otherwise.
        *
        * Type: Boolean
        */
        this.subtitles = null;

        /*
        * Description:
        * Flag indicating whether the programme has high-definition video.
        * This flag SHALL be true if a VerticalSize element is present in the programme’s description and has a
        * value greater than 576, false otherwise.
        *
        * Type: Boolean
        */
        this.isHD = null;

        /*
        * Description:
        * Bitfield indicating the type of audio that is available for the programme.
        * The value of this field is determined by the NumOfChannels elements in a programme’s A/V attributes.
        * Values are determined as follows:
        *
        * -------------------------------------------------------------------------------------------------------
        * Value |                                           Description
        * -------------------------------------------------------------------------------------------------------
        *   1   |  A mono audio stream is available (at least one AvAttributes.AudioAttributes element is present
        *       |  which has a child NumOfChannels element whose value is 1).
        * -------------------------------------------------------------------------------------------------------
        *   2   |  A mono audio stream is available (at least one AvAttributes.AudioAttributes element is present
        *       |  which has a child NumOfChannels element whose value is 2).
        * -------------------------------------------------------------------------------------------------------
        *   4   |  A mono audio stream is available (at least one AvAttributes.AudioAttributes element is present
        *       |  which has a child NumOfChannels element whose value is 4).
        * -------------------------------------------------------------------------------------------------------
        * For programmes with multiple audio streams, these values may be ORed together.
        */
        this.audioType = null;

        /*
        * Description:
        * Flag indicating whether more than one audio language is available for the programme.
        * This flag SHALL be true if more than one BCG Language element is present in the programme’s
        * description, false otherwise.
        *
        * Visibility Type: Boolean
        */
        this.isMultilingual = null;

        /*
        * Description:
        * A collection of genres that describe this programme.
        * The value of this field is the concatenation of the values of any Name elements that are children of Genre
        * elements in the programme’s description.
        *
        * Type: StringCollection
        */
        this.genre = null;

        /*
        * Description:
        * Flag indicating whether the Programme has a recording associated with it (either scheduled, in progress, or
        * completed).
        *
        * Type: readonly Boolean
        */
        this.hasRecording = null;

        /*
        * Description:
        * Supported audio languages, indicated by their ISO.639-2 language codes as defined in [ISO 639-2].
        *
        * Type: StringCollection
        */
        this.audioLanguages = null;

        /*
        * Description:
        * Supported subtitle languages, indicated by their ISO.639-2 language codes as defined in [ISO 639-2].
        *
        * Visibility Type: readonly StringCollection
        */
        this.subtitleLanguages = null;

        /*
        * Description:
        * Flag indicating whether the current state of the parental control system prevents the programme from being
        * viewed (e.g. a correct parental control PIN has not been entered to allow the programme to be viewed).
        *
        * Visibility Type: readonly Boolean
        */
        this.locked = null;

        /*
        * Description:
        * If available, this property represents the recording associated with this programme (either scheduled, in-
        * progress or completed). Has value undefined if this programme has no scheduled recording associated with
        * it.
        *
        * Visibility Type: readonly ScheduledRecording
        */
        this.recording = null;

        OipfUtils.initProperties.call(this, initObject);
    }

    /*
    * Description:
    * Get the value of the field referred to by fieldId that is contained in the metadata for this
    * programme. If the field does not exist, this method SHALL return undefined .
    *
    * Arguments:
    * - fieldId: The name of the field whose value SHALL be retrieved.
    *
    * Return: String
    */
    getField(fieldId) {

    }

    /*
    * Description:
    * Get the contents of the descriptor specified by descriptorTag from the DVB SI EIT
    * programme's descriptor loop. If more than one descriptor with the specified tag is available for
    * the given programme, the contents of all matching descriptors SHALL be returned in the
    * order the descriptors are found in the stream.
    * The descriptor content bytes SHALL be encoded in a string whose characters shall be
    * restricted to the ISO Latin-1 character set. Each character in the string represents a byte of a
    * DVB-SI descriptor, such that a byte at position " i " in the descriptor is equal the Latin-1
    * character code of the character at position " i " in the string.
    * Described in the syntax of JavaScript: let desc[ ] be the byte array of a descriptor, in which
    * desc[0] is the descriptor_tag, then, the returned string (retval in the example below) is its
    * equivalent string, if :
    *       desc.length==retval.length and
    *       for each integer i : 0<=i<desc.length holds
    *       desc[i] == retval.charCodeAt(i) .
    * If the descriptor specified by descriptorTag and (optionally) descriptorTagExtension
    * and privateDataSpecifier does not exist, or if the metadata for this programme was
    * retrieved from a source other than DVB-SI, this method SHALL return null .
    * If metadata for this programme has not yet been retrieved, this method SHALL return
    * undefined . If the OITF supports the application/oipfSearchManager object as defined
    * in section 7.12.1, the OITF SHALL notify applications of the availability of additional metadata
    * via MetadataSearch events targeted at the application/oipfSearchManager object
    * used to retrieve the programme metadata.
    *
    * Arguments:
    * - descriptorTag: The descriptor tag as specified by [EN 300 468].
    *
    * - descriptorTag: An optional argument giving the descriptor tag extension as
    * specified by [EN 300 468]. This argument is mandatory when
    * descriptorTag is 0x7f and ignored in all other cases.
    *
    * - privateDataSpecifier: An optional argument giving the private_data_specifier as
    * specified by [EN 300 468]. If this argument is present, only
    * descriptors related to the identified specifier will be returned.
    *
    * Return: StringCollection
    */
    getSIDescriptors(descriptorTag, descriptorTagExtension, privateDataSpecifier) {

    }

}
