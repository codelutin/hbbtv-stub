/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * The Query class represents a metadata query that the user wants to carry out.
 * This may be a simple search, or a complex search involving Boolean logic.
 * Queries are immutable; an operation on a query SHALL return
 * a new Query object, allowing applications to continue referring to
 * the original query.
 */
class Query {

    constructor(field, comparison, value, count) {
        this._inverse = {
            0: 1,
            1: 0,
            2: 4,
            3: 5,
            4: 2,
            5: 3,
            6: 1
        };

        if (field == "startTime" && value == null) {
            this._type = "current";
        } else {
            this._type = "undifferent";
        }
        this._and = [];
        this._or = [];
        this._field = field;
        this._comparison = comparison;
        this._value = value;
        if (count) {
            this._count = count;
        }
    }

    and(query) {
        var queryClone = this._cloneKeys(this);
        queryClone._and && queryClone._and.push(query);

        return queryClone;
    }

    or(query) {
        var queryClone = this._cloneKeys(this);
        queryClone._or && queryClone._or.push(query);

        return queryClone;
    }

    _cloneKeys(query) {
        var keys = Object.keys(query);
        var newQuery = new Query();
        for (var i = 0, l = keys.length; i < l; i++) {
            newQuery[keys[i]] = query[keys[i]];
        }

        return newQuery;
    }

    not() {
        var queryClone = this._cloneKeys(this);
        queryClone._comparison = this._inverse[queryClone._comparison];
    }
}
