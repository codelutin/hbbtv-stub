/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

class Recording extends ScheduledRecording {

    /*
     * FIXME:
     * Yannis - 28/04/2015 - Initialize its own properties matching(not the inherited)
     * programme field contained by its inherited property channel
     */
    constructor(associatedScheduledRecording) {
        super(associatedScheduledRecording);

        this.uri = null;
        this.id = null;
        this.doNotDelete = null;
        this.saveDays = null;
        this.saveEpisodes = null;
        this.blocked = null;
        this.showType = null;
        this.subtitles = null;

        /*
         * Decsription:
         *
         * Visibilty Type: readonly StringCollection
         */
        this.subtitleLanguages = null;
        this.isHD = null;
        this.is3D = null;
        this.audioType = null;
        this.isMultilingual = null;

        /*
         * Decsription:
         *
         * Visibilty Type: readonly StringCollection
         */
        this.audioLanguages = null;

        /*
         * Decsription:
         *
         * Visibilty Type: readonly StringCollection
         */
        this.genres = null;
        this.recordingStartTime = null;
        this.recordingDuration = null;

        /*
         * Decsription:
         *
         * Visibilty Type: readonly BookmarkCollection
         */
        this.bookmarks = null;
        this.locked = null;

        this.isManual = true;
        associatedScheduledRecording &&
            OipfUtils.initProperties.call(this, associatedScheduledRecording);

        this.id = OipfUtils.createUUID();
    }

    _setRecordingStartTime(recordingStartTime) {
        this.recordingStartTime = recordingStartTime;
    }

    _setRecordingEndTime(recordingEndTime) {
        this.recordingEndTime = recordingEndTime;
    }

}

