/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * The ScheduledRecording object represents a scheduled programme in the system,
 * i.e. a recording that is scheduled but which has not yet started.
 * For group recordings (e.g. recording an entire series),
 * a ScheduledRecording object is also used to represent a “parent” recording
 * that enables management of the group recording without representing any of
 * the actual recordings in the group.
 * The values of the properties of a ScheduledRecording (except for
 * startPadding and endPadding ) are provided when the object is created
 * using one of the record() methods in section 7.10.1, for example by
 * using a corresponding Programme object as argument for the record() method,
 * and can not be changed for this scheduled recording object
 * (except for startPadding and endPadding).
 */
class ScheduledRecording {

    constructor(...parameters) {
        /*
         * This specification does not define values for these constants.
         * Implementations may use any values as long as the value of
         * each constant is unique.
         */
        this.RECORDING_SCHEDULED = 0;
        this.RECORDING_REC_STARTED = 1;
        this.RECORDING_REC_COMPLETED = 2;
        this.RECORDING_REC_PARTIALLY_COMPLETED = 3;
        this.RECORDING_ERROR = 4;
        this.ERROR_REC_RESOURCE_LIMITATION = 1712;
        this.ERROR_INSUFFICIENT_STORAGE = 918;
        this.ERROR_REC_UNKNOWN = 21;

        this.ID_TVA_GROUP_CRID = 2;

        /*
         * Description:
         * The state of the recording. Valid values are:
         * RECORDING_REC_STARTED
         * RECORDING_REC_COMPLETED
         * RECORDING_REC_PARTIALLY_COMPLETED
         * RECORDING_SCHEDULED
         * RECORDING_ERROR
         *
         * Visibility Type: readonly Integer
         */
        this.state = null;

        /*
         * Description:
         * If the state of the recording has changed due to an error,
         * this field contains an error code detailing the type of error.
         * This is only valid if the value of the state argument is RECORDING_ERROR
         * or RECORDING_REC_PARTIALLY_COMPLETED otherwise
         * this property SHALL be null . Valid values are:
         * ERROR_REC_RESOURCE_LIMITATION
         * ERROR_INSUFFICIENT_STORAGE
         * ERROR_REC_UNKNOWN
         *
         * Visibility Type: readonly Integer
         */
        this.error = null;

        this.scheduleID = null;
        this.customID = null;
        this.startPadding = null;
        this.endPadding = null;

        /*
         * Description:
         * Bitfield indicating which days of the week the recording SHOULD
         * be repeated. Values are as follows:
         * ------------------------------------------------------------------------
         * Day          |              Bitfield Value
         * ------------- ----------------------------------------------------------
         * Sunday            |   Indicates there are no analogue video outputs
         * ------------- ----------------------------------------------------------
         * Monday            |   Indicates platform support for the NTSC TV standard.
         * ------------- ----------------------------------------------------------
         * Tuesday            |   Indicates platform support for the PAL-BGH TV standard.
         * ------------- ----------------------------------------------------------
         * Wednesday            |   Indicates platform support for the SECAM TV standard.
         * ------------- ----------------------------------------------------------
         * Thursday            |   Indicates platform support for the PAL-M TV standard.
         * ------------- ----------------------------------------------------------
         * Friday           |   Indicates platform support for the PAL-N TV standard.
         * ------------------------------------------------------------------------
         * Saturday           |   Indicates platform support for the PAL-N TV standard.
         * ------------------------------------------------------------------------
         *
         * Visibility Type: readonly Integer
         *
         * FIXME: Yannis - 27/04/2015 - Express this properties as a bitfield.
         */
        this.repeatDays = null;

        /*
        * Description:
        * The short name of the programme, e.g. 'Star Trek: DS9'.
        *
         * Type: String
        */
        this.name = null;

        /*
        * Description:
        * The long name of the programme, e.g. 'Star Trek: Deep Space Nine'.
        * If the long name is not available, this property will be undefined.
        *
         * Type: String
        */
        this.longName = null;

        /*
        * Description:
        * The description of the programme, e.g. an episode synopsis. If no description is available, this property will be undefined.
        *
        * Type: String
        */
        this.description = null;

        /*
        * Description:
        * The long description of the programme. If no description is available, this property will be undefined.
        *
        * Type: String
        */
        this.longDescription = null;

        /*
        * Description:
        * The long description of the programme. If no description is available, this property will be undefined.
        *
        * Visibility Type: readonly Integer
        */
        this.startTime = null;

        /*
        * Description:
        * The duration of the programme (in seconds).
        *
        * Visibility Type: readonly Integer
        */
        this.duration = null;

        /*
        * Description:
        * Reference to the broadcast channel where the scheduled programme
        * is available.
        *
        * Visibility Type: reaonly Channel
        */
        this.channel = null;

        /*
         * Description:
         * true if the recording was scheduled using
         * oipfRecordingScheduler.recordAt() or using a terminal-specific approach
         * that does not use guide data to determine what to record, false otherwise.
         * If false , then any fields whose name matches a field in the Programme
         * object contains details from the programme guide on the programme
         * that has been recorded. If true , only the channel , startTime and
         * duration properties are required to be valid.
         *
         * Visibility Type: readonly Boolean
         */
        this.isManual = null;

        /*
        * Description:
        * The episode number for the programme if it is part of a series. This property is undefined when the
        * programme is not part of a series or the information is not available.
        *
        * Visibility Type: readonly Integer
        */
        this.episode = null;

        /*
        * Description:
        * If the programme is part of a series, the total number of episodes in the series. This property is undefined
        * when the programme is not part of a series or the information is not available.
        *
        * Visibility Type: readonly Integer
        */
        this.totalEpisodes = null;

        /*
        * Description:
        * The unique identifier of the programme or series, e.g.,
        * a TV-Anytime CRID (Content Reference Identifier).
        *
        * Visibility Type: readonly Integer
        */
        this.programmeID = null;

        /*
        * Description:
        * The type of identification used to reference the programme,
        * as indicated by one of the ID_* constants
        * defined above.
        *
        * Visibility Type: readonly Integer
        */
        this.programmeIDType = null;

        /*
        * Description:
        * A collection of parental rating values for the programme for zero or more
        * parental rating schemes supported by the OITF. The value of this property
        * is typically provided by a corresponding “ Programme ” object that is
        * used to schedule the recording and can not be changed for this scheduled
        * recording object. If no parental rating information is available for this
        * scheduled recording, this property is a ParentalRatingCollection
        * object (as defined in section 7.9.5) with length 0.
        * Note that if the parentalRating property contains a certain
        * parental rating (e.g. PG-13) and the broadcastchannel associated with
        * this scheduled recording has metadata that says that the content is
        * rated PG-16,then the conflict resolution is implementation dependent.
        * Note that this property was formerly called “parentalRating”
        * (singular not plural).
        *
        * Visibility Type: readonly ParentalRatingCollection
        */
        this.parentalRatings = null;

        /*
         * Description:
         * Application-specific information for this recording.
         * This value is information that the DAE application can set in order
         * to retain additional information on this scheduled recording. It is not
         * changed by the OITF. The OITF SHALL support values up to and including
         * 16 K Bytes in size. Strings longer than this MAY get truncated.
         *
         * Type: String
         */
        this.customMetadata = null;

        this.state = this.RECORDING_SCHEDULED;
        this.scheduleID = OipfUtils.createUUID();
        if (parameters.length == 1) {
            this._initFromProgramme.apply(this, parameters);
        } else {
            this._initFromFields.apply(this, parameters);
        }
    }

    _initFromProgramme(programme) {
        console.log("Method _initFromProgramme called.");
        var keys = Object.keys(programme);

        if (programme.programmeIDType == this.ID_TVA_GROUP_CRID) {

        } else {
            for (var i = 0, l = keys.length; i < l; i++) {
                this[keys[i]] = programme[keys[i]];
            }
            this.isManual = false;
        }
    }

    _initFromFields(startTime, duration, repeatDays, channelID) {
        console.log("Method _initFromFields called");
        this.startTime = startTime;
        this.duration = duration;
        this.repeatDays = repeatDays;
        this.isManual = true;
        this.programmeID = undefined;
        this.programmeIDType = undefined;
    }

    _setDuration(duration) {
        this.duration = duration;
    }

    _setStartTime(startTime) {
        this.startTime = startTime;
    }

    _setRepeatDays(repeatDays) {
        this.repeatDays = repeatDays;
    }

    _setState(state) {
        this.state = state;
    }

}
