/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
* Description:
* The SearchResults class represents the results of a metadata search. Since the result set may contain a large number
* of items, applications request a ‘window’ on to the result set, similar to the functionality provided by the OFFSET and
* LIMIT clauses in SQL.
*
* Applications MAY request the contents of the result in groups of an arbitrary size, based on an offset from the beginning
* of the result set. The data SHALL be fetched from the appropriate source, and the application SHALL be notified when
* the data is available.
* The set of results SHALL only be valid if a call to getResults() has been made and a MetadataSearch event notifying
* the application that results are available has been dispatched. If this event has not been dispatched, the set of results
* SHALL be empty (i.e. the value of the totalSize property SHALL be 0 and calls to item() SHALL return
* undefined ).
* In addition to the properties and methods defined below a SearchResults object SHALL support the array notation to
* access the results in this collection.
*/
class SearchResults extends Array {

    constructor(search) {
        super();

        /*
        * Description:
        * The number of items in the current window within the overall result set. The value of this property SHALL be
        * zero until getResults() has been called and a MetadataSearch event notifying the application that results
        * are available has been dispatched. If the current window onto the result set is in fact the whole result set then
        * length will be the same as totalSize. Otherwise length will be less than totalSize.
        *
        * Visibility: readonly
        */
        this.length = 0;

        /*
        * Description :
        * The current offset into the total result set.
        *
        * Visibility Type: readonly Integer
        */
        this.offset = null;

        /*
        * Description:
        * The total number of items in the result set.
        * The value of this property SHALL be zero until getResults() has been called and a MetadataSearch
        * event notifying the application that results are available has been dispatched.
        *
        * Visibility: readonly
        */
        this.totalSize = null;

        this._cachedResults = [];
        this._search = search;
        this._timerManager = new TimerManager();
        this._timeout = this._timerManager.createTimer.bind(this._timerManager, 0);
        this.totalSize = 0;

        this._methodToCall = {
            0: "equals",
            1: "notEquals",
            2: "superior",
            3: "superiorOrEquals",
            4: "inferior",
            5: "inferiorOrEquals",
            6: "contains"
        };

        this._fireEventUncompleted = this._search._searchManager._fireEventUncompleted.bind(this._search._searchManager);
        this._fireEventFinished = this._search._searchManager._fireEventFinished.bind(this._search._searchManager);
    }

    /*
     * Description:
     * Perform the search and retrieve the specified subset of the items that match the query.
     * Results SHALL be returned asynchronously. A MetadataSearch event with state=0
     * SHALL be dispatched when results are available.
     * This method SHALL always return false.
     *
     * Arguments:
     * - offset: The number of items at the start of the result set to be skipped before data is retrieved.
     *
     * - count: The number of results to retrieve.
     * FIXME: Yannis - 08/04/2015 - Verification about results ordering constraint
     */
    getResults(offset, count) {
        var parameters = arguments;
        if (parameters.length !== 2) {
            throw new TypeError("Insufficient number of arguments");
        }

        if (!Number.isInteger(offset) ||
            !Number.isInteger(count)) {
            throw new TypeError("This function cannot be called with these arguments.");
        }

        this.offset = offset;
        this._count = count;

        var programmes = this._search._searchManager._metadata;

        /*
         * Verify some conditions before beginning of
         * search.
         */
        if (this._search._currentQuery && programmes) {

            /*
             * A verification must be done on the  presence of cached results
             * in order to not restart a complete search when
             * that's not necessary.
             */
            if (this._cachedResults.length !== 0) {

                this._timeout()
                .then(this._getResultFromInterval.bind(this));

            } else {

                this._timeout()
                .then(this.begin.bind(this, programmes))
                .then(this._timeout)
                .then(this._getResultFromInterval.bind(this));
            }

        } else {
            this._fireEventUncompleted(this._search);
        }

        return false;
    }

    begin(programmes) {
        /*
         * When the search corresponds to current programme
         * the code differs from other usual query.
         */
        if (this._search._currentQuery._type === "current") {

            this._getCurrentProgramme(programmes);

        } else {

            this._getAnyProgramme(programmes);
        }
    }

    /*
    * Description:
    * Abort any outstanding request for results and remove any query, constraints or ordering
    * rules set on the MetadataSearch object that is associated with this SearchResults
    * object. Regardless of whether or not there is an outstanding request for results, items
    * currently in the collection SHALL be removed (i.e. the value of the length property SHALL
    * be 0 and any calls to item() SHALL return undefined ). All cached search results SHALL
    * be discarded.
    *
    */
    abort() {
        this._timerManager.clearTimer();
        this._search._removeConstraints();
        this._search._removeQuery();
        this._search._removeOrdering();
        this._cachedResults.length = 0;
        this.length = 0;
    }

    _getResultFromInterval() {
        this.length = 0;
        if (this._cachedResults) {

            for (var i = this.offset, l = this._count; i < l; i++) {
                var programme = this._cachedResults[i];
                if (programme) {
                    this.push(programme);

                } else {
                    console.log("[Info]No results found.");
                }
            }

            this._fireEventFinished(this._search);
            this.totalSize = this._cachedResults.length;
        }
    }

    _getAnyProgramme(programmes) {

        for (var i = 0, l = programmes.length; i < l; i++) {
            var programme = programmes[i];
            if (this._evaluateQuery(this._search._currentQuery, programme)) {

                var constraints = this._search.
                _constraints.channels;

                var next = true;

                if (constraints.length > 0 &&
                    OipfUtils.isPresent(constraints, programme.channel.name)) {

                    this._cachedResults.push(programme);
                    next = false;
                }

                if (next && constraints.length === 0) {

                    this._cachedResults.push(programme);

                }
            }
        }
    }

    _getCurrentProgramme(programmes) {
        var time = new Date().getTime() / 1000;
        var constraints = this._search.
        _constraints.channels;

        for (var i = 0, l = programmes.length; i < l; i++) {
            var programme = programmes[i];

            if (constraints.length > 0 &&
                OipfUtils.isPresent(constraints, programme.channel.name) &&
                programme.duration) {

                var stopTime = programme.startTime +
                programme.duration;

                if (time >= programme.startTime && time <= stopTime) {

                    this._cachedResults.push(programme);

                    i = l;
                }
            }
        }
    }

    /*
    * Description:
    * Return the item at position index in the collection of currently available results, or
    * undefined if no item is present at that position. This function SHALL only return objects
    * that are instances of Programme , CODAsset , CODFolder , or CODService .
    *
    * Arguments:
    * - index: The index into the result set.
    *
    */
    item(index) {
        return this[index];
    }

    _evaluateQuery(query, programme) {
        var field = this._getPropertyName(this._search._currentQuery._field);
        var comparison = this._search._currentQuery._comparison;
        var value = this._search._currentQuery._value;
        var methodName = this._methodToCall[comparison];

        if (query._and.length > 0 && query._or.length > 0) {

            return (OipfUtils[methodName].call(null, programme[field], value) && this._evaluateQueryLoop(query._and, programme))
                || (OipfUtils[methodName].call(null, programme[field], value)
                || this._evaluateQueryLoop(query._and, programme));

        } else if (query._and.length > 0 && query._or.length === 0) {

            return (OipfUtils[methodName].call(null, programme[field], value) && this._evaluateQueryLoop(query._and, programme));

        } else if (query._or.length > 0 && query._and.length === 0) {

            return (OipfUtils[methodName].call(null, programme[field], value) && this._evaluateQueryLoop(query._or, programme));

        } else if (query._and.length === 0 && query._or.length === 0) {
            return (OipfUtils[methodName].call(null, programme[field], value));
        }

    }

    _evaluateQueryLoop(query, programme) {
        for (var i = 0, l = query; i < l; i++) {
            if (!this._evaluateQuery(query, programme)) {
                return false;
            }
        }
    }

    _getPropertyName(field) {
        var pattern = /\w+.(\w+)/;
        var propertyName = pattern.exec(field);

        return propertyName[1];
    }

}
