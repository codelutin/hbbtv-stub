/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * The SignalInfo object provides details on the signal strength of the tuner.
 * If the tuner is not tuned to a transponder the all properties SHALL have
 * the value undefined .
 *
 */

class SignalInfo {

    constructor(defaultProperties) {
        OipfUtils.initProperties.call(this, defaultProperties);

        /*
         * Description:
         * Signal strength measured in dBm, for example -31.5dBm.
         *
         * Type: readonly Number
         */
        this.strength = null;

        /*
         * Description:
         * Signal quality with range from 0 to 100. Calculation of quality is a
         * function of ber and snr . The specification remains silent as to
         * how the calculation is made.
         *
         * Type: readonly Integer
         */
        this.quality = null;

        /*
         * Description:
         * Bit error rate.
         *
         * Type: readonly Integer
         */
        this.ber = null;

        /*
         * Description:
         * Signal to noise ratio (dB), for example 22.3dB.
         *
         * Type: readonly Number
         */
        this.snr = null;

        /*
         * Description:
         * True if the tuner is locked to a transponder.
         *
         * Type: readonly Boolean
         */
        this.lock = null;
    }
}

