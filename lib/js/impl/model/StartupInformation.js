/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

class StartupInformation {

    constructor(urlSource, url) {
        /*
         * Description:
         * The mechanism used to obtain the url property.
         * Any of the STARTUP_URL_* values defined in section 7.3.3.1 are valid.
         *
         * Visibility Type : readonly String
         */
        this.urlSource = urlSource;

        /*
         * Description:
         * The URL used at startup of the OITF.
         * If the urlSource property is STARTUP_URL_NONE then
         * the value of this property SHALL be NULL.
         * If the urlSource property is STARTUP_URL_PRECONFIGURED then
         * the value of this property SHALL be undefined.
         *
         * Visibility Type : readonly String
         */
        this.url = url;
    }
}
