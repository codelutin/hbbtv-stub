/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var APPLICATION_MANAGER_DAE_MIME_TYPE = "application/oipfApplicationManager";
var CAPABILITIES_DAE_MIME_TYPE = "application/oipfCapabilities";
var CHANNEL_CONFIG_TYPE = "channelConfig"; //Not a DAE MIME Type
var COD_MANAGER_DAE_MIME_TYPE = "application/oipfCodManager";
var COMMUNICATION_SERVICES_DAE_MIME_TYPE = "application/oipfCommunicationServices";
var CONFIGURATION_DAE_MIME_TYPE = "application/oipfConfiguration";
var DOWNLOAD_MANAGER_DAE_MIME_TYPE = "application/oipfDownloadManager";
var DOWNLOAD_TRIGGER_DAE_MIME_TYPE = "application/oipfDownloadTrigger";
var DRM_AGENT_DAE_MIME_TYPE = "application/oipfDrmAgent";
var GATEWAY_INFO_DAE_MIME_TYPE = "application/oipfGatewayInfo";
var MDTF_DAE_MIME_TYPE = "application/oipfMDTF";
var NOTIF_SOCKET_DAE_MIME_TYPE = "application/notifsocket";
var PARENTAL_CONTROL_MANAGER_DAE_MIME_TYPE = "application/oipfParentalControlManager";
var RECORDING_SCHEDULER_DAE_MIME_TYPE = "application/oipfRecordingScheduler";
var REMOTE_CONTROL_FUNCTION_DAE_MIME_TYPE = "application/oipfRemoteControlFunction";
var REMOTE_MANAGEMENT_DAE_MIME_TYPE = "application/oipfRemoteManagement";
var SEARCH_MANAGER_DAE_MIME_TYPE = "application/oipfSearchManager";
var STATUS_VIEW_DAE_MIME_TYPE = "application/oipfStatusView";
var VIDEO_BROADCAST_DAE_MIME_TYPE = "video/broadcast";
var VIDEO_MPEG_DAE_MIME_TYPE = "video/mpeg";
