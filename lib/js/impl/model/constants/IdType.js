/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var ID_ANALOG = 0;
var ID_DVB_C = 10;
var ID_DVB_S = 11;
var ID_DVB_T = 12;
var ID_DVB_SI_DIRECT = 13;
var ID_DVB_C2 = 14;
var ID_DVB_S2 = 15;
var ID_DVB_T2 = 16;
var ID_ISDB_C = 20;
var ID_ISDB_S = 21;
var ID_ISDB_T = 22;
var ID_ATSC_T = 30;
var ID_IPTV_SDS = 40;
var ID_IPTV_URI = 41;

