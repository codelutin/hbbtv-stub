/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var recordingConstants = {
    state: {}
};

recordingConstants.state.STARTED = 1;
recordingConstants.state.COMPLETED = 2;
recordingConstants.state.SCHEDULED = 7;
recordingConstants.state.REMOVED = 8;
recordingConstants.state.BEFORE_STARTED = 9;
recordingConstants.state.UPDATED = 10;
