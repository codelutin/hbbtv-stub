/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var videoBroadcastConstants = {
    timeShiftMode: {},
    state: {}
};

videoBroadcastConstants.timeShiftMode.OFF = 0;
videoBroadcastConstants.timeShiftMode.LOCAL = 1;
videoBroadcastConstants.timeShiftMode.NETWORK = 2;
videoBroadcastConstants.timeShiftMode.LOCAL_THEN_NETWORK = 3;

videoBroadcastConstants.state.UNREALIZED = 0;
videoBroadcastConstants.state.CONNECTING = 1;
videoBroadcastConstants.state.PRESENTING = 2;
videoBroadcastConstants.state.STOPPED = 3;
