/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
class ChannelHelper {

    constructor(jsonChannels) {
        this.map = {
            "display-name": "setName",
            icon: "setLogoUrl"
        };
        this.oipfChannels = [];
        this.jsonChannels = jsonChannels;

        this.channelDefaultConf = {
            name: "",
            channelType: "TYPE_TV",
            idType: 12,
            ccid: null,
            tunerID: null,
            onid: null,
            nid: null,
            tsid: null,
            sid: null,
            sourceID: null,
            freq: null,
            cni: null,
            majorChannel: null,
            minorChannel: null,
            dsd: "",
            favourite: false,
            favIDs: null,
            locked: false,
            manualBlock: true,
            ipBroadcastID: null,
            channelMaxBitRate: null,
            channelTTR: null,
            recordable: true,
            longName: null,
            description: null,
            authorised: null,
            genre: null,
            hidden: false,
            is3D: false,
            isHD: false,
            logoURL: ""
        }
    }

    getOipfChannels() {

        for (var i = 0, li = this.jsonChannels.length; i < li; i++) {
            var channelItem = this.jsonChannels[i];
            var channelConf = {}
            var channelItemKeys = Object.keys(channelItem);

            for (var j = 0, lj = channelItemKeys.length; j < lj; j++) {
                var channelKey = channelItemKeys[j];

                var channelKeyValue = channelItem[channelKey];

                var methodToCall = this.map[channelKey];

                if (methodToCall) {
                    this[methodToCall].call(this, channelKeyValue, channelConf);
                }
            }

            var oipfChannel = this.getDVBTChannel(channelConf);

            if (oipfChannel) {
                this.oipfChannels.push(oipfChannel);
            } else {
                console.log("Error in channel creation");
            }
        }

        return this.oipfChannels;
    }

    setName(name, channelConf) {
        channelConf.name = name || null;
    }

    setLogoUrl(logoObject, channelConf) {
        channelConf.logoURL = logoObject && logoObject.src || null;
    }

    getDVBTChannel(channelConf) {
        channelConf.onid = OipfUtils.generateFourDigit();
        channelConf.tsid = OipfUtils.generateFourDigit();
        channelConf.sid = OipfUtils.generateFourDigit();
        channelConf.idType = 12;

        return new Channel(channelConf);
    }

}

