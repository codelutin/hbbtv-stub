/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

class ChannelService {

    constructor(currentChannel, channelNumber, channelOrigin, channelList) {
        this.channelNumber = channelNumber;
        this.channelOrigin = channelOrigin;

        this.getNumber = OipfUtils.generateFourDigit.bind(null);

        this.channelList = new ChannelList();
        this.createChannelList(channelList);
        this.favouriteLists = new FavouriteListCollection();
        this.currentFavouriteList = new FavouriteList();
        this.setCurrentChannel(currentChannel);
    }

    createChannelList(channelList) {
        if (this.channelNumber > 0) {

            for (var i = 0, li = channelList.length; i < li; i++) {
                var channel = channelList[i];
                var newOipfChannel = new Channel(channel);
                this.channelList && this.channelList.push(newOipfChannel);

            }
        }
    }

    setCurrentChannel(name) {
        this.currentChannel = this.getChannelByName(this.channelList, name);

        if (!this.currentChannel && this.channelList) {
            var channel = this.channelList[0];

            if (channel) {
                this.currentChannel = channel;
            }
        }
    }

    getChannelByName(channelCollection, channelName) {
        for (var i = 0, li = channelCollection.length; i < li; i++) {
            if (channelCollection[i].name == channelName) {
                return channelCollection[i];
            }
        }
    }

    getChannelSimplyByName(channelName) {
        for (var i = 0, li = this.channelList.length; i < li; i++) {
            if (this.channelList[i].name == channelName) {
                return this.channelList[i];
            }
        }
    }

    prevChannel(channelCollection, channel) {
        return channelUtils.findChannel(channelCollection, channel, -1);
    }

    nextChannel(channelCollection, channel) {
        return channelUtils.findChannel(channelCollection, channel, 1);
    }

    getADvbTChannel(name) {
        return new Channel(ID_DVB_T,
        this.getNumber(),
        this.getNumber(),
        this.getNumber(),
        name);
    }

    getADvbTChannelWithConf(conf) {
        if (conf) {
            conf.idType = ID_DVB_T;
            conf.onid = this.getNumber();
            conf.tsid = this.getNumber();
            conf.sid = this.getNumber();

            return new Channel(conf);
        }
    }

    addChannel(channel) {
        if (channel && this.channelList) {
            this.channelList.push(channel);
        }
    }

}
