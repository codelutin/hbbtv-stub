/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
class ChannelUtils {

    static findChannel(channelCollection, channel, step) {
        for (var i = 0, l = channelCollection.length; i < l; i++) {

            if (channelCollection[i] == channel) {

                if ((i == 0) && (step == -1)) {
                    var index = channelCollection.length + step;

                } else {

                    if ((i == (channelCollection.length-1)) && (step == 1)) {
                        index = 0;

                    } else {
                        index = i + step;

                    }
                }
                return channelCollection[index];
            }
        }
        return null;
    };

    static isAValidDVBTChannel(channel) {
        return (channel.onid) && (channel.tsid) && (channel.sid);
    };

    /*
     * Description:
     * This method verify that a tuner is available to receive a channel with this "idType".
     * And return the first tuner found which match the "idType".
     * Actually all video broadcast will use the same tuner because there is only one and
     * this choice would be directed by parameter.
     *
     * ToDo :Normally the method should check if the tuner don't locked by another object but
     * no way to do that in the norm.
     *
     * Argument:
     * - idType : the type of tuner researched.
     *
     * Return: Tuner
     */
    static verifyASuitableTunerAvailable(idType, object) {
        var config = oipfObjectFactory.createConfigurationObject();
        var availableTuners = config.localSystem.tuners;

        for (var i = 0; i < availableTuners.length; i++) {
            var tunerIdTypes = availableTuners[i].idTypes;
            for (var j = 0; j < tunerIdTypes.length; j++) {
                var tuner = availableTuners[i];
                if (tunerIdTypes[j] == idType) {
                    return tuner;
                }
            }
        }

        return null;
    };

    static getChannelByDsd(channelCollection, dsd) {
        for (var i = 0, l = channelCollection.length; i < l; i++) {
            if (channelCollection[i].dsd == dsd) {
                return channelCollection[i];
            }
        }
    };

    /*
     * Description:
     * This method verify the validity of the terrestrial delivery system descriptor.
     * The argument will be compare to the dsd array byte defined either in
     * the transport stream or directly in the terminal.
     *
     * Arguments
     * - dsd: the delivery system descriptor string necessary to create a channel by intermediate of method
     * createChannelObject.
     */
    static isAValidTerrestrialDsd(dsd) {
        var dsdAttributes = dsd.split(",");

        if (TERRESTIAL_DELIVERY_SYSTEM_DESCRIPTOR.length == dsdAttributes.length) {

            for (var i = 0, l = TERRESTIAL_DELIVERY_SYSTEM_DESCRIPTOR.length; i < l; i++) {

                if (!(TERRESTIAL_DELIVERY_SYSTEM_DESCRIPTOR[i] == dsdAttributes[i])) {
                    return false;
                }
            }

            return true;
        }
    };

    static getChannelByIpBroadcastID(channelCollection, ipBroadcastID) {
        for (var i = 0, l = channelCollection.length; i < l; i++) {
            if (channelCollection[i].ipBroadcastID == ipBroadcastID) {
                return channelCollection[i];
            }
        }
    };

}

