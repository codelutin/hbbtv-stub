/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
class EventManager {
    constructor() {
    }

    addEventListener(type, listener, target) {
        var listenersList = target._listeners[type];

        if (listenersList) {
            listenersList.push(listener);
        } else {
            target._listeners[type] = [listener];
        }
    }

    fireEvent(event, target) {
        var listeners = target._listeners[event.type];

        listeners && listeners.forEach(function(listener) {
            console.log(">>fireEvent");

            OipfUtils.timeout(0)
                .then(listener.apply.bind(listener, target, event.detail));
        });

        var callback = target._callbacks[event.type];

        callback && callback.apply(target, event.detail);
    }

    removeEventListener(type, listener, target) {
        var listeners = target._listeners[type];

        if (listeners) {
            target._listeners[type] = listeners.filter(function(currentListener) {
                return currentListener != listener;
            });
        }
    }

    createCustomEvent(type, param) {
        return new CustomEvent(type, {detail: param});
    }
}
