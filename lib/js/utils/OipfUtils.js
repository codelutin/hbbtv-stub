/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
class OipfUtils {

    static initProperties(defaultProperties) {
        var keys = Object.keys(defaultProperties);
        for (var i = 0, l = keys.length; i < l; i++) {
            this[keys[i]] = defaultProperties[keys[i]];
        }
    }

    static timeout(time) {
        return new Timeout(time);
    }

    static equals(field1, field2) {
        return field1 == field2;
    }

    static notEquals(field1, field2) {
        return field1 != field2;
    }

    static superior(field1, field2) {
        return field1 > field2;
    }

    static superiorOrEquals(field1, field2) {
        return field1 >= field2;
    }

    static inferior(field1, field2) {
        return field1 < field2;
    }

    static inferiorOrEquals(field1, field2) {
        return field1 <= field2;
    }

    /*
     * Description:
     * Use for search of metadata, this method verify, if field1 is contained in field2.
     *
     * Arguments:
     * -field1: The String object which must be found into String object field2.
     * -field2: The String object which should contains the String object field1.
     *
     * Return: Boolean
     */
    static contains(field1, field2) {
        return field2.includes(field1);
    }

    static generateFourDigit() {

        return Math.floor(Math.random() * 9000) + 1000;
    }

    static getRandomNumberBetweenMinMax(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    static isPresent(list, element) {

        for (var i = 0, l = list.length; i < l; i++) {
            if (OipfUtils.equals(list[i].name, element)) {
                return true;
            }
        }
    }

    /*
     * Some code based on RFC 4122, section 4.4
     * (Algorithms for Creating a UUID from Truly Random or Pseudo-Random Number).
     */
    static createUUID() {
        // http://www.ietf.org/rfc/rfc4122.txt
        var s = [];
        var hexDigits = "0123456789abcdef";
        for (var i = 0; i < 36; i++) {
            s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
        }
        s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
        s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
        s[8] = s[13] = s[18] = s[23] = "-";

        var uuid = s.join("");
        return uuid;
    }

    /*
     * Description:
     * Should be use in each test method  in anticipation of tests executed on a tv platform.
     */
    static isMethodImplemented(object, methodName) {
        return object[methodName];
    }

    static upperCaseFirstChar(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    static logTest() {
        console.log.apply(console, arguments);
    }

}
