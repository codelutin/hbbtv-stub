/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
class ProgrammeHelper {

    constructor(channelService, channels) {
        this.map = {
            category: "genre",
            title: "name",
            start: "startTime",
            stop: "duration",
            "desc#text": "description",
            "episode-num#text": "episode",
            "title#text": "name",
            value: "parentalRatings",
            src: "_picture"
        };

        this.transformationData = {
            start: {
                methodTransformName: "millsecondsSinceJanuary1970",
                necessaryData: ["content"]
            },
            stop: {
                methodTransformName: "millsecondsSinceJanuary1970",
                necessaryData: ["content"]
            },
            category: {
                methodTransformName: "setCategory",
                necessaryData: ["content"]
            },
            value: {
                methodTransformName: "setParentalRating",
                necessaryData: ["content"]
            },
            src: {
                methodTransformName: "setProgrammePicture",
                necessaryData: ["content"]
            }
        }

        this.channelsLogoAndRealName = {};
        this.setChannelsLogoAndRealName(channels)

        this.channelService = channelService;
        this.programmes = [];
    }

    setChannelsLogoAndRealName(channelsArray) {
        var self = this;
        if (channelsArray) {
            channelsArray.forEach(function(item) {
                if (item.id) {
                    self.channelsLogoAndRealName[item.id] = item;
                }
            });
        }
    }
    /*
     * Description:
     * Don't take in charge the "credits" property of a xmlTv programmes for the moment.
     */
    initProgrammes(programmeList) {
        for (var i = 0, l = programmeList.length; i < l; i++) {
            var programme = {};
            var object = programmeList[i];
            var channelName = {
                value: ""
            };
            var self = this;
            var createProgramme = function(object, programme, parentKey) {
                var keys = Object.keys(object);
                for (var j = 0, l1 = keys.length; j < l1; j++) {
                    var key = keys[j];
                    var element = object[key];
                    if (element instanceof Object && !(element instanceof Array)) {
                        createProgramme(element, programme, key);
                    } else {
                        self.setProgrammeProperty(element, key, parentKey, programme, channelName);
                    }
                }

            };

            createProgramme(object, programme);
            var realChannelName = this.channelsLogoAndRealName[channelName.value]["display-name"];
            var channelLogoUrl = this.channelsLogoAndRealName[channelName.value].icon.src;

            if (realChannelName) {
                var channel = this.channelService.getChannelSimplyByName(realChannelName);

                if (!channel) {
                    channel = this.initChannel(realChannelName);

                    if (channel) {
                        this.channelService.addChannel(channel);
                    }
                }

                programme.channel = channel;
            }

            if (programme.startTime && programme.duration) {
                programme.duration = programme.duration - programme.startTime;
            }

            programme.programmeID = OipfUtils.createUUID();
            var programmeObject = new Programme(programme);
            this.programmes.push(programmeObject);
        }

        return this.programmes;
    }

    initChannel(name) {
        return this.channelService.getADvbTChannel(name);
    }

    setProgrammeProperty(element, key, parentKey, programme, channelName) {
        /*if (key == "start") {
            element = self.transformInSecondSince01011970(element);
        }
        if (key == "stop") {
            element = self.transformInSecondSince01011970(element);
        }*/
        var data = {
            content: element,
            key: key
        };

        if (key == "channel") {
            channelName.value = element;
        }

        if (key == "#text") {
            key = parentKey + key;
        }

        var mapping = this.map[key];

        /*if (mapping) {
            programme[mapping] = element;
        }*/

        if (mapping) {
            programme[mapping] = this.doTransformation(data);
        }

    }

    transformToDateObject(wrongDateFormat) {
//        var pattern = /(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})\s\+(\d{2})/;
        var pattern = /(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/;

        var tmpDate = wrongDateFormat;
        tmpDate = pattern.exec(tmpDate);

        var year = parseInt(tmpDate[1]);
        var month = parseInt(tmpDate[2]) - 1;
        var day = parseInt(tmpDate[3]);
        var hs = parseInt(tmpDate[4]);
        var mins = parseInt(tmpDate[5]);
        var secs = parseInt(tmpDate[6]);

        var date = new Date(year, month, day, hs, mins, secs);

        return date;
    }

    transformInSecondSince01011970(wrongDateFormat) {
        return this.transformToDateObject(wrongDateFormat).getTime() / 1000;
    }

    minutesToSeconds(minutes) {
        return minutes * 60;
    }

    doTransformation(data) {
        var key = data.key;

        var transformationData = this.transformationData[key];

        if (transformationData) {
            var methodTransformationParams =
                this.getMethodTransformationParameter(data, transformationData.necessaryData);

            var methodTransformationName = transformationData.methodTransformName;

            return this[methodTransformationName].apply(this, methodTransformationParams);
        }

        return data.content;
    }

    getMethodTransformationParameter(data, infoList) {
        var param = [];

        for (var i = 0, li = infoList.length; i < li; i++) {
            var infoName = infoList[i];

            var paramValue = data[infoName];

            param.push(paramValue);
        }

        return param;
    }

    setCategory(categoryList) {
        var category = new StringCollection();

        for (var i = 0, li = categoryList.length; i < li; i++) {
            var categoryItem = categoryList[i];

            categoryItem && category.push(categoryItem);
        }

        return category;
    }

    millsecondsSinceJanuary1970(date) {
        date = this.transformInSecondSince01011970(date);

        return date;
    }

    setParentalRating(rating) {
        var parentalRating = new ParentalRating("dvb-si", rating, "", "0", "");

        var parentalRatingCollection = new ParentalRatingCollection(parentalRating);

        if (parentalRating && parentalRatingCollection) {
            return parentalRatingCollection;
        }

        return undefined;
    }

    setProgrammePicture(url) {
        return url;
    }

}

