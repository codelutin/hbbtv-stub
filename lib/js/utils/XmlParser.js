/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

class XmlParser {

    constructor() {

        this.initParser();
        this.xmlDocument;
    }

    initParser() {
        if (window.DOMParser) {
            this.parser = new DOMParser();
        }
    }

    createAXmlStringDocument(defaultPropertiesObject) {
        console.log("Method createAXmlStringDocument called");
        var defaultProperties = defaultPropertiesObject.rearrangeDefaultProperties();
        var objectsNames = Object.keys(defaultProperties);
        var xmlStringDocument = "<" + defaultPropertiesObject.roots + ">";
        for (var i = 0, l = objectsNames.length; i < l; i++) {

            var currentObjectName = objectsNames[i];
            xmlStringDocument += "<" + currentObjectName;
            var currentObjectData = defaultProperties[currentObjectName];
            var currentAttributes = currentObjectData.attributes;

            if (currentAttributes) {
                xmlStringDocument = this.createAttributes(currentAttributes,
                        xmlStringDocument);
            }

            var currentObjectValue = currentObjectData.value;
            xmlStringDocument += ">" + currentObjectValue;
            xmlStringDocument += "</" + currentObjectName + ">";
        }
        xmlStringDocument += "</" + defaultPropertiesObject.roots + ">";
        console.log("Method createAXmlStringDocument completed");
        return xmlStringDocument;
    }

    createAttributes(currentAttributes, xmlStringDocument) {
        var currentAttributesNames = Object.keys(currentAttributes);
        for (var i = 0, l = currentAttributesNames.length; i < l; i++) {
            var currentAttributeName = currentAttributesNames[i];
            var currentAttributeValue = currentAttributes[currentAttributeName];
            xmlStringDocument += " " + currentAttributeName
                    + "='" + currentAttributeValue + "'";
        }
        return xmlStringDocument;
    }

    getXmlDocument(defaultProperties) {
        if (!this.parser) {
            this.initParser();
        }
        var xmlStringDocument =
                this.createAXmlStringDocument(defaultProperties);

        this.xmlDocument =
                this.parser.parseFromString(xmlStringDocument, "text/xml");

        return this.xmlDocument;
    }

}


