/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

class XmlToJson {

    constructor(xmlDocument) {
        xmlDocument && (this.xmlDocument = xmlDocument) &&
            (this.json = {}) && this.initJson();

        this.unwantedNodeName = {
            "#text": true,
            "#comment": true
        };

        this.mappingValue = {
            "true": true,
            "false": false
        };

        this.wantedNode = "channel";
    }

    initJson() {
        if (!this.xmlDocument) {
            return null;
        }

        this.rootsNode = this.xmlDocument.firstElementChild;

        //We already know that document contains at least one child.
        var rootsName = this.rootsNode.nodeName;
        this.rootsElementJson = this.json[rootsName] = {};

        var rootsObject = this.json[rootsName];

        this.convertAttributesToProperties(rootsObject, this.rootsNode);
    }

    convertAttributesToProperties(object, node) {
        var attributesList = node.attributes;

        for (var i = 0, li = attributesList.length; i < li; i++) {
            var currentAttributes = attributesList[i];
            var name = currentAttributes.name;
            var value = currentAttributes.value;
            object[name] = value;
        }
        return object;
    }

    getJson() {
        if (!this.json) {
            console.log("Your xml document is not valid or null.");
            return null;
        }
        var data = this.rootsNode.childNodes;
        var dataStoredByKind = {};

        for (var i = 0, li = data.length; i < li; i++) {
            var currentNode = data[i];
            this.setPropertiesFromDomNodeList(currentNode, this.rootsElementJson);
        }
        return this.json;
    }

    setPropertiesFromDomNodeList(currentNode, parentContainer) {

        if (this.unwantedNodeName[currentNode.nodeName]) {
            return;
        }

        var childNodes = currentNode.childNodes;
        var childNodeName;
        var currentChildNode;
        var nodeDataMap = {};
        if (currentNode.hasAttributes()) {
            nodeDataMap =
                this.convertAttributesToProperties(nodeDataMap, currentNode);
        }

        for (var i = 0, li = childNodes.length; i < li; i++) {
            currentChildNode = childNodes[i];
            childNodeName = currentChildNode.nodeName;

            if (!this.unwantedNodeName[childNodeName]) {
                nodeDataMap = this.setPropertiesFromDomNodeList
                    (currentChildNode, nodeDataMap);
            }

            if (/\w+/.exec(currentChildNode.nodeValue) &&
                    (currentChildNode.childNodes.length == 0)) {

                if (!this.isMapNotEmpty(nodeDataMap)) {
                    var currentNodeName = currentNode.nodeName;
                    var key = currentNodeName;
                    var value = currentChildNode.nodeValue;
                    this.addPropertiesToMap(parentContainer, key, value,
                        currentNode.parentNode);
                }

                //This add concern only the nodes which have an atomic content
                if (this.isMapNotEmpty(nodeDataMap) && childNodes.length == 1) {
                    var key = "#text";
                    var value = currentChildNode.nodeValue;
                    this.addPropertiesToMap(nodeDataMap, key, value,
                        currentNode.parentNode);
                }
            }
        }

        //Addition of node in parent container even if this one have not a value
        if (!this.unwantedNodeName[currentNode.nodeName] &&
            !currentChildNode) {

            var currentNodeName = currentNode.nodeName;
            var key = currentNodeName;
            this.addPropertiesToMap(parentContainer, key, null,
                currentNode.parentNode);
        }

        var currentNodeName = currentNode.nodeName;
        if (this.isMapNotEmpty(nodeDataMap)) {
            var key = currentNodeName;
//            parentContainer[key] = nodeDataMap;
            this.addPropertiesToMap(parentContainer, key, nodeDataMap,
                currentNode.parentNode);
        }

        return parentContainer;
    }

    isMapNotEmpty(map) {
        return Object.keys(map).length > 0;
    }

    /*
     *
     */
    addPropertiesToMap(map, key, value, parentNode) {
//        map[key] = value;

        var childNodesWithSpecificName = parentNode.getElementsByTagName(key);
        var specificNodesNumber = childNodesWithSpecificName.length;

        if ((specificNodesNumber > 1) && !map[key]) {
            map[key] = [];
        }

        var nodeValue = map[key];

        var value = this.setValue(value);

        if (nodeValue && (nodeValue instanceof Array)) {
            map[key].push(value);
        } else {
            map[key] = value;
        }
    }

    setValue(value) {
        var newValue = this.mappingValue[value];
        if ((typeof value) == "string" && newValue != undefined) {
            return newValue;
        }
        return value;
    }
}
