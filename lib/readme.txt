This project is an implementation of oipf standard. You compile it and use it as a library.

To build the project, you need to install the require dependencies :
> npm install

then install Gulp :
> npm install gulp -g

and finally, run gulp :
> gulp

The gulp task is watching for change within the oipf project files. If you make a change, the project will be automatically rebuilt.
The building process produce a js bundle under build/ directory. It also copy the static files that the library need to work. If you want to use this library in another project, you need to point to that directory or embed the files within the build dir.
