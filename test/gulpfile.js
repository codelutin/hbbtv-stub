/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var gulp = require("gulp");
var gulpSequence = require("gulp-sequence");
var concat = require("gulp-concat");
var sourcemaps = require("gulp-sourcemaps");
var babel = require("gulp-babel");
var del = require("del");

var notifier = require("node-notifier");
var util = require("gulp-util");

var express = require("express");
var app = express();

var paths = require("./paths.json");

var conf = {
    jsTestFiles: paths.test,
    buildFile: "oipf-tests.js",
    buildDir: "build"
};

function errorHandler(err) {
    notifier.notify({message: "Error: " + err.message});
    util.log(util.colors.red("Error"), err.message);
    this.emit("end");
}

gulp.task("clean", function(cb) {
    del([conf.buildDir], cb);
});

gulp.task("javascriptTests", function() {
    return gulp.src(conf.jsTestFiles)
    .pipe(sourcemaps.init())
    .pipe(babel({
        blacklist: [
            "es3.memberExpressionLiterals",
            "es3.propertyLiterals",
            "es5.properties.mutators"
        ],
        optional: [
            "asyncToGenerator",
            "minification.memberExpressionLiterals",
            "minification.propertyLiterals",
            "utility.inlineEnvironmentVariables"
        ],
        compact: false
    }))
    .on("error", errorHandler)
    .pipe(concat(conf.buildFile))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(conf.buildDir));
});

// Rerun the task when a file changes
gulp.task("watch", function() {
    gulp.watch(conf.jsTestFiles, ["javascriptTests"]);
});

gulp.task("server", function() {
    require("./server.js");
});

gulp.task("default", gulpSequence("javascriptTests", "watch", "server"));
