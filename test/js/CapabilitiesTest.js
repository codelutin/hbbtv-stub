/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
class CapabilitiesTest extends TestCase {

    constructor() {
        super();
    }

    beforeTest() {
        this.capabilitiesObject = oipfObjectFactory.createCapabilitiesObject();
    }

    afterTest() {

    }

    /*
     * FIXME:
     * Yannis - 12/05/2015 - Instanciation of variable everyProperties.
     */
    testOIFPNormConformityOfObject(resolve, reject) {
        var everyProperties;

        for (var i = 0, l = everyProperties.length; i < l; i++) {
            var currentProperty = everyProperties[i];
            if (!this.capabilitiesObject[currentProperty]) {
                reject();
                return;
            }
        }
        resolve();
    }

    testHasCapabilityWithAnInvalidProfileName(resolve, reject) {
        var profileName = "Toto";
        var result = this.capabilitiesObject.hasCapability(profileName);

        if (result) {
            reject();
        } else {
            resolve();
        }
    }

    testHasCapabilityForAtLeastOneBaseProfileName(resolve, reject) {
        var baseProfileNameList = this.modelFactory
                .getCapabilitiesProperties().baseUIProfileName;
        var supportedProfileNumber = 0;

        for (var i = 0, l = baseProfileNameList.length; i < l; i++) {
            var baseProfileName = baseProfileNameList[i];
            var result = this.capabilitiesObject.hasCapability(baseProfileName);
            result && supportedProfileNumber++;
        }

        if (supportedProfileNumber > 0) {
            resolve();
        } else {
            reject();
        }
    }

    testHasCapabilityForAllUIFragmentProfile(resolve, reject) {
        var UIProfileNameFragmentList = this.modelFactory
                .getCapabilitiesProperties().UIProfileNameFragment;
        var supportedProfileNumber = 0;

        for (var i = 0, l = UIProfileNameFragmentList.length; i < l; i++) {
            var UIProfileNameFragment = UIProfileNameFragmentList[i];
            var result = this.capabilitiesObject
                    .hasCapability(UIProfileNameFragment);
            result && supportedProfileNumber++;
        }

        if (supportedProfileNumber == UIProfileNameFragmentList.length) {
            resolve();
        } else {
            reject();
        }
    }

    getCapabilityAndNotifyResult(resolve, reject, profileName) {
        var result = this.capabilitiesObject.hasCapability(profileName);

        if (result) {
            resolve();
        } else {
            reject();
        }
    }

    testHasCapabilityTRICKMODE(resolve, reject) {
        var profileName = "+TRICKMODE";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityAVCAD(resolve, reject) {
        var profileName = "+AVCAD";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityDL(resolve, reject) {
        var profileName = "+DL";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityIPTV_SDS(resolve, reject) {
        var profileName = "+IPTV_SDS";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityIPTV_URI(resolve, reject) {
        var profileName = "+IPTV_URI";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityANA(resolve, reject) {
        var profileName = "+ANA";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityDVB_C(resolve, reject) {
        var profileName = "+DVB_C";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityDVB_T(resolve, reject) {
        var profileName = "+DVB_T";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityDVB_S(resolve, reject) {
        var profileName = "+DVB_S";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityDVB_C2(resolve, reject) {
        var profileName = "+DVB_C2";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityDVB_T2(resolve, reject) {
        var profileName = "+DVB_T2";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityDVB_S2(resolve, reject) {
        var profileName = "+DVB_S2";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityISDB_C(resolve, reject) {
        var profileName = "+ISDB_C";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityISDB_T(resolve, reject) {
        var profileName = "+ISDB_T";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityISDB_S(resolve, reject) {
        var profileName = "+ISDB_S";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityMETA_BCG(resolve, reject) {
        var profileName = "+META_BCG";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityMETA_EIT(resolve, reject) {
        var profileName = "+META_EIT";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityMETA_SI(resolve, reject) {
        var profileName = "+META_SI";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityITV_KEYS(resolve, reject) {
        var profileName = "+ITV_KEYS";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityCONTROLLED(resolve, reject) {
        var profileName = "+CONTROLLED";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityPVR(resolve, reject) {
        var profileName = "+PVR";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityDRM(resolve, reject) {
        var profileName = "+DRM";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityCommunicationServices(resolve, reject) {
        var profileName = "+CommunicationServices";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilitySVG(resolve, reject) {
        var profileName = "+SVG";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityPOINTER(resolve, reject) {
        var profileName = "+POINTER";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityPOLLNOTIF(resolve, reject) {
        var profileName = "+POLLNOTIF";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityHtml5Media(resolve, reject) {
        var profileName = "+HTML5_MEDIA";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityWIDGETS(resolve, reject) {
        var profileName = "+WIDGETS";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityRCF(resolve, reject) {
        var profileName = "+RCF";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityTELEPHONY(resolve, reject) {
        var profileName = "+TELEPHONY";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

    testHasCapabilityVIDEOTELEPHONY(resolve, reject) {
        var profileName = "+VIDEOTELEPHONY";
        this.getCapabilityAndNotifyResult(resolve, reject, profileName);
    }

}

