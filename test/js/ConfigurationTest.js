/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
class ConfigurationTest extends TestCase {

    constructor() {
        super();
    }

    beforeTest(resolve, reject) {
        this.configurationObject =  oipfObjectFactory.createConfigurationObject();
        this.localSystem = this.configurationObject.localSystem;
        OipfUtils.logTest("State: Pending");
    }

    testSetScreenSize(resolve, reject) {

        if (this.localSystem) {
            var result = this.localSystem.setScreenSize(1280, 720);

            if (result) {
                resolve();
            } else {
                throw new Error("Unexpected error");
            }
        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetScreenSizeWithoutParameters(resolve, reject) {

        if (this.localSystem) {
            this.waitForError(this.localSystem.setScreenSize, resolve);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetScreenSizeWithUnexpectedTypeParameters(resolve, reject) {

        if (this.localSystem) {

            this.waitForError(
            this.localSystem.setScreenSize.bind(this.localSystem, "", ""),
            resolve);

        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetScreenSizeWithExcessiveParametersNumber(resolve, reject) {

        if (this.localSystem) {
            this.waitForError(
            this.localSystem.setScreenSize.bind(this.localSystem, 640, 480, 854),
            resolve);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetTvStandard(resolve, reject) {

        if (this.localSystem) {
            var result = this.localSystem.setTVStandard(4);
            if (result) {
                resolve();
            } else {
                throw new Error("Unexpected error");
            }
        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetTvStandardWihtoutParameters(resolve, reject) {

        if (this.localSystem) {
            this.waitForError(this.localSystem.setTVStandard, resolve);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetTvStandardWithUnexpectedTypeParameters(resolve, reject) {

        if (this.localSystem) {
            this.waitForError(
            this.localSystem.setTVStandard.bind(this.localSystem, ""),
            resolve);

        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetTvStandardWithExcessiveParametersNumber(resolve, reject) {

        if (this.localSystem) {
            this.waitForError(
            this.localSystem.setTVStandard.bind(this.localSystem, 4, 4),
            resolve);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetPvrSupport(resolve, reject) {

        if (this.localSystem) {
            var result = this.localSystem.setPvrSupport(1);
            if (result) {
                resolve();
            } else {
                throw new Error("Unexpected error");
            }
        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetPvrSupportWithoutParameters(resolve, reject) {

        if (this.localSystem) {
            this.waitForError(this.localSystem.setPvrSupport, resolve);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetPvrSupportWithUnexpectedTypeParameters(resolve, reject) {

        if (this.localSystem) {
            this.waitForError(
            this.localSystem.setPvrSupport.bind(this.localSystem, {}),
            resolve);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetPvrSupportWithExcessiveParametersNumber(resolve, reject) {

        if (this.localSystem) {
            this.waitForError(
            this.localSystem.setPvrSupport.bind(this.localSystem, 0, 0, 0),
            resolve);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetPowerState(resolve, reject) {
        var self = this

        this.localSystem.onPowerStateChange = function(state) {
            if (self.result && state == 1) {
                resolve();
            } else {
                reject();
            }
        };

        if (this.localSystem) {
            this.result = this.localSystem.setPowerState(1);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetPowerStateWithoutParameters(resolve, reject) {

        if (this.localSystem) {
            this.waitForError(this.localSystem.setPowerState, resolve);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetPowerStateWithUnexpectedTypeParameters(resolve, reject) {

        if (this.localSystem) {
            this.waitForError(
            this.localSystem.setPowerState.bind(this.localSystem, {}),
            resolve);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetPowerStateWithExcessiveParametersNumber(resolve, reject) {

        if (this.localSystem) {
            this.waitForError(
            this.localSystem.setPowerState.bind(this.localSystem, 0, 0, 0),
            resolve);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetDigestCredentials(resolve, reject) {

        if (this.localSystem) {
            this.localSystem.setDigestCredentials(640, 480, 854);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetDigestCredentialsWithoutParameters(resolve, reject) {

        if (this.localSystem) {
            this.localSystem.setDigestCredentials(640, 480, 854);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetDigestCredentialsWithUnexpectedTypeParameters(resolve, reject) {

        if (this.localSystem) {
            this.localSystem.setDigestCredentials(640, 480, 854);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testSetDigestCredentialsWithExcessiveParametersNumber(resolve, reject) {

        if (this.localSystem) {
            this.localSystem.setDigestCredentials(640, 480, 854);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testClearDigestCredentials(resolve, reject) {

        if (this.localSystem) {
            this.localSystem.clearDigestCredentials(640, 480, 854);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testClearDigestCredentialsWithoutParameters(resolve, reject) {

        if (this.localSystem) {
            this.localSystem.clearDigestCredentials(640, 480, 854);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testClearDigestCredentialsWithUnexpectedTypeParameters(resolve, reject) {

        if (this.localSystem) {
            this.localSystem.clearDigestCredentials(640, 480, 854);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testClearDigestCredentialsWithExcessiveParametersNumber(resolve, reject) {

        if (this.localSystem) {
            this.localSystem.clearDigestCredentials(640, 480, 854);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testHasDigestCredentials(resolve, reject) {

        if (this.localSystem) {
            this.localSystem.hasDigestCredentials(640, 480, 854);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testHasDigestCredentialsWithoutParameters(resolve, reject) {

        if (this.localSystem) {
            this.localSystem.hasDigestCredentials(640, 480, 854);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testHasDigestCredentials(resolve, reject) {

        if (this.localSystem) {
            this.localSystem.hasDigestCredentials(640, 480, 854);
        } else {
            throw new Error("Unexpected error");
        }
    }

    testHasDigestCredentialsWithExcessiveParametersNumber(resolve, reject) {

        if (this.localSystem) {
            this.localSystem.hasDigestCredentials(640, 480, 854);
        } else {
            throw new Error("Unexpected error");
        }
    }

    afterTest() {
    }

}

