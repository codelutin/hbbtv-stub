/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
class OipfFactoryTest extends TestCase {
    constructor() {
        super();

        this.testIsApplicationManagerObjectSupported =
            this.testObjectSupported.bind(this, APPLICATION_MANAGER_DAE_MIME_TYPE);

        this.testIsCapabilitiesObjectSupported = this.testObjectSupported.bind(this, CAPABILITIES_DAE_MIME_TYPE);
        this.testIsChannelConfigSupported = this.testObjectSupported.bind(this, CHANNEL_CONFIG_TYPE);
        this.testIsCodManagerObjectSupported = this.testObjectSupported.bind(this, COD_MANAGER_DAE_MIME_TYPE);
        this.testIsIMSObjectSupported = this.testObjectSupported.bind(this, COMMUNICATION_SERVICES_DAE_MIME_TYPE);
        this.testIsConfigurationObjectSupported = this.testObjectSupported.bind(this, CONFIGURATION_DAE_MIME_TYPE);
        this.testIsDownloadManagerObjectSupported = this.testObjectSupported.bind(this, DOWNLOAD_MANAGER_DAE_MIME_TYPE);
        this.testIsDownloadTriggerObjectSupported = this.testObjectSupported.bind(this, DOWNLOAD_TRIGGER_DAE_MIME_TYPE);
        this.testIsDrmAgentObjectSupported = this.testObjectSupported.bind(this, DRM_AGENT_DAE_MIME_TYPE);
        this.testIsGatewayInfoObjectSupported = this.testObjectSupported.bind(this, GATEWAY_INFO_DAE_MIME_TYPE);
        this.testIsMDTFObjectSupported = this.testObjectSupported.bind(this, MDTF_DAE_MIME_TYPE);
        this.testIsNotifSocketObjectSupported = this.testObjectSupported.bind(this, NOTIF_SOCKET_DAE_MIME_TYPE);

        this.testIsParentalControlManagerObjectSupported =
            this.testObjectSupported.bind(this, PARENTAL_CONTROL_MANAGER_DAE_MIME_TYPE);

        this.testIsRecordingSchedulerObjectSupported =
            this.testObjectSupported.bind(this, RECORDING_SCHEDULER_DAE_MIME_TYPE);


        this.testIsRemoteControlFunctionObjectSupported =
            this.testObjectSupported.bind(this, REMOTE_CONTROL_FUNCTION_DAE_MIME_TYPE);
        this.testIsRemoteManagementObjectSupported = this.testObjectSupported.bind(this, REMOTE_MANAGEMENT_DAE_MIME_TYPE);
        this.testIsSearchManagerObjectSupported = this.testObjectSupported.bind(this, SEARCH_MANAGER_DAE_MIME_TYPE);
        this.testIsStatusViewObjectSupported = this.testObjectSupported.bind(this, STATUS_VIEW_DAE_MIME_TYPE);
        this.testIsVideoBroadcastObjectSupported = this.testObjectSupported.bind(this, VIDEO_BROADCAST_DAE_MIME_TYPE);
        this.testIsVideoMpegObjectSupported = this.testObjectSupported.bind(this, VIDEO_MPEG_DAE_MIME_TYPE);



        this.testCreateApplicationManagerObject =
            this.testObjectCreated.bind(this, oipfObjectFactory.createApplicationManagerObject);

        this.testCreateCapabilitiesObject = this.testObjectCreated.bind(this, oipfObjectFactory.createCapabilitiesObject);
        this.testCreateChannelConfig = this.testObjectCreated.bind(this, oipfObjectFactory.createChannelConfig);
        this.testCreateCodManagerObject = this.testObjectCreated.bind(this, oipfObjectFactory.createCodManagerObject);

        this.testCreateConfigurationObject =
            this.testObjectCreated.bind(this, oipfObjectFactory.createConfigurationObject);

        this.testCreateDownloadManagerObject =
            this.testObjectCreated.bind(this, oipfObjectFactory.createDownloadManagerObject);

        this.testCreateDownloadTriggerObject =
            this.testObjectCreated.bind(this, oipfObjectFactory.createDownloadTriggerObject);

        this.testCreateDrmAgentObject = this.testObjectCreated.bind(this, oipfObjectFactory.createDrmAgentObject);
        this.testCreateGatewayInfoObject = this.testObjectCreated.bind(this, oipfObjectFactory.createGatewayInfoObject);
        this.testCreateIMSObject = this.testObjectCreated.bind(this, oipfObjectFactory.createIMSObject);
        this.testCreateMDTFObject = this.testObjectCreated.bind(this, oipfObjectFactory.createMDTFObject);
        this.testCreateNotifSocketObject = this.testObjectCreated.bind(this, oipfObjectFactory.createNotifSocketObject);

        this.testCreateParentalControlManagerObject =
            this.testObjectCreated.bind(this, oipfObjectFactory.createParentalControlManagerObject);

        this.testCreateRecordingSchedulerObject =
            this.testObjectCreated.bind(this, oipfObjectFactory.createRecordingSchedulerObject);

        this.testCreateRemoteControlFunctionObject =
            this.testObjectCreated.bind(this, oipfObjectFactory.createRemoteControlFunctionObject);

        this.testCreateRemoteManagementObject =
            this.testObjectCreated.bind(this, oipfObjectFactory.createRemoteManagementObject);

        this.testCreateSearchManagerObject =
            this.testObjectCreated.bind(this, oipfObjectFactory.createSearchManagerObject);

        this.testCreateStatusViewObject = this.testObjectCreated.bind(this, oipfObjectFactory.createStatusViewObject);

        this.testCreateVideoBroadcastObject =
            this.testObjectCreated.bind(this, oipfObjectFactory.createVideoBroadcastObject);

        this.testCreateVideoMpegObject = this.testObjectCreated.bind(this, oipfObjectFactory.createVideoMpegObject);

    }

    testObjectCreated(method, resolve, reject) {
        var object = method.call(oipfObjectFactory);

        if (this.assertNotNull(object)) {
            resolve();
        } else {
            reject("The object is null.");
        }
    }

    testObjectSupported(mimeType, resolve, reject) {
        if (oipfObjectFactory.isObjectSupported(mimeType)) {
            resolve();
        } else {
            reject();
        }
    }
}
