/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
class ParentalControlManagerTest extends TestCase {

    constructor() {
        super();
        this.PIN_CORRECT = 0;
        this.PIN_INCORRECT = 1;
        this.PIN_LOCKED = 2;
        this.PIN = "1234";
    }

    beforeTest(resolve, reject) {
        this.parentalControlManagerObject =
                oipfObjectFactory.createParentalControlManagerObject();
    }

    afterTest() {
        this.parentalControlManagerObject.setParentalControlStatus(this.PIN, false);
        this.parentalControlManagerObject.setBlockUnrated(this.PIN, false);
        this.parentalControlManagerObject._setCurrentInvalidPINAttempts &&
            this.parentalControlManagerObject._setCurrentInvalidPINAttempts(0);

        this.newPIN && this.resetOldPIN();
    }

    resetOldPIN() {
        this.parentalControlManagerObject
                .setParentalControlPIN(this.newPIN, this.PIN);
    }

    renderResultIfTrue(resolve, reject, condition) {
        if (condition) {
            resolve();
        } else {
            reject();
        }
    }

    testSetParentalControlStatusWithValidPINAndTrue(resolve, reject) {
        var result = this.parentalControlManagerObject
                .setParentalControlStatus(this.PIN, true);

        console.log(result);

        if (this.assertEquals(result, 0)) {
            resolve();
        } else {
            reject();
        }
    }

    testSetParentalControlStatusWithValidPINAndFalse(resolve, reject) {
        var result = this.parentalControlManagerObject
                .setParentalControlStatus(this.PIN, false);

        console.log(result);

        if (this.assertEquals(result, 0)) {
            resolve();
        } else {
            reject();
        }
    }

    testSetParentalControlStatusWithInvalidPINAndTrue(resolve, reject) {
        var result = this.parentalControlManagerObject
                .setParentalControlStatus("5555", true);

        console.log(result);

        this.renderResultIfTrue(resolve, reject, this.assertEquals(result, 1));
    }

    testSetParentalControlStatusWithInvalidPINAndFalse(resolve, reject) {
        var result = this.parentalControlManagerObject
                .setParentalControlStatus("5555", false);

        console.log(result);

        this.renderResultIfTrue(resolve, reject, this.assertEquals(result, 1));
    }

    testSetParentalControlStatusWithInvalidOldPINAndValidNewPIN10Times(resolve, reject) {
        for (var i = 0, li = 10; i < li; i++) {
            var result = this.parentalControlManagerObject
                .setParentalControlStatus("PIN", false);

            console.log(i + 1, ":", result);
        }

        this.renderResultIfTrue(resolve, reject,
            this.assertEquals(result, this.PIN_LOCKED));
    }

    testVerifyParentalControlPINWithInvalidPIN10Times(resolve, reject) {
        for (var i = 0, li = 10; i < li; i++) {
            var result = this.parentalControlManagerObject
                    .verifyParentalControlPIN("0000");

            console.log(i + 1, ":", result);
        }

        if (this.assertEquals(result, 2)) {
            resolve();
        } else {
            reject();
        }
    }

    testVerifyParentalControlPINWithValidPIN(resolve, reject) {
        var result = this.parentalControlManagerObject
                .verifyParentalControlPIN("1234");

        console.log(result);

        this.renderResultIfTrue(resolve, reject,
            this.assertEquals(result, this.PIN_CORRECT));
    }

    testVerifyParentalControlPINWithInvalidPIN(resolve, reject) {
        var result = this.parentalControlManagerObject
                .verifyParentalControlPIN("68465");

        console.log(result);

        this.renderResultIfTrue(resolve, reject,
            this.assertEquals(result, this.PIN_INCORRECT));
    }

    testSetBlockUnratedWithInvalidPIN10Times(resolve, reject) {
        for (var i = 0, li = 10; i < li; i++) {
            var result = this.parentalControlManagerObject
                    .setBlockUnrated("toto", true);

            console.log(i + 1, ":", result);
        }

        if (this.assertEquals(result, 2)) {
            resolve();
        } else {
            reject();
        }
    }

    testSetBlockUnratedWithValidPINAndTrue(resolve, reject) {
        var result = this.parentalControlManagerObject
                    .setBlockUnrated(this.PIN, true);

        console.log(result);

        if (this.assertEquals(result, 0)) {
            resolve();
        } else {
            reject();
        }
    }

    testSetBlockUnratedWithValidPINAndFalse(resolve, reject) {
        var result = this.parentalControlManagerObject
                    .setBlockUnrated(this.PIN, false);

        console.log(result);

        if (this.assertEquals(result, 0)) {
            resolve();
        } else {
            reject();
        }
    }

    testSetBlockUnratedWithInvalidPINAndTrue(resolve, reject) {
        var result = this.parentalControlManagerObject
                    .setBlockUnrated("5555", true);

        console.log(result);

        this.renderResultIfTrue(resolve, reject, this.assertEquals(result, 1));
    }

    testSetBlockUnratedWithInvalidPINAndFalse(resolve, reject) {
        var result = this.parentalControlManagerObject
                    .setBlockUnrated("5555", false);

        console.log(result);

        this.renderResultIfTrue(resolve, reject, this.assertEquals(result, 1));
    }

    testGetBlockUnratedWhenUnratedContentsAreNotBlocked(resolve, reject) {
        if (!this.parentalControlManagerObject.getBlockUnrated()) {
            resolve();
        } else {
            reject();
        }
    }

    testGetBlockUnratedWhenUnratedContentsAreBlocked(resolve, reject) {
        this.parentalControlManagerObject.setBlockUnrated("1234", true);

        if (this.parentalControlManagerObject.getBlockUnrated()) {
            resolve();
        } else {
            reject();
        }
    }

    testSetParentalControlPINWithInvalidOldPINAndValidNewPIN10Times(resolve, reject) {
        for (var i = 0, li = 10; i < li; i++) {
            var resultSetPIN = this.parentalControlManagerObject
                .setParentalControlPIN("PIN", "1212");

            console.log(i + 1, ":", resultSetPIN);
        }

        this.renderResultIfTrue(resolve, reject,
            this.assertEquals(resultSetPIN, this.PIN_LOCKED));
    }

    testSetParentalControlPINWithValidOldPINAndNewPIN(resolve, reject) {
        this.newPIN = "1212";
        var resultSetPIN = this.parentalControlManagerObject
                .setParentalControlPIN(this.PIN, this.newPIN);

        var resultVerifyPIN = this.parentalControlManagerObject
                .verifyParentalControlPIN(this.newPIN);

        if (this.assertEquals(resultSetPIN, this.PIN_CORRECT) &&
                this.assertEquals(resultVerifyPIN, this.PIN_CORRECT)) {
            resolve();
        } else {
            reject();
        }
    }

    testSetParentalControlPINWithInvalidOldPINAndValidNewPIN(resolve, reject) {
        this.newPIN = "1212";
        var resultSetPIN = this.parentalControlManagerObject
                .setParentalControlPIN("PIN", this.newPIN);

        console.log(resultSetPIN);

        var resultVerifyPIN = this.parentalControlManagerObject
                .verifyParentalControlPIN(this.newPIN);

        console.log(resultVerifyPIN);

        if (this.assertEquals(resultSetPIN, this.PIN_INCORRECT) &&
                this.assertEquals(resultVerifyPIN, this.PIN_INCORRECT)) {
            resolve();
        } else {
            reject();
        }
    }

    testGetParentalControlStatusWhenParentalControlStatusDeactivated(resolve, reject) {
        if (!this.parentalControlManagerObject.getParentalControlStatus()) {
            resolve();
        } else {
            reject();
        }
    }

    testGetParentalControlStatusWhenParentalControlStatusActivated(resolve, reject) {
        this.parentalControlManagerObject
                .setParentalControlStatus(this.PIN, true);

        if (this.parentalControlManagerObject.getParentalControlStatus()) {
            resolve();
        } else {
            reject();
        }
    }
}
