/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var RecordingSchedulerTest = class extends TestCase {

    constructor() {
        super();
        this.timer = 3 * 60 * 1000; //3minutes, we had increased timer for unresolved tests.
    }

    deleteAndStopStartedRecording() {
        var recordings = this.recordingSchedulerObject.recordings;

        for (var i = 0, l = recordings.length; i < l; i++) {
            var recording = recordings[i];
            if (recording.state == this.STARTED) {
                this.recordingSchedulerObject.stop(recording);
                this.recordingSchedulerObject.remove(recording);
            }
        }
    }

    beforeTest(resolve, reject) {
        var self = this;
        this.getPromise = function() {
            return new Promise(function(resolve, reject) {
                self.resolve = resolve;
                resolve();
            });
        };
        this.onPVREventCopy = this.onPVREvent.bind(this, resolve, reject);
        this.recordingSchedulerObject =
            oipfObjectFactory.createRecordingSchedulerObject();

        this.recordingSchedulerObject
            .addEventListener("PVREvent", this.onPVREventCopy, false);

        this.createVideoBroadcastObject();
        this.onPlayStateChange = this.onChangeState.bind(this, resolve, reject);
        this.vidBroadObj
            .addEventListener("PlayStateChange", this.onPlayStateChange, false);

//        this.deleteAndStopStartedRecording();

        this.searchManagerObj = oipfObjectFactory.createSearchManagerObject();

        this.transitions = [];
        this.previousState = null;
        this.currentIndex = 0;
    }

    afterTest(resolve, reject) {
        this.recordingSchedulerObject
            .removeEventListener("PVREvent", this.onPVREventCopy);
        this.recordingSchedulerObject.stop(this.recording);
        this.recordingSchedulerObject.remove(this.recording);
    }

    onPVREvent(resolve, reject, newState, recordings) {
        if (this.currentIndex < this.transitions.length) {
            console.log("PVREvent received: State:", newState);
            var currentTransition = this.transitions[this.currentIndex];
            var begin = currentTransition.begin;
            var end = currentTransition.end;

            if (begin == this.previousState && end == newState) {
                currentTransition.callback && currentTransition.callback();
                this.previousState = newState;
                this.currentIndex++;
            } else {
                reject("Invalid state.");
            }
        } else {
            console.log("Any others transitions have been set.");
        }
    }

    testRecordAFinishedProgramme(resolve, reject) {
        var self = this;

        this.getPromise()
            .then(this.bindToCurrentChannel.bind(this))
            .then(this.timeout.bind(this))
            .then(this.findAFinishedProgramme.bind(this))
            .then(this.timeout.bind(this))
            .then(function() {
                self.recording = self.recordingSchedulerObject.record(self.programme);
                if (self.assertEquals(self.recording, null)) {
                    resolve();
                } else {
                    reject();
                }
            });
    }

    testRecordCurrentProgramme(resolve, reject) {
        var self = this;

        this.getPromise()
            .then(this.bindToCurrentChannel.bind(this))
            .then(this.timeout.bind(this))
            .then(this.findProgrammeFromStream.bind(this, resolve, reject))
            .then(this.timeout.bind(this))
            .then(function() {
                console.log("Addition of transition.");
                self.resetTransitionVariables();
                self.addTransition(null, self.SCHEDULED);
                self.addTransition(self.SCHEDULED, self.BEFORE_STARTED);
                self.addTransition(self.BEFORE_STARTED, self.STARTED, function() {
                    var result = self.recordingSchedulerObject.update(self.recording.id,
                        undefined, 3000, undefined);
                });
                self.addTransition(self.STARTED, self.COMPLETED, resolve);

                self.recording = self.recordingSchedulerObject.record(self.programme);
                console.log("Recording State after scheduling:", self.recording.state);
//                console.log("Recording:", self.recording);
            });
    }

    testRecordAtCurrentProgramme(resolve, reject) {
        this.addTransition(null, this.SCHEDULED);
        this.addTransition(this.SCHEDULED, this.BEFORE_STARTED);
        this.addTransition(this.BEFORE_STARTED, this.STARTED);
        this.addTransition(this.STARTED, this.COMPLETED, resolve);
        var channelConfig = this.vidBroadObj.getChannelConfig();
        var randomIndex = OipfUtils
                .getRandomNumberBetweenMinMax(0, channelConfig.channelList.length);
        var channel = channelConfig.channelList[randomIndex];
        var currentTime = this.dateUtils.nowInSeconds();
        console.log(currentTime);
        this.recording = this.recordingSchedulerObject.recordAt(currentTime,
            5, 0x00, channel.ccid);
        console.log(this.recording);
    }

    testStopAScheduledRecording(resolve, reject) {
        var self = this;

        this.getPromise()
            .then(this.bindToCurrentChannel.bind(this))
            .then(this.timeout.bind(this))
            .then(this.findProgrammeFromStream.bind(this, resolve, reject))
            .then(this.timeout.bind(this))
            .then(function() {
                self.resetTransitionVariables();
                self.addTransition(null, self.SCHEDULED, function() {
                    self.recordingSchedulerObject.stop(self.recording);
                    self.timeout(2000).then(resolve);
                });

                self.recording = self.recordingSchedulerObject.record(self.programme);
            });
    }

    testStopAnOngoingRecording(resolve, reject) {
        var self = this;

        this.getPromise()
            .then(this.bindToCurrentChannel.bind(this))
            .then(this.timeout.bind(this))
            .then(this.findProgrammeFromStream.bind(this, resolve, reject))
            .then(this.timeout.bind(this))
            .then(function() {
                self.resetTransitionVariables();
                self.addTransition(null, self.SCHEDULED);
                self.addTransition(self.SCHEDULED, self.BEFORE_STARTED);
                self.addTransition(self.BEFORE_STARTED, self.STARTED, function() {
                    self.recordingSchedulerObject.stop(self.recording);
                    self.timeout(2000).then(resolve);
                });

                self.recording = self.recordingSchedulerObject.record(self.programme);
            });
    }

    testStopACompletedRecording(resolve, reject) {
        var self = this;

        this.getPromise()
            .then(this.bindToCurrentChannel.bind(this))
            .then(this.timeout.bind(this))
            .then(this.findProgrammeFromStream.bind(this, resolve, reject))
            .then(this.timeout.bind(this))
            .then(function() {
                self.resetTransitionVariables();
                self.addTransition(null, self.SCHEDULED);
                self.addTransition(self.SCHEDULED, self.BEFORE_STARTED);
                self.addTransition(self.BEFORE_STARTED, self.STARTED);
                self.addTransition(self.STARTED, self.COMPLETED, function() {
                    self.recordingSchedulerObject.stop(self.recording);
                    self.timeout(3000).then(resolve);
                });

                self.recording = self.recordingSchedulerObject.record(self.programme);
            });
    }

    testRemoveAScheduledRecording(resolve, reject) {
        var self = this;

        this.getPromise()
            .then(this.bindToCurrentChannel.bind(this))
            .then(this.timeout.bind(this))
            .then(this.findProgrammeFromStream.bind(this, resolve, reject))
            .then(this.timeout.bind(this))
            .then(function() {
                self.resetTransitionVariables();
                self.addTransition(null, self.SCHEDULED, function() {
                    self.recordingSchedulerObject.remove(self.recording);
                });
                self.addTransition(self.SCHEDULED, self.REMOVED, resolve);

                self.recording = self.recordingSchedulerObject
                        .record(self.programme);
            });
    }

    testRemoveAnOngoingRecording(resolve, reject) {
        var self = this;

        this.getPromise()
            .then(this.bindToCurrentChannel.bind(this))
            .then(this.timeout.bind(this))
            .then(this.findProgrammeFromStream.bind(this, resolve, reject))
            .then(this.timeout.bind(this))
            .then(function() {
                self.resetTransitionVariables();
                self.addTransition(null, self.SCHEDULED);
                self.addTransition(self.SCHEDULED, self.BEFORE_STARTED);
                self.addTransition(self.BEFORE_STARTED, self.STARTED, function() {
                    self.recordingSchedulerObject.remove(self.recording);
                });
                self.addTransition(self.STARTED, self.REMOVED, resolve);

                self.recording = self.recordingSchedulerObject
                        .record(self.programme);
            });
    }

    testRemoveACompletedRecording(resolve, reject) {
        var self = this;

        this.getPromise()
            .then(this.bindToCurrentChannel.bind(this))
            .then(this.timeout.bind(this))
            .then(this.findProgrammeFromStream.bind(this, resolve, reject))
            .then(this.timeout.bind(this))
            .then(function() {
                self.resetTransitionVariables();
                self.addTransition(null, self.SCHEDULED);
                self.addTransition(self.SCHEDULED, self.BEFORE_STARTED);
                self.addTransition(self.BEFORE_STARTED, self.STARTED);
                self.addTransition(self.STARTED, self.COMPLETED, function() {
                    self.recordingSchedulerObject.remove(self.recording);
                });
                self.addTransition(self.COMPLETED, self.REMOVED, resolve);
                self.recording = self.recordingSchedulerObject.record(self.programme);
            });
    }

    testUpdateAScheduledRecordingOnStartTime(resolve, reject) {
        var self = this;

        this.getPromise()
            .then(this.bindToCurrentChannel.bind(this))
            .then(this.timeout.bind(this))
            .then(this.findProgrammeFromStream.bind(this, resolve, reject))
            .then(this.timeout.bind(this))
            .then(function() {
                self.resetTransitionVariables();
                self.addTransition(null, self.SCHEDULED, function() {
                    var twentyMinutes = 20 * 60;
                    self.recordingSchedulerObject.update(self.recording.id,
                    self.recording.startTime + twentyMinutes, undefined,
                    undefined);
                });
                self.addTransition(self.SCHEDULED, self.BEFORE_STARTED);
                self.addTransition(self.BEFORE_STARTED, self.STARTED);
                self.addTransition(self.STARTED, self.COMPLETED);
                self.recording = self.recordingSchedulerObject.record(self.programme);
            });
    }

    testUpdateAScheduledRecordingOnDuration(resolve, reject) {
        var self = this;

        this.getPromise()
            .then(this.bindToCurrentChannel.bind(this))
            .then(this.timeout.bind(this))
            .then(this.findProgrammeFromStream.bind(this, resolve, reject))
            .then(this.timeout.bind(this))
            .then(function() {
                self.resetTransitionVariables();
                self.addTransition(null, self.SCHEDULED, function() {
                    var twentyMinutes = 5;
                    self.recordingSchedulerObject.update(self.recording.id,
                    undefined, self.recording.duration + twentyMinutes,
                    undefined);
                });
                self.addTransition(self.SCHEDULED, self.BEFORE_STARTED);
                self.addTransition(self.BEFORE_STARTED, self.STARTED);
                self.addTransition(self.STARTED, self.COMPLETED);
                self.recording = self.recordingSchedulerObject.record(self.programme);
            });
    }

    testUpdateAnOngoingRecordingWithAValidDuration(resolve, reject) {
        var self = this;

        this.getPromise()
            .then(this.bindToCurrentChannel.bind(this))
            .then(this.timeout.bind(this))
            .then(this.findProgrammeFromStream.bind(this, resolve, reject))
            .then(this.timeout.bind(this))
            .then(function() {
                self.resetTransitionVariables();
                self.addTransition(null, self.SCHEDULED);
                self.addTransition(self.SCHEDULED, self.BEFORE_STARTED);
                self.addTransition(self.BEFORE_STARTED, self.STARTED, function() {
                    var oneMinute = 5;
                    var result = self.recordingSchedulerObject.update(self.recording.id,
                    undefined, self.recording.duration + oneMinute,
                    undefined);
                    if (self.assertEquals(result, true)) {
                        resolve();
                    } else {
                        reject();
                    }
                });
                self.recording = self.recordingSchedulerObject
                        .record(self.programme);
            });
    }

    testUpdateAnOngoingRecordingWithATooShortDuration(resolve, reject) {
        var self = this;

        this.getPromise()
            .then(this.bindToCurrentChannel.bind(this))
            .then(this.timeout.bind(this))
            .then(this.findProgrammeFromStream.bind(this, resolve, reject))
            .then(this.timeout.bind(this))
            .then(function() {
                self.resetTransitionVariables();
                self.addTransition(null, self.SCHEDULED);
                self.addTransition(self.SCHEDULED, self.BEFORE_STARTED);
                self.addTransition(self.BEFORE_STARTED, self.STARTED, function() {
                    var oneMinute = 1 * 60;
                    var result = self.recordingSchedulerObject.update(
                        self.recording.id, undefined, oneMinute, undefined);
                    if (self.assertEquals(result, false)) {
                        resolve();
                    } else {
                        reject();
                    }
                });
                self.recording = self.recordingSchedulerObject
                        .record(self.programme);
            });
    }

    testUpdateAnOngoingRecordingOnStartTime(resolve, reject) {
        var self = this;

        this.getPromise()
            .then(this.bindToCurrentChannel.bind(this))
            .then(this.timeout.bind(this))
            .then(this.findProgrammeFromStream.bind(this, resolve, reject))
            .then(this.timeout.bind(this))
            .then(function() {
                self.resetTransitionVariables();
                self.addTransition(null, self.SCHEDULED);
                self.addTransition(self.SCHEDULED, self.BEFORE_STARTED);
                self.addTransition(self.BEFORE_STARTED, self.STARTED, function() {

                    var result = self.recordingSchedulerObject.update(self.recording.id,
                    564, undefined, undefined);
                    if (self.assertEquals(result, false)) {
                        resolve();
                    } else {
                        reject();
                    }
                });
                self.recording = self.recordingSchedulerObject
                        .record(self.programme);
            });
    }


    testUpdateAnOngoingRecordingOnRepeatDays(resolve, reject) {
        var self = this;

        this.getPromise()
            .then(this.bindToCurrentChannel.bind(this))
            .then(this.timeout.bind(this))
            .then(this.findProgrammeFromStream.bind(this, resolve, reject))
            .then(this.timeout.bind(this))
            .then(function() {
                self.resetTransitionVariables();
                self.addTransition(null, self.SCHEDULED);
                self.addTransition(self.SCHEDULED, self.BEFORE_STARTED);
                self.addTransition(self.BEFORE_STARTED, self.STARTED, function() {
                    var result = self.recordingSchedulerObject.update(
                            self.recording.id, undefined, undefined, 0x00);

                    if (self.assertEquals(result, false)) {
                        resolve();
                    } else {
                        reject();
                    }
                });
                self.recording = self.recordingSchedulerObject
                        .record(self.programme);
            });
    }

    testUpdateACompletedRecordingOnDuration(resolve, reject) {
        var self = this;

        this.getPromise()
            .then(this.bindToCurrentChannel.bind(this))
            .then(this.timeout.bind(this))
            .then(this.findProgrammeFromStream.bind(this, resolve, reject))
            .then(this.timeout.bind(this))
            .then(function() {
                self.resetTransitionVariables();
                self.addTransition(null, self.SCHEDULED);
                self.addTransition(self.SCHEDULED, self.BEFORE_STARTED);
                self.addTransition(self.BEFORE_STARTED, self.STARTED);
                self.addTransition(self.STARTED, self.COMPLETED, function() {
                    var twentyMinutes = 20 * 60;
                    var result = self.recordingSchedulerObject
                            .update(self.recording.id, undefined,
                                self.recording.duration + twentyMinutes,
                                undefined);

                    if (self.assertEquals(result, false)) {
                        resolve();
                    } else {
                        reject();
                    }
                });

                self.recording = self.recordingSchedulerObject
                        .record(self.programme);
            });
    }

    bindToCurrentChannel() {
        this.previousState = this.UNREALIZED;
        this.addTransition(this.UNREALIZED, this.CONNECTING);
        this.addTransition(this.CONNECTING, this.PRESENTING, function() {
            console.log("Binding with current channel succeeded.");
        });

        this.vidBroadObj.bindToCurrentChannel();
    }

    findAFinishedProgramme() {
        var self = this;
        var metadataSearch = self.searchManagerObj.
        createSearch(self.SCHEDULED_CONTENT);
        var now = Math.round(Date.now() / 1000);
        var query = metadataSearch.createQuery("Programme.startTime", 4, now);
        metadataSearch.setQuery(query);

        var offset = 0;
        var count = 4;
        metadataSearch.result.getResults(offset, count);

        this.searchManagerObj.onMetadataSearch = function(search, state) {

            console.log("[INFO]: onMetadataSearch called");

            switch (state) {

                case 0:
                    if (!self.assertEquals(search.result.length, 0)) {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: found");
                        self.programme = search.result[0];
                        console.log(self.programme);
                    } else {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: not found");
                    }
                    break;

                default:
                    console.log("Unknow state");
                    reject();
            }
        };
    }

    findProgrammeFromStream() {
        var searchTarget = 1;
        var metaDataSearch = this.searchManagerObj.createSearch(searchTarget);
        var currentChannel = this.vidBroadObj.currentChannel;
        var startTime = null;
        metaDataSearch.findProgrammesFromStream(currentChannel, startTime, 1);
        var offset = 0;
        var count = 1;
        metaDataSearch.result.getResults(offset, count);
        var self = this;
        this.searchManagerObj.onMetadataSearch = function(search, state) {

            console.log("[INFO]: onMetadataSearch called");

            switch (state) {

                case 0:
                    if (self.assertNotNull(search.result[0])) {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: found");
                        self.programme = search.result[0];
                        console.log(self.programme);
                    } else {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: not found");
                    }
                    break;

                default:
                    console.log("Unknow state");
            }
        };
    }

    resetTransitionVariables() {
        this.currentIndex = 0;
        this.transitions = [];
        this.previousState = null;
    }
};


