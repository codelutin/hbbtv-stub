/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * Description:
 * This class gather a set of method necessary to test a search manager object.
 *
 */
var SearchManagerTest = class extends TestCase {

    constructor() {
        super();
        this.searchManagerObj = null;
    }

    beforeTest(resolve, reject) {
        this.createVideoBroadcastObject();
        this.searchManagerObj = oipfObjectFactory.createSearchManagerObject();
        this.onPlayStateChange = this.onChangeState.bind(this, resolve, reject);
        this.vidBroadObj.addEventListener("PlayStateChange", this.onPlayStateChange, false);
        this.transitions = [];
        this.previousState = this.vidBroadObj.playState;
        this.currentIndex = 0;
        this.channel = null;
        OipfUtils.logTest("State: Pending");
    }

    afterTest(resolve, reject) {
        this.vidBroadObj.removeEventListener("PlayStateChange",
        this.onPlayStateChange);
        this.searchManagerObj.removeEventListener("MetadataSearch",
        this.onPlayStateChange);
        this.timerManager.clearTimer();
    }

    testFindAFinishedProgramme(resolve, reject) {
        var self = this;
        var metadataSearch = self.searchManagerObj.
        createSearch(self.SCHEDULED_CONTENT);
        var now = Math.round(Date.now() / 1000);
        var query = metadataSearch.createQuery("Programme.startTime", 4, now);
        metadataSearch.setQuery(query);

        var offset = 0;
        var count = 4;
        metadataSearch.result.getResults(offset, count);

        this.searchManagerObj.onMetadataSearch = function(search, state) {

            console.log("[INFO]: onMetadataSearch called");

            switch (state) {

                case 0:
                    if (!self.assertEquals(search.result.length, 0)) {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: found");
                        resolve();
                    } else {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: not found");
                        reject();
                    }
                    break;

                default:
                    console.log("Unknow state");
                    reject();
            }
        };

    }

    testGetCurrentProgrammeFromStream(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {

            var stat = "Pending";
            OipfUtils.logTest("State: " + stat);
            var searchTarget = 1;
            var metaDataSearch = self.searchManagerObj.createSearch(searchTarget);
            var currentChannel = self.vidBroadObj.currentChannel;
            var startTime = null;
            metaDataSearch.findProgrammesFromStream(currentChannel, startTime);
            var offset = 0;
            var count = 1;
            metaDataSearch.result.getResults(offset, count);
//        console.log(metaDataSearch.result[0]);//Must return undefined else wrong implementation of getResults method
        });

        this.searchManagerObj.onMetadataSearch = function(search, state) {

            console.log("[INFO]: onMetadataSearch called");

            switch (state) {

                case 0:
                    if (self.assertNotNull(search.result[0])) {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: found");
                        resolve();
                    } else {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: not found");
                        reject();
                    }
                    break;

                case 3:
                    var message = "[INFO] MetadataSearch in Idle state because of either search abort or parameters have been modified (query, constraints or search target)";
                    console.log(message);
                    reject();
                    break;

                case 4:
                    message = "[INFO] The search cannot be complete because of lack of ressources or any other reason.";
                    console.log(message);
                    reject();
                    break;

                default:
                    console.log("Unknow state");
                    reject();

            }
        };

        this.vidBroadObj.bindToCurrentChannel();
    }

    testSearchingForASpecificProgrammeByName(resolve, reject) {
        var self = this;
        var value = "X:enius";

        var stat = "Pending";
        OipfUtils.logTest("State: " + stat);
        var metadataSearch = self.searchManagerObj.
        createSearch(self.SCHEDULED_CONTENT);

        var query = metadataSearch.createQuery("Programme.name", 0, value);
        metadataSearch.setQuery(query);

        var offset = 0;
        var count = 1;
        metadataSearch.result.getResults(offset, count);

        this.searchManagerObj.onMetadataSearch = function(search, state) {

            console.log("[INFO]: onMetadataSearch called");

            switch (state) {

                case 0:
                    if (self.assertNotNull(search.result[0]) &&
                        self.assertEquals(search.result[0].name, value)) {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: found");
                        resolve();
                    } else {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: not found");
                        reject();
                    }
                    break;

                case 3:
                    var message = "[INFO] MetadataSearch in Idle state because of either search abort or parameters have been modified (query, constraints or search target)";
                    console.log(message);
                    reject();
                    break;

                case 4:
                    message = "[INFO] The search cannot be complete because of lack of ressources or any other reason.";
                    console.log(message);
                    reject();
                    break;

                default:
                    console.log("Unknow state");
                    reject();

            }
        };

//        this.vidBroadObj.bindToCurrentChannel();

    }

    testSearchingForProgrammesWithDurationLowerThanOneHour(resolve, reject) {
        var self = this;
        var value = "3600";

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {

            var stat = "Pending";
            OipfUtils.logTest("State: " + stat);
            var metadataSearch = self.searchManagerObj.
            createSearch(self.SCHEDULED_CONTENT);

            var query = metadataSearch.createQuery("Programme.duration", 4,
            value);
            metadataSearch.setQuery(query);

            var offset = 0;
            var count = 6;
            metadataSearch.result.getResults(offset, count);
//        //Must return undefined else wrong implementation of getResults method
//        console.log(metaDataSearch.result[0]);
        });

        this.searchManagerObj.onMetadataSearch = function(search, state) {

            console.log("[INFO]: onMetadataSearch called");

            switch (state) {

                case 0:
                    if (self.assertEquals(search.result.length, 6)) {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: found");
                        resolve();
                    } else {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: not found");
                        reject();
                    }
                    break;

                default:
                    console.log("Unknow state");
                    reject();

            }
        };

        this.vidBroadObj.bindToCurrentChannel();

    }

    testAbortWhileProgrammesSearchWithoutConstraints(resolve, reject) {
        var value = 3600;
        var self = this;
        var stat = "Pending";
        OipfUtils.logTest("State: " + stat);
        var metadataSearch = this.searchManagerObj.
        createSearch(this.SCHEDULED_CONTENT);

        var query = metadataSearch.createQuery("duration", 4, value);
        metadataSearch.setQuery(query);

        var offset = 0;
        var count = 6;
        metadataSearch.result.getResults(offset, count);
//        //Must return undefined else wrong implementation of getResults method
//        console.log(metadataSearch.result[0]);
        metadataSearch.result.abort();
        resolve();


        this.searchManagerObj.onMetadataSearch = function(search, state) {

            console.log("[INFO]: onMetadataSearch called");

            switch (state) {

                case 0:
                    if (self.assertEquals(search.result.length, 6)) {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: found");
                        resolve();
                    } else {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: not found");
                        reject();
                    }
                    break;

                case 3:
                    var message = "[INFO] MetadataSearch in Idle state because of either search abort or parameters have been modified (query, constraints or search target)";
                    console.log(message);
                    break;

                default:
                    console.log("Unknow state");
                    reject();

            }
        };

    }

    testSetQueryWhileSearchWithoutConstraints(resolve, reject) {
        var value = 3600;
        var stat = "Pending";
        OipfUtils.logTest("State: " + stat);
        var metadataSearch = this.searchManagerObj.
        createSearch(this.SCHEDULED_CONTENT);

        var query = metadataSearch.createQuery("duration", 4, value);
        metadataSearch.setQuery(query);

        var offset = 0;
        var count = 6;
        metadataSearch.result.getResults(offset, count);
//        //Must return undefined else wrong implementation of getResults method
//        console.log(metadataSearch.result[0]);
        metadataSearch.setQuery(query);

        if (this.assertEquals(metadataSearch.result.length, 0)) {
            resolve();
        } else {
            reject();
        }

    }

    testFindProgrammesFromStreamWhileSearchWithoutConstraints(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {

            var stat = "Pending";
            OipfUtils.logTest("State: " + stat);
            var searchTarget = 1;
            var metadataSearch = self.searchManagerObj.createSearch(searchTarget);

            var currentChannel = self.vidBroadObj.currentChannel;
            var startTime = null;
            metadataSearch.findProgrammesFromStream(currentChannel, startTime);

            var offset = 0;
            var count = 1;
            metadataSearch.result.getResults(offset, count);

            metadataSearch.findProgrammesFromStream(currentChannel, startTime);

            if (self.assertEquals(metadataSearch.result.length, 0)) {
                resolve();
            } else {
                reject();
            }
        });
        this.vidBroadObj.bindToCurrentChannel();

    }

    testAddChannelConstraintWhileSearchWithoutConstraints(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {

            var stat = "Pending";
            OipfUtils.logTest("State: " + stat);
            var searchTarget = 1;
            var metadataSearch = self.searchManagerObj
            .createSearch(searchTarget);

            var currentChannel = self.vidBroadObj.currentChannel;
            var startTime = null;
            metadataSearch.findProgrammesFromStream(currentChannel, startTime);

            var offset = 0;
            var count = 1;
            metadataSearch.result.getResults(offset, count);

            metadataSearch.addChannelConstraint(currentChannel);

            if (self.assertEquals(metadataSearch.result.length, 0)) {
                resolve();
            } else {
                reject();
            }
        });

        this.vidBroadObj.bindToCurrentChannel();

    }

    testRemoveChannelConstraintWhileSearchWithoutConstraints(resolve, reject) {
        var value = 3600;
        var stat = "Pending";
        OipfUtils.logTest("State: " + stat);
        var metadataSearch = this.searchManagerObj.
        createSearch(this.SCHEDULED_CONTENT);

        var query = metadataSearch.createQuery("duration", 4, value);
        metadataSearch.setQuery(query);

        var offset = 0;
        var count = 6;
        metadataSearch.result.getResults(offset, count);
        metadataSearch.addChannelConstraint(null);

        if (this.assertEquals(metadataSearch.result.length, 0)) {
            resolve();
        } else {
            reject();
        }

    }

    testGetCurrentProgrammeFromNullStream(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {

            var stat = "Pending";
            OipfUtils.logTest("State: " + stat);
            var searchTarget = 1;
            var metaDataSearch = self.searchManagerObj.createSearch(searchTarget);
            var currentChannel = null;
            var startTime = null;
            new Promise(function(resolve, reject) {
                metaDataSearch.findProgrammesFromStream(currentChannel, startTime);
            })
            .catch(function(val) {
                console.log(val);
                resolve();
            });
        });

        this.vidBroadObj.bindToCurrentChannel();
    }

    testCreateSearchForScheduledContent(resolve, reject) {
        this.metadataSearch = this.searchManagerObj.
        createSearch(this.SCHEDULED_CONTENT);

        if (this.assertNotNull(this.metadataSearch) &&
            this.assertEquals(this.metadataSearch.searchTarget,
                                this.SCHEDULED_CONTENT)) {
            resolve();
        } else {
            reject();
        }
    }

    testCreateSearchForContentOnDemand(resolve, reject) {
        this.metadataSearch = this.searchManagerObj.
        createSearch(this.CONTENT_ON_DEMAND);

        if (this.assertNotNull(this.metadataSearch) &&
            this.assertEquals(this.metadataSearch.searchTarget,
                                this.CONTENT_ON_DEMAND)) {
            resolve();
        } else {
            reject();
        }
    }

    testCreateSearchWithNull(resolve, reject) {
        this.metadataSearch = this.searchManagerObj.
        createSearch(null);

        if (!this.assertNotNull(this.metadataSearch)) {
            resolve();
        } else {
            reject();
        }
    }

    testCreateSearchWithString(resolve, reject) {
        this.metadataSearch = this.searchManagerObj.
        createSearch("1");

        if (!this.assertNotNull(this.metadataSearch)) {
            resolve();
        } else {
            reject();
        }
    }

    testCreateSearchWithZero(resolve, reject) {
        this.metadataSearch = this.searchManagerObj.
        createSearch(0);

        if (!this.assertNotNull(this.metadataSearch)) {
            resolve();
        } else {
            reject();
        }
    }

    testCreateQueryWithWellArguments(resolve, reject) {
        var metadataSearch = this.searchManagerObj.
        createSearch(this.SCHEDULED_CONTENT);

        var query = metadataSearch.createQuery("name", 0, "X:ENIUS");

        if (this.assertNotNull(query)) {
            resolve();
        } else {
            reject();
        }

    }

    testCreateQueryWithoutParameters(resolve, reject) {
        var metadataSearch = this.searchManagerObj.
        createSearch(this.SCHEDULED_CONTENT);

        new Promise(function(resolve, reject) {
            var query = metadataSearch.createQuery();
        })
        .catch(function(val) {
            console.log(val);
            resolve();
        });

    }

    testCreateQueryWithFieldArgumentNull(resolve, reject) {
        var metadataSearch = this.searchManagerObj.
        createSearch(this.SCHEDULED_CONTENT);


        new Promise(function(resolve, reject) {
            var query = metadataSearch.createQuery(null, 0, "X:ENIUS");
        })
        .catch(function(val) {
            console.log(val);
            resolve();
        });

    }

    testCreateQueryWithIntegerFieldArgument(resolve, reject) {
        var metadataSearch = this.searchManagerObj.
        createSearch(this.SCHEDULED_CONTENT);


        new Promise(function(resolve, reject) {
            var query = metadataSearch.createQuery(10, 0, "X:ENIUS");
        })
        .catch(function(val) {
            console.log(val);
            resolve();
        });

    }

    testCreateQueryWithComparisonArgumentNull(resolve, reject) {
        var metadataSearch = this.searchManagerObj.
        createSearch(this.SCHEDULED_CONTENT);


        new Promise(function(resolve, reject) {
            metadataSearch.createQuery("name", null, "X:ENIUS");
        })
        .catch(function(val) {
            console.log(val);
            resolve();
        });

    }

    testCreateQueryWithStringComparisonArgument(resolve, reject) {
        var metadataSearch = this.searchManagerObj.
        createSearch(this.SCHEDULED_CONTENT);


        new Promise(function(resolve, reject) {
            metadataSearch.createQuery("name", "null", "X:ENIUS");
        })
        .catch(function(val) {
            console.log(val);
            resolve();
        });

    }

    testCreateQueryWithValueArgumentNull(resolve, reject) {
        var metadataSearch = this.searchManagerObj.
        createSearch(this.SCHEDULED_CONTENT);

        new Promise(function(resolve, reject) {
            metadataSearch.createQuery("name", 0, null);
        })
        .catch(function(val) {
            console.log(val);
            resolve();
        });

    }

    testAnd(resolve, reject) {
        var metadataSearch = this.searchManagerObj.
        createSearch(this.SCHEDULED_CONTENT);

        var queryA = metadataSearch.createQuery("name", 0, "X:ENIUS");
        var queryB = metadataSearch.createQuery("name", 0, "X:ENIUS");

        var newQuery = queryA.and(queryB);

        if (this.assertNotNull(newQuery) &&
            !this.assertEquals(newQuery, queryA)) {

            resolve();
        } else {
            reject();
        }

    }

    testOr(resolve, reject) {
        var metadataSearch = this.searchManagerObj.
        createSearch(this.SCHEDULED_CONTENT);

        var queryA = metadataSearch.createQuery("name", 0, "X:ENIUS");
        var queryB = metadataSearch.createQuery("name", 0, "X:ENIUS");

        var newQuery = queryA.or(queryB);

        if (this.assertNotNull(newQuery) &&
            !this.assertEquals(newQuery, queryA)) {

            resolve();
        } else {
            reject();
        }

    }

    testProgrammePropertiesImplementationState() {
        var self = this;
        var value = "X:ENIUS";

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {

            var stat = "Pending";
            OipfUtils.logTest("State: " + stat);
            var metadataSearch = self.searchManagerObj.
            createSearch(self.SCHEDULED_CONTENT);

            var query = metadataSearch.createQuery("Programme.name", 0,
            value);
            metadataSearch.setQuery(query);

            var offset = 0;
            var count = 1;
            metadataSearch.result.getResults(offset, count);
//        //Must return undefined else wrong implementation of getResults method
//        console.log(metaDataSearch.result[0]);
        });

        this.searchManagerObj.onMetadataSearch = function(search, state) {

            console.log("[INFO]: onMetadataSearch called");

            switch (state) {

                case 0:
                    if (self.assertNotNull(search.result[0]) &&
                        self.assertEquals(search.result[0].name, value)) {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: found");
                        resolve();
                    } else {
                        console.log("[TEST-RUNNING][Info] Label: Obtain current program according to oipf norm, State: not found");
                        reject();
                    }
                    break;

                case 3:
                    var message = "[INFO] MetadataSearch in Idle state because of either search abort or parameters have been modified (query, constraints or search target)";
                    console.log(message);
                    reject();
                    break;

                case 4:
                    message = "[INFO] The search cannot be complete because of lack of ressources or any other reason.";
                    console.log(message);
                    reject();
                    break;

                default:
                    console.log("Unknow state");
                    reject();

            }
        };

        this.vidBroadObj.bindToCurrentChannel();
    }

};
