/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
class Test {

    constructor() {
        this.testsObjects = [{
            name: "OipfFactoryTest",
            object: new OipfFactoryTest(),
            tests: [
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'application/oipfApplicationManager'.",
                    method: "testIsApplicationManagerObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'application/oipfCapabilities'.",
                    method: "testIsCapabilitiesObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'channelConfig'.",
                    method: "testIsChannelConfigSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'application/oipfCodManager'.",
                    method: "testIsCodManagerObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'application/oipfConfiguration'.",
                    method: "testIsConfigurationObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'application/oipfDownloadManager'.",
                    method: "testIsDownloadManagerObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'application/oipfDownloadTrigger'.",
                    method: "testIsDownloadTriggerObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'application/oipfDrmAgent'.",
                    method: "testIsDrmAgentObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'application/oipfGatewayInfo'.",
                    method: "testIsGatewayInfoObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'application/oipfCommunicationServices'.",
                    method: "testIsIMSObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'application/oipfMDTF'.",
                    method: "testIsMDTFObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'application/notifsocket'.",
                    method: "testIsNotifSocketObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'application/oipfParentalControlManager'.",
                    method: "testIsParentalControlManagerObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'application/oipfRecordingScheduler'.",
                    method: "testIsRecordingSchedulerObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'application/oipfRemoteControlFunction'.",
                    method: "testIsRemoteControlFunctionObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'application/oipfRemoteManagement'.",
                    method: "testIsRemoteManagementObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'application/oipfSearchManager'.",
                    method: "testIsSearchManagerObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'application/oipfStatusView'.",
                    method: "testIsStatusViewObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'video/broadcast'.",
                    method: "testIsVideoBroadcastObjectSupported"
                },
                {
                    label: "Call the method 'isObjectSupported' with mime-type 'video/mpeg'.",
                    method: "testIsVideoMpegObjectSupported"
                },
                {
                    label: "Call the method 'createApplicationManagerObject of oifp factory.",
                    method: "testCreateApplicationManagerObject"
                },
                {
                    label: "Call the method createCapabilitiesObject of oifp factory.",
                    method: "testCreateCapabilitiesObject"
                },
                {
                    label: "Call the method createChannelConfig of oifp factory.",
                    method: "testCreateChannelConfig"
                },
                {
                    label: "Call the method createCodManagerObject of oifp factory.",
                    method: "testCreateCodManagerObject"
                },
                {
                    label: "Call the method createConfigurationObject of oifp factory.",
                    method: "testCreateConfigurationObject"
                },
                {
                    label: "Call the method createDownloadManagerObject of oifp factory.",
                    method: "testCreateDownloadManagerObject"
                },
                {
                    label: "Call the method createDownloadTriggerObject of oifp factory.",
                    method: "testCreateDownloadTriggerObject"
                },
                {
                    label: "Call the method createDrmAgentObject of oifp factory.",
                    method: "testCreateDrmAgentObject"
                },
                {
                    label: "Call the method createGatewayInfoObject of oifp factory.",
                    method: "testCreateGatewayInfoObject"
                },
                {
                    label: "Call the method createImsObject of oifp factory.",
                    method: "testCreateIMSObject"
                },
                {
                    label: "Call the method createMdtfObject of oifp factory.",
                    method: "testCreateMDTFObject"
                },
                {
                    label: "Call the method createNotifSocketObject of oifp factory.",
                    method: "testCreateNotifSocketObject"
                },
                {
                    label: "Call the method createParentalControlManagerObject of oifp factory.",
                    method: "testCreateParentalControlManagerObject"
                },
                {
                    label: "Call the method createRecordingSchedulerObject of oifp factory.",
                    method: "testCreateRecordingSchedulerObject"
                },
                {
                    label: "Call the method createRemoteControlFunctionObject of oifp factory.",
                    method: "testCreateRemoteControlFunctionObject"
                },
                {
                    label: "Call the method createRemoteManagementObject of oifp factory.",
                    method: "testCreateRemoteManagementObject"
                },
                {
                    label: "Call the method createSearchManagerObject of oifp factory.",
                    method: "testCreateSearchManagerObject"
                },
                {
                    label: "Call the method createStatusViewObject of oifp factory.",
                    method: "testCreateStatusViewObject"
                },
                {
                    label: "Call the method createVideoBroadcastObject of oifp factory.",
                    method: "testCreateVideoBroadcastObject"
                },
                {
                    label: "Call the method createVideoMpegObject of oifp factory.",
                    method: "testCreateVideoMpegObject"
                }
            ]},{
            name: "VideoBroadcastTest",
            object: new VideoBroadcastTest(),
            tests: [
                {
                    label: "Call 'bindToCurrentChannel' when there isn't channel presentation.",
                    method: "testBindToCurrentChannelWhenNoChannelPresentation"
                },
                {
                    label: "Bind the current channel stream with a video broadcast object while this one is in connecting state.",
                    method: "testBindToCurrentChannelInConnecting"
                },
                {
                    label: "Bind the current channel stream with a video broadcast object while this one is in presenting state.",
                    method: "testBindToCurrentChannelInPresenting"
                },
                {
                    label: "Bind the current channel stream with a video broadcast object while this one is in stopped state.",
                    method: "testBindToCurrentChannelInStopped"
                },
                {
                    label: "Bind the current channel stream with a video broadcast object while this one is in stopped state.",
                    method: "testBindToCurrentChannelInStoppedWhenNoTuner"
                },
                {
                    label: "Call 'bindToCurrentChannel' twice.",
                    method: "testDoubleBindToCurrentChannel"
                },
                {
                    label: "Switch to precedent channel in unrealized state.",
                    method: "testPrevChannelInUnrealized"
                },
                {
                    label: "Switch to precedent channel in connecting state.",
                    method: "testPrevChannelInConnecting"
                },
                {
                    label: "Switch to precedent channel in presenting state.",
                    method: "testPrevChannelInPresenting"
                },
                {
                    label: "Switch to precedent channel in stopped state.",
                    method: "testPrevChannelInStopped"
                },
                {
                    label: "Switch to next channel in unrealized state.",
                    method: "testNextChannelInUnrealized"
                },
                {
                    label: "Switch to next channel in connecting state.",
                    method: "testNextChannelInConnecting"
                },
                {
                    label: "Switch to next channel in presenting state.",
                    method: "testNextChannelInPresenting"
                },
                {
                    label: "Switch to next channel in stopped state.",
                    method: "testNextChannelInStopped"
                },
                {
                    label: "Release any tuner and/or ressources held by a video broadcast object in connecting.",
                    method: "testReleaseInConnecting"
                },
                {
                    label: "Release any tuner and/or ressources held by a video broadcast object in presenting.",
                    method: "testReleaseInPresenting"
                },
                {
                    label: "Release any tuner and/or ressources held by a video broadcast object in stopped.",
                    method: "testReleaseInStopped"
                },
                {
                    label: "Stop the video and audio presentation of video broadcast object in connecting.",
                    method: "testStopInConnecting"
                },
                {
                    label: "Stop the video and audio presentation of video broadcast object in presenting.",
                    method: "testStopInPresenting"
                },
                {
                    label: "Set the volume of current VideoBroadcastObject to 15.",
                    method: "testSetVolume"
                },
                {
                    label: "Don't pass argument to method which set the volume of Video-Broadcast Object.",
                    method: "testSetVolumeWithoutArgument"
                },
                {
                    label: "Set the volume of current VideoBroadcastObject with a float value.",
                    method: "testSetVolumeWithFloatArgument"
                },
                {
                    label: "Set the volume of current VideoBroadcastObject with a float value superior of bound max.",
                    method: "testSetVolumeWithFloatUnderBoundMin"
                },
                {
                    label: "Set the volume of current VideoBroadcastObject with a float value inferior of bound min.",
                    method: "testSetVolumeWithFloatAboveBoundMax"
                },
                {
                    label: "Set the volume of current VideoBroadcastObject inferior to 0.",
                    method: "testSetVolumeWithValueUnderBoundMin"
                },
                {
                    label: "Set the volume of current VideoBroadcastObject superior to 100.",
                    method: "testSetVolumeWithValueAboveBoundMax"
                },
                {
                    label: "Get the volume of current VideoBroadcastObject.",
                    method: "testGetVolume"
                },
                {
                    label: "Set a channel in unrealized state.",
                    method: "testSetChannelInUnrealized"
                },
                {
                    label: "Set a channel in connecting state.",
                    method: "testSetChannelInConnecting"
                },
                {
                    label: "Call method 'setChannel' with null in connecting state.",
                    method: "testSetChannelInConnectingWithNull"
                },
                {
                    label: "Set a channel in presenting state.",
                    method: "testSetChannelInPresenting"
                },
                {
                    label: "Call method 'setChannel' with null in presenting state.",
                    method: "testSetChannelInPresentingWithNull"
                },
                {
                    label: "Set a channel in stopped state.",
                    method: "testSetChannelInStopped"
                },
                {
                    label: "Call method 'setChannel' with null in stopped state.",
                    method: "testSetChannelInStoppedWithNull"
                }
            ]},{
            name: "SearchManagerTest",
            object: new SearchManagerTest(),
            tests: [
                {
                    label: "Verify the right creation of search object.",
                    method: "testCreateSearchForScheduledContent"
                },
                {
                    label: "Verify the right creation of search object about content on demand.",
                    method: "testCreateSearchForContentOnDemand"
                },
                {
                    label: "Call method 'createSearch' with parameter equals to null.",
                    method: "testCreateSearchWithNull"
                },
                {
                    label: "Call method 'createSearch' with a String parameter.",
                    method: "testCreateSearchWithString"
                },
                {
                    label: "Call method 'createSearch' with parameter equals to 0.",
                    method: "testCreateSearchWithZero"
                },
                {
                    label: "Looking for a programme already broadcasted.",
                    method: "testFindAFinishedProgramme"
                },
                {
                    label: "Obtain current program according to oipf norm.",
                    method: "testGetCurrentProgrammeFromStream"
                },
                {
                    label: "Try to obtain current program with a null stream.",
                    method: "testGetCurrentProgrammeFromNullStream"
                },
                {
                    label: "Try to obtain a programme by its name.",
                    method: "testSearchingForASpecificProgrammeByName"
                },
                {
                    label: "Try to obtain all programme \n\
                            with duration lower than one hour.",
                    method: "testSearchingForProgrammesWithDurationLowerThanOneHour"
                },
                {
                    label: "Create a query with well parameters.",
                    method: "testCreateQueryWithWellArguments"
                },
                {
                    label: "Create a query with well parameters.",
                    method: "testCreateQueryWithoutParameters"
                },
                {
                    label: "Create a query with its first parameters null.",
                    method: "testCreateQueryWithFieldArgumentNull"
                },
                {
                    label: "Create a query with a Integer as first parameter.",
                    method: "testCreateQueryWithIntegerFieldArgument"
                },
                {
                    label: "Create a query with its second parameters.",
                    method: "testCreateQueryWithComparisonArgumentNull"
                },
                {
                    label: "Create a query with a String as second parameters.",
                    method: "testCreateQueryWithStringComparisonArgument"
                },
                {
                    label: "Create a query with its last parameters.",
                    method: "testCreateQueryWithValueArgumentNull"
                },
                {
                    label: "Modify query while search performing by calling 'setQuery'.",
                    method: "testSetQueryWhileSearchWithoutConstraints"
                },
                {
                    label: "Call 'findProgrammesFromStream' while search performing.",
                    method: "testFindProgrammesFromStreamWhileSearchWithoutConstraints"
                },
                {
                    label: "Call 'addChannelConstraint' while search performing.",
                    method: "testAddChannelConstraintWhileSearchWithoutConstraints"
                },
                {
                    label: "Call 'removeChannelConstraint' while search performing.",
                    method: "testRemoveChannelConstraintWhileSearchWithoutConstraints"
                },
                {
                    label: "Abort a search.",
                    method: "testAbortWhileProgrammesSearchWithoutConstraints"
                },
                {
                    label: "Call method 'or' of a 'Query' object.",
                    method: "testAnd"
                },
                {
                    label: "Call method 'and' of a 'Query' object.",
                    method: "testOr"
                }
            ]},{
            name: "ConfigurationTest",
            object: new ConfigurationTest(),
            tests: [
                {
                    label: "Call method 'setScreenSize' with right parameters(Type and value).",
                    method: "testSetScreenSize"
                },
                {
                    label: "Call method 'setScreenSize' without parameters.",
                    method: "testSetScreenSizeWithoutParameters"
                },
                {
                    label: "Call method 'setScreenSize' with 'String' parameters.",
                    method: "testSetScreenSizeWithUnexpectedTypeParameters"
                },
                {
                    label: "Call method 'setScreenSize' with an excessive number of parameters.",
                    method: "testSetScreenSizeWithExcessiveParametersNumber"
                },
                {
                    label: "Call method 'setTvStandard' with right parameters(Type and value).",
                    method: "testSetTvStandard"
                },
                {
                    label: "Call method 'setTvStandard' without parameters.",
                    method: "testSetTvStandardWihtoutParameters"
                },
                {
                    label: "Call method 'setTvStandard' with wrong parameters(Type).",
                    method: "testSetTvStandardWithUnexpectedTypeParameters"
                },
                {
                    label: "Call method 'setTvStandard' with an excessive number of parameters.",
                    method: "testSetTvStandardWithExcessiveParametersNumber"
                },
                {
                    label: "Call method 'setPvrSupport' with right parameters(Type and value).",
                    method: "testSetPvrSupport"
                },
                {
                    label: "Call method 'setPvrSupport' without parameters.",
                    method: "testSetPvrSupportWithoutParameters"
                },
                {
                    label: "Call method 'setPvrSupport' with wrong parameters(Type).",
                    method: "testSetPvrSupportWithUnexpectedTypeParameters"
                },
                {
                    label: "Call method 'setPvrSupport' with an excessive number of parameters.",
                    method: "testSetPvrSupportWithExcessiveParametersNumber"
                },
                {
                    label: "Call method 'setPowerState' with right parameters(Type and value).",
                    method: "testSetPowerState"
                },
                {
                    label: "Call method 'setPowerState' without parameters.",
                    method: "testSetPowerStateWithoutParameters"
                },
                {
                    label: "Call method 'setPowerState' with wrong parameters(Type).",
                    method: "testSetPowerStateWithUnexpectedTypeParameters"
                },
                {
                    label: "Call method 'setPowerState' with an excessive number of parameters.",
                    method: "testSetPowerStateWithExcessiveParametersNumber"
                },
                {
                    label: "Call method 'setDigestCredentials' with right parameters(Type and value).",
                    method: "testSetDigestCredentials"
                },
                {
                    label: "Call method 'setDigestCredentials' without parameters.",
                    method: "testSetDigestCredentialsWithoutParameters"
                },
                {
                    label: "Call method 'setDigestCredentials' with wrong parameters(Type).",
                    method: "testSetDigestCredentialsWithUnexpectedTypeParameters"
                },
                {
                    label: "Call method 'setDigestCredentials' with an excessive number of parameters.",
                    method: "testSetDigestCredentialsWithExcessiveParametersNumber"
                },
                {
                    label: "Call method 'clearDigestCredentials' with right parameters(Type and value).",
                    method: "testClearDigestCredentials"
                },
                {
                    label: "Call method 'clearDigestCredentials' without parameters.",
                    method: "testClearDigestCredentialsWithoutParameters"
                },
                {
                    label: "Call method 'clearDigestCredentials' with wrong parameters(Type).",
                    method: "testClearDigestCredentialsWithUnexpectedTypeParameters"
                },
                {
                    label: "Call method 'clearDigestCredentials' with an excessive number of parameters.",
                    method: "testClearDigestCredentialsWithExcessiveParametersNumber"
                },
                {
                    label: "Call method 'hasDigestCredentials' with right parameters(Type and value).",
                    method: "testHasDigestCredentials"
                },
                {
                    label: "Call method 'hasDigestCredentials' without parameters.",
                    method: "testHasDigestCredentialsWithoutParameters"
                },
                {
                    label: "Call method 'hasDigestCredentials' with wrong parameters(Type).",
                    method: "testHasDigestCredentialsWithUnexpectedTypeParameters"
                },
                {
                    label: "Call method 'hasDigestCredentials' with an excessive number of parameters.",
                    method: "testHasDigestCredentialsWithExcessiveParametersNumber"
                }
            ]},{
            name: "RecordingSchedulerTest",
            object: new RecordingSchedulerTest(),
            tests: [
                {
                    label: "Call method 'setRecord' in order to record current programme.",
                    method: "testRecordCurrentProgramme"
                },
                {
                    label: "Try to call method 'setRecord' with a finished programme.",
                    method: "testRecordAFinishedProgramme"
                },
                {
                    label: "Call method 'setRecordAt' in order to record current programme.",
                    method: "testRecordAtCurrentProgramme"
                },
                {
                    label: "Call method 'stop' after a scheduling recording.",
                    method: "testStopAScheduledRecording"
                },
                {
                    label: "Call method 'stop' while a recording.",
                    method: "testStopAnOngoingRecording"
                },
                {
                    label: "Call method 'stop' at the end of a recording.",
                    method: "testStopACompletedRecording"
                },
                {
                    label: "Call method 'remove' after a scheduling recording.",
                    method: "testRemoveAScheduledRecording"
                },
                {
                    label: "Call method 'remove' while a recording.",
                    method: "testRemoveAnOngoingRecording"
                },
                {
                    label: "Call method 'remove' at the end of a recording.",
                    method: "testRemoveACompletedRecording"
                },
                {
                    label: "Call method 'update' to update start time of a scheduled recording",
                    method: "testUpdateAScheduledRecordingOnStartTime"
                },
                {
                    label: "Call method 'update' to update duration of a scheduled recording.",
                    method: "testUpdateAScheduledRecordingOnDuration"
                },
                {
                    label: "Call method 'update' to update duration of an in-progress recording.",
                    method: "testUpdateAnOngoingRecordingWithAValidDuration"
                },
                {
                    label: "Call method 'update' to update duration of an in-progress recording with a short duration.",
                    method: "testUpdateAnOngoingRecordingWithATooShortDuration"
                },
                {
                    label: "Call method 'update' to update start time of an in-progress recording.",
                    method: "testUpdateAnOngoingRecordingOnStartTime"
                },
                {
                    label: "Call method 'update' to update repetition frequency of an in-progress recording.",
                    method: "testUpdateAnOngoingRecordingOnRepeatDays"
                }
            ]},{
            name: "CapabilitiesTest",
            object: new CapabilitiesTest(),
            tests: [
                {
                    label: "Call method 'hasCapability' with an invalid profile name.",
                    method: "testHasCapabilityWithAnInvalidProfileName"
                },
                {
                    label: "Call method 'hasCapability' to verify that the TV support at least one of pre-defined base UI profiles.",
                    method: "testHasCapabilityForAtLeastOneBaseProfileName"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support all pre-defined UI fragments profiles.",
                    method: "testHasCapabilityForAllUIFragmentProfile"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'TRICKMODE'.",
                    method: "testHasCapabilityTRICKMODE"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'AVCAD'.",
                    method: "testHasCapabilityAVCAD"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'DL'.",
                    method: "testHasCapabilityDL"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'IPTV_SDS'.",
                    method: "testHasCapabilityIPTV_SDS"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'IPTV_URI'.",
                    method: "testHasCapabilityIPTV_URI"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'ANA'.",
                    method: "testHasCapabilityANA"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'DVB_C'.",
                    method: "testHasCapabilityDVB_C"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'DVB_T'.",
                    method: "testHasCapabilityDVB_T"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'DVB_S'.",
                    method: "testHasCapabilityDVB_S"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'DVB_C2'.",
                    method: "testHasCapabilityDVB_C2"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'DVB_T2'.",
                    method: "testHasCapabilityDVB_T2"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'DVB_S2'.",
                    method: "testHasCapabilityDVB_S2"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'ISDB_C'.",
                    method: "testHasCapabilityISDB_C"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'ISDB_T'.",
                    method: "testHasCapabilityISDB_T"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'ISDB_S'.",
                    method: "testHasCapabilityISDB_S"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'META_BCG'.",
                    method: "testHasCapabilityMETA_BCG"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'META_EIT'.",
                    method: "testHasCapabilityMETA_EIT"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'META_SI'.",
                    method: "testHasCapabilityMETA_SI"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'ITV_KEYS'.",
                    method: "testHasCapabilityITV_KEYS"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature 'CONTROLLED'.",
                    method: "testHasCapabilityCONTROLLED"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature recording(UI profile name fragment: 'PVR').",
                    method: "testHasCapabilityPVR"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature recording(UI profile name fragment: 'DRM').",
                    method: "testHasCapabilityDRM"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature recording(UI profile name fragment: 'CommunicationServices').",
                    method: "testHasCapabilityCommunicationServices"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature recording(UI profile name fragment: 'SVG').",
                    method: "testHasCapabilitySVG"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature recording(UI profile name fragment: 'POINTER').",
                    method: "testHasCapabilityPOINTER"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature recording(UI profile name fragment: 'POLLNOTIF').",
                    method: "testHasCapabilityPOLLNOTIF"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature recording(UI profile name fragment: 'HTML5_MEDIA').",
                    method: "testHasCapabilityHtml5Media"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature recording(UI profile name fragment: 'WIDGETS').",
                    method: "testHasCapabilityWIDGETS"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature recording(UI profile name fragment: 'RCF').",
                    method: "testHasCapabilityRCF"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature recording(UI profile name fragment: 'TELEPHONY').",
                    method: "testHasCapabilityTELEPHONY"
                },
                {
                    label: "Call method 'hasCapability' to verify if the TV support the feature recording(UI profile name fragment: 'VIDEOTELEPHONY').",
                    method: "testHasCapabilityVIDEOTELEPHONY"
                }
            ]},{
            name: "ParentalControlManagerTest",
            object: new ParentalControlManagerTest(),
            tests: [
                {
                    label: "Call method 'setParentalControlPIN' with valids parameters.",
                    method: "testSetParentalControlPINWithValidOldPINAndNewPIN"
                },
                {
                    label: "Call method 'setParentalControlPIN' with a invalid pin but a valid new pin.",
                    method: "testSetParentalControlPINWithInvalidOldPINAndValidNewPIN"
                },
                {
                    label: "Call method 'setParentalControlPIN' with a invalid pin but a valid new pin.",
                    method: "testSetParentalControlPINWithInvalidOldPINAndValidNewPIN10Times"
                },
                {
                    label: "Call method 'verifyParentalControlPIN' with an invalid pin 10 times.",
                    method: "testVerifyParentalControlPINWithInvalidPIN10Times"
                },
                {
                    label: "Call method 'verifyParentalControlPIN' with a valid pin.",
                    method: "testVerifyParentalControlPINWithValidPIN"
                },
                {
                    label: "Call method 'verifyParentalControlPIN' with an invalid pin.",
                    method: "testVerifyParentalControlPINWithInvalidPIN"
                },
                {
                    label: "Call method 'setBlockUnrated' with an invalid pin.",
                    method: "testSetBlockUnratedWithInvalidPIN10Times"
                },
                {
                    label: "Call method 'getBlockUnrated' when contents have not been blocked.",
                    method: "testGetBlockUnratedWhenUnratedContentsAreNotBlocked"
                },
                {
                    label: "Call method 'getBlockUnrated' when contents have been blocked.",
                    method: "testGetBlockUnratedWhenUnratedContentsAreBlocked"
                },
                {
                    label: "Call method 'setBlockUnrated' with a valid pin and value 'true' to blocked unrated contents.",
                    method: "testSetBlockUnratedWithValidPINAndTrue"
                },
                {
                    label: "Call method 'setBlockUnrated' with a valid pin and value 'false' to don't blocked unrated contents.",
                    method: "testSetBlockUnratedWithValidPINAndFalse"
                },
                {
                    label: "Call method 'setBlockUnrated' with an invalid pin and value 'true' to blocked unrated contents.",
                    method: "testSetBlockUnratedWithInvalidPINAndTrue"
                },
                {
                    label: "Call method 'setBlockUnrated' with an invalid pin and value 'false' to don't blocked unrated contents.",
                    method: "testSetBlockUnratedWithInvalidPINAndFalse"
                },
                {
                    label: "Call method 'setParentalControlStatus' with a valid pin\
                            and value 'true' to active parental control.",
                    method: "testSetParentalControlStatusWithValidPINAndTrue"
                },
                {
                    label: "Call method 'setParentalControlStatus' with a valid pin\
                            and value 'false' to desactive parental control.",
                    method: "testSetParentalControlStatusWithValidPINAndFalse"
                },
                {
                    label: "Call method 'setParentalControlStatus' with an invalid\
                            pin and value 'true' to active parental control.",
                    method: "testSetParentalControlStatusWithInvalidPINAndTrue"
                },
                {
                    label: "Call method 'setParentalControlStatus' with an invalid\
                            pin and value 'false' to desactive parental control.",
                    method: "testSetParentalControlStatusWithInvalidPINAndFalse"
                },
                {
                    label: "Call method 'setParentalControlStatus' with an invalid\
                            pin and value 'false' to verify the PIN entry locking",
                    method: "testSetParentalControlStatusWithInvalidOldPINAndValidNewPIN10Times"
                },
                {
                    label: "Call method 'getParentalControlStatus' when parental\
                            control status is activated.",
                    method: "testGetParentalControlStatusWhenParentalControlStatusActivated"
                },
                {
                    label: "Call method 'getParentalControlStatus' when parental\
                            control status is deactivated.",
                    method: "testGetParentalControlStatusWhenParentalControlStatusDeactivated"
                }
            ]}/*,{
            name: "ApplicationManagerTest",
            object: new ApplicationManagerTest(),
            tests: [
                {
                    label: "Call method 'getOwnerApplication' with 'object document' of global property 'window'.",
                    method: "testGetOwnerApplicationOfDocumentOfGlobalPropertyWindow"
                },
                {
                    label: "Call method 'getOwnerApplication' with a parameter null.",
                    method: "testGetOwnerApplicationWithNull"
                }
            ]
        }*/];

        this.animation = {
            i: 1,
            start: true,
            intervalID: 0
        };

        this.sectionsID = [];
    }

    runAll() {
        this.results = [];
        this.currentResult = {
            succeeded: 0,
            failed: 0
        };
        var style = document.getElementById("running").style;
        style.display = (!style.display || style.display == "none") ? "block" : "none";
        var style = document.getElementById("globalTestResultsDiv").style;
        style.display = (!style.display || style.display == "none") ? "block" : "none";
        this.runLoop(0, 0).then(this.totalPercentage.bind(this));
    }

    runLoop(indexObject, indexTest) {
        var element = this.testsObjects[indexObject];
        var object = element.object;
        var tests = element.tests;

        var self = this;
        var loop = function() {
            if (indexTest < tests.length - 1) {
                return self.runLoop(indexObject, indexTest + 1);

            } else if (indexObject < self.testsObjects.length - 1) {
                element.results = {
                    testName: "",
                    failed: 0,
                    succeeded: 0
                };
                element.results.testName = element.name;
                element.results.failed = self.currentResult.failed;
                element.results.succeeded = self.currentResult.succeeded;
                self.currentResult.failed = 0;
                self.currentResult.succeeded = 0;
                return self.runLoop(indexObject + 1, 0);

            }
            if (indexObject == self.testsObjects.length - 1) {
                element.results = {
                    testName: "",
                    failed: 0,
                    succeeded: 0
                };
                element.results.testName = element.name;
                element.results.failed = self.currentResult.failed;
                element.results.succeeded = self.currentResult.succeeded;
            }
        };

        return this.runTest(tests[indexTest], object)
        .then(loop).catch(loop);

    }

    percentageCalculating(currentResult) {
        this.round = Math.round.bind(Math);
        var testNumber = currentResult.failed + currentResult.succeeded;
        currentResult.failed = this.round(currentResult.failed / testNumber * 100);
        currentResult.succeeded = this.round(currentResult.succeeded / testNumber * 100);
    }

    totalPercentage() {
        this.resultsPercentageFailed = 0;
        this.resultsPercentageSucceeded = 0;

        for (var i = 0, l = this.testsObjects.length; i < l; i++) {

            this.percentageCalculating(this.testsObjects[i].results);
            this.resultsPercentageFailed += this.testsObjects[i].results.failed;
            this.resultsPercentageSucceeded += this.testsObjects[i].results.succeeded;
            this.results.push(this.testsObjects[i].results);
        }
        this.resultsPercentageFailed = this.resultsPercentageFailed / this.testsObjects.length;
        this.resultsPercentageSucceeded = this.resultsPercentageSucceeded / this.testsObjects.length;
        this.displayGlobalTestResults();
    }

    displayGlobalTestResults() {
        var id = "globalTestResultsTable";
        var tableResult = document.getElementById(id);
        var content = "";
        content += "<table>";
        content += "<tbody>";
        content += "<thead>";
        content += "<tr>";
        content += "<th></th>";
        content += "<th>Succeeded</th>";
        content += "<th>Failed</th>";
        content += "</tr>";
        content += "</thead>";
        for (var i = 0, l = this.results.length; i < l; i++) {
            content += "<tr>";
            content += "<th>" + this.results[i].testName + "</th>";
            content += "<td>" + this.results[i].succeeded + "%</td>";
            content += "<td>" + this.results[i].failed + "%</td>";
            content += "</tr>";
        }
        content += "<tr>";
        content += "<th>Total</th>";
        content += "<td>" + this.resultsPercentageSucceeded + "%</td>";
        content += "<td>" + this.resultsPercentageFailed + "%</td>";
        content += "</tr>";
        content += "</tbody>";
        content += "</table>";

        tableResult.innerHTML = content;

//        var style = document.getElementById("globalTestResultsDiv").style;
//        style.display = (!style.display || style.display == "none") ? "block" : "none";
        var style = document.getElementById("running").style;
        style.display = (!style.display || style.display == "none") ? "block" : "none";
    }

    runSectionTest(indexObject, indexTest) {
        var element = this.testsObjects[indexObject];
        var object = element.object;
        var tests = element.tests;

        var self = this;
        var loop = function() {
            if (indexTest < tests.length - 1) {
                self.runSectionTest(indexObject, indexTest + 1);
            }
        };

        this.runTest(tests[indexTest], object)
        .then(loop).catch(loop);
    }

    displayInLineArray() {
        var content = "<div id='testControllerDiv'>";
        content +=  "<div id='titleTestControllerDiv'><h4>All tests</h4></div>";
        content +=      "<button id='runAllBtn'>Run All</button>";
        content +=      "<button id='displayConfigBtn'>Hide/Show</button>";
        content +=      "<div id='globalTestResultsDiv' class='invisible'>";
        content +=          "<div id='titleGlobalTestResultsDiv'><h4>Test Results</h4><img id='running' src='test/pictures/running.gif'></div>";
        content +=          "<table id='globalTestResultsTable'></table>";
        content +=      "</div>";
        content += "</div>";

        var testsObjects = this.testsObjects;
        content += "<div id=AllTestDiv>";

        for (var i = 0; i < testsObjects.length; i++) {
            var currentObject = testsObjects[i];
            content += "<div id='" + currentObject.name + "Title' class='section'>"
            + "<h4>Section " + currentObject.name + "</h4>"
            + "<button id='" + currentObject.name + "HideShowBtn'>Hide/Show</button>"
            + "<button id='runAll" + currentObject.name + "Btn'>Run tests of this section</button>";
            content += "</div>";
            var idOfTestSection = currentObject.name + "Section";
            this.sectionsID.push(idOfTestSection);

            content += "<div id=" + idOfTestSection + ">";
            content += "<table id='" + currentObject.name + "Table'>";
            content += "<tbody>";
            content += "<thead>";
            content += "<tr>";
            content += "<th>Lauching</th>";
            content += "<th>Tested Method</th>";
            content += "<th>Label</th>";
            content += "<th>Test duration</th>";
            content += "<th>Result</th>";
            content += "</tr>";
            content += "</thead>";

            var currentObject = testsObjects[i];
            var tests = currentObject["tests"];
            for (var j = 0; j < tests.length; j++) {

                var currentTest = tests[j];
                content += "<tr id='" + currentTest["method"] + "'>";
                content += "<td><button id='" + currentTest["method"] + "Btn" + "'>Run</button></td>";
                content += "<td>" + currentTest["method"] + "</td>";
                content += "<td>" + currentTest["label"] + "</td>";
                content += "<td id='" + currentTest["method"] + "Duration'>None</td>";
                content += "<td id='" + currentTest["method"] + "Result'>None</td>";
                content += "</tr>";
            }
            content += "</tbody>";
            content += "</table>";
            content += "</div>";
        }
        content += "</div>";
        document.body.innerHTML += content;
    }

    runTest(currentTest, object) {
        this.updateTestResultStart(currentTest);

        console.group(currentTest["label"]);
        var timeStart = new Date();

        var self = this;
        var result = false;
        var promiseTest = new Promise(function(resolve, reject) {
            try {
                OipfUtils.timeout(object.timer || 5000)
                .then(reject.bind(null, "No result returned."))
                .then(function() {
                    !result
                    && self.stopAnimationInProgress(currentTest)
                    && object.afterTest
                    && object.afterTest.bind(object);
                    self.currentResult && self.currentResult.failed++;
                });
                object.beforeTest && object.beforeTest(resolve, reject);
                object[currentTest["method"]] && object[currentTest["method"]](resolve, reject);
                self.startAnimationInProgress(currentTest);

            } catch (error) {
                reject(error);
            }
        })
        .then(function(val) {
            self.stopAnimationInProgress(currentTest);
            result = true;
            console.groupEnd(currentTest["label"]);
            var timeEnd = new Date();
            self.currentResult && self.currentResult.succeeded++;
            self.updateTestResultSuccess(timeStart, timeEnd, currentTest, val);
            object.afterTest && object.afterTest();
        })
        .catch(function(val) {
            self.stopAnimationInProgress(currentTest);
            self.currentResult && self.currentResult.failed++;
            result = true;
            console.log(val);
            console.groupEnd(currentTest["label"]);
            var timeEnd = new Date();
            self.updateTestResultError(timeStart, timeEnd, currentTest, val);
            object.afterTest && object.afterTest();
        });

        return promiseTest;
    }

    updateTestResultStart(currentTest) {
        document.getElementById(currentTest["method"] + "Result").textContent = "None";
        document.getElementById(currentTest["method"] + "Result").className = "";
        document.getElementById(currentTest["method"] + "Duration").textContent = "None";
    }

    updateTestResultSuccess(timeStart, timeEnd, currentTest, message) {
        document.getElementById(currentTest["method"] + "Result").textContent = message || "Success";
        document.getElementById(currentTest["method"] + "Result").className = "success";
        document.getElementById(currentTest["method"] + "Duration").textContent = (timeEnd - timeStart).toFixed(2) + " ms";
    }

    updateTestResultError(timeStart, timeEnd, currentTest, message) {
        document.getElementById(currentTest["method"] + "Result").textContent = message || "Error";
        document.getElementById(currentTest["method"] + "Result").className = this.colorManaging(message);
        document.getElementById(currentTest["method"] + "Duration").textContent = (timeEnd - timeStart).toFixed(2) + " ms";
    }

    startAnimationInProgress(currentTest) {
        var self = this;

        self.animation.intervalID = setInterval(function() {
            var currentTestResult = document
                .getElementById(currentTest["method"] + "Result");

            if ((self.animation.i % 6) == 0) {
                currentTestResult.textContent = "*";
                self.animation.i = 1;
            } else {
                if (self.animation.start) {
                    currentTestResult.textContent = "*";
                    self.animation.start = false;
                } else {
                    currentTestResult.textContent += "*";
                }
                console.log("Test in progress");
                self.animation.i++;
            }

        }, 500);
    }

    stopAnimationInProgress(object, currentTest) {

        clearInterval(this.animation.intervalID);
        this.animation.id = 1;
        this.animation.start = true;
        this.animation.intervalID = null;

    }

    colorManaging(errorMessage) {
        var map = {
            "No result returned.": "errorNoTestResult",
            "Untestable case for the moment.": "errorUnexecutableTest",
            "Invalid state.": "errorInvalidState"
        };

        if (!errorMessage) {
            return "error";

        } else if (typeof errorMessage == "string") {
            return map[errorMessage];

        } else if (errorMessage instanceof Error) {
            return "errorTechnical";
        }
    }
}
