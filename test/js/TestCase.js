/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
class TestCase {

    constructor() {
        this.modelFactory = new ModelFactory();
        this.UNREALIZED = videoBroadcastConstants.state.UNREALIZED;
        this.CONNECTING = videoBroadcastConstants.state.CONNECTING;
        this.PRESENTING = videoBroadcastConstants.state.PRESENTING;
        this.STOPPED = videoBroadcastConstants.state.STOPPED;
        this.SCHEDULED = recordingConstants.state.SCHEDULED;
        this.STARTED = recordingConstants.state.STARTED;
        this.COMPLETED = recordingConstants.state.COMPLETED;
        this.BEFORE_STARTED = recordingConstants.state.BEFORE_STARTED;
        this.REMOVED = recordingConstants.state.REMOVED;
        this.UPDATED = recordingConstants.state.UPDATED;
        this.SCHEDULED_CONTENT = 1;
        this.CONTENT_ON_DEMAND = 2;
        this.dateUtils = new DateUtils();
        this.timerManager = new TimerManager();
        this.timeout = this.timerManager.createTimer.bind(this.timerManager);
//        this.transitions = null;
    }

    assertNotUndefined(elmt) {
        return elmt;
    }

    assertEquals(elmt1, elmt2) {
        return elmt1 == elmt2;
    }

    assertNotNull(elmt) {
        return elmt;
    }

    waitForError(callback, resolve) {
        new Promise(function(resolve, reject) {
            callback();
        })
        .catch(function(val) {
            console.log(val);
            resolve();
        });
    }

    addTransition(begin, end, callback) {
        this.addTransitionWithError(begin, end, null, callback);
    }

    addTransitionWithError(begin, end, error, callback) {
        this.transitions.push({
            begin: begin,
            end: end,
            error: error,
            callback: callback
        });
    }

    /*
     *
     * @param {type} resolve
     * @param {type} reject
     * @param {type} newState
     * @param {type} errorCode
     * @returns {undefined}
     * FIXME: Yannis - 01/04/2015 - The verification done in else clause
     * could be reviewed.
     */
    onChangeState(resolve, reject, newState, errorCode) {
        var currentTransition = this.transitions[this.currentIndex];
        var begin = currentTransition.begin;
        var end = currentTransition.end;
        var error = currentTransition.error;
        console.log(this.vidBroadObj.playState);

        if (begin == this.previousState && end == newState && error == errorCode) {
            currentTransition.callback && currentTransition.callback();
            this.previousState = newState;
            this.currentIndex++;
        } else {
            if (begin == this.CONNECTING && this.transitions.length == this.currentIndex + 1) {
                reject("Untestable case for the moment.");
            } else {
                reject("Invalid state.");
            }
        }
    }

    createVideoBroadcastObject() {
        this.vidBroadObj = oipfObjectFactory.createVideoBroadcastObject();
    }

}

