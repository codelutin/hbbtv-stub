/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


if (!String.prototype.includes) {
    String.prototype.includes = function() {
        "use strict";
        return String.prototype.indexOf.apply(this, arguments) !== -1;
    };
}

var userAgent = navigator.userAgent;

if (userAgent.includes("HbbTV") || userAgent.includes("SmartTv")) {

    console.log("OIPF TESTS ON TV PLATFORM");
    var hbbTv = ApplicationManager.getWindows[2];
    hbbTv && hbbTv.deactivate();
    var ui = ApplicationManager.getWindows()[0]; //or getWindowsByName
    ui && ui.keySet.setValue(0x10, []);
    !ui.visible && ui.show();
    ui.activate();

    class VideoBroadcastManager extends VideoBroadcastTest {

        constructor() {

            super();
            console.log("VideoBroadcastManager constructor");
            this.startEndStates = {

                "00": "unrealizedToUnrealized",
//                "01": "unrealizedToConnecting",
                "12": "connectingToPresenting",
                "33": "stopToStop",
                "nullnull": "noStatesDefined"
            };

            this.vidBroadObj = oipfObjectFactory.createVideoBroadcastObject();

            this.superBeforeTest = this.beforeTest;
            this.superOnChangeState = this.onChangeState;

        }

        testBindToCurrentChannelInStoppedWhenNoTuner(resolve, reject) {
            reject("This test depend on tv's hardwares.");
        }

        testBindToCurrentChannelWhenNoChannelPresentation(resolve, reject) {
            reject("This test depend on tv's hardwares.");
        }

        onChangeState(resolve, reject, event) {
            this.superOnChangeState.call(this,
            resolve, reject, event.state, event.error);
        }

        beforeTest(resolve, reject) {
            this.onPlayStateChange = this.onChangeState.bind(this, resolve, reject);
            this.vidBroadObj.addEventListener("PlayStateChange", this.onPlayStateChange, false);
            this.transitions = [];
            this.previousState = this.vidBroadObj.playState;
            this.currentIndex = 0;
            this.channel = null;
            this.timerManager = new TimerManager();
            this.timeout = this.timerManager.createTimer.bind(this.timerManager);
            OipfUtils.logTest("State: Pending");
            this.cancel = true;
        }

        unrealizedToConnecting(begin, end, callback) {
            if (callback) {
                throw "Untestable Case.";
            }
        }

        connectingToPresenting(begin, end, callback) {
             /*
             * Avoid that the future transition C->P be canceled.
             * More extend super method beforeTest in order to reset
             * 'startEndStates' property.
             */

            if (this.cancel) {
                callback && this.timeout().then(callback).then(console.log.bind(console, "TOTO"));
                this.cancel = false;
            } else {
                this.pushTransition(begin, end, callback, null);
            }
        }

        unrealizedToUnrealized(begin, end, callback) {
            this.reject("Untestable Case.");
        }

        stopToStop(begin, end, callback) {
            this.reject("Untestable Case.");
        }

        noStatesDefined(begin, end, callback) {
            this.reject("Untestable Case.");
        }

        pushTransition(begin, end, callback, error) {
            this.transitions.push({
                begin: begin,
                end: end,
                error: error,
                callback: callback
            });
        }

        pushTransitionWithError(begin, end, callback, error) {
            var method = this.startEndStates["" + begin + end];
            if (method) {
                this[method].call(this, begin, end, callback);
            } else {
                this.pushTransition(begin, end, callback, error);
            }
        }

        addTransition(begin, end, callback) {
            this.addTransitionWithError(begin, end, callback, null);
        }

        addTransitionWithError(begin, end, callback, error) {
            this.pushTransitionWithError(begin, end, callback, error);
        }
    }
    console.log("VideoBroadcastManager class defined.");

    var VideoBroadcastTest = class extends VideoBroadcastManager {

        constructor() {
            super();
        }

    };

    console.log("VideoBroadcastTest class redefined.");

    var SearchManagerTest = class extends SearchManagerTest {

        constructor() {
            super();
            this.searchManagerObj = oipfObjectFactory.createSearchManagerObject();
            this.createVideoBroadcastObject();

            /*The VideoBroadcastObject must be created on TV only one time if the TV own only one tuner
             *because this one block this tuner.
             */
            this.createVideoBroadcastObject = function() {};
            console.log(this.createVideoBroadcastObject);
        }

        addTransition(begin, end, callback) {
            if (begin + end == 3) {
                callback && callback();
            }
        }
    };
    console.log("SearchManagerTest class redefined.");

    var RecordingSchedulerTest = class extends RecordingSchedulerTest {

        constructor() {
            super();
            this.blocked = function() {};
            this.bindToCurrentChannel = this.blocked;
            this.onChangeState = this.blocked;
            this.createVideoBroadcastObject = this.blocked;
        }

        onPVREvent(resolve, reject, event) {
            console.log("beforeTest");
            this.onPVREvent(resolve, reject, event.state, event.recording);
        }

        beforeTest(resolve, reject) {
            console.log("beforeTest");
            this.beforeTest(resolve, reject);
        }

    };
    console.log("RecordingSchedulerTest class redefined.");

}
