/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * Description:
 * This class gather a set of method necessary to test a video broadcast object.
 *
 */

var VideoBroadcastTest = class extends TestCase {

    constructor() {
        super();
        this.channel = null;

        this.vidBroadObj = null;

        this.errorMap = {
            0: "Channel not supported by tuner.",
            1: "Cannot tune to given transport stream (e.g. no signal).",
            2: "Tuner locked by other object.",
            3: "Parental lock on channel.",
            4: "Encrypted channel, key/module missing.",
            5: "Unknown channel (e.g. canâ€™t resolve DVB or ISDB triplet).",
            6: "Channel switch interrupted (e.g. because another channel switch was activated before the previous one completed).",
            7: "Channel cannot be changed, because it is currently being recorded.",
            8: "Cannot resolve URI of referenced IP channel.",
            9: "Insufficient bandwidth.",
            10: "Channel cannot be changed by nextChannel()/prevChannel() methods either because the OITF does not maintain a favourites or channel list or because the video/broadcast object is in the Unrealized state.",
            11: "Insufficient resources are available to present the given channel (e.g. a lack of available codec resources).",
            12: "Specified channel not found in transport stream.",
            100: "Unidentified error."
        };
    }

    beforeTest(resolve, reject) {
        this.createVideoBroadcastObject();
        this.onPlayStateChange = this.onChangeState.bind(this, resolve, reject);
        this.vidBroadObj.addEventListener("PlayStateChange", this.onPlayStateChange, false);
        this.transitions = [];
        this.previousState = this.vidBroadObj.playState;
        this.currentIndex = 0;
        this.channel = null;
        OipfUtils.logTest("State: Pending");
    }

    afterTest() {
//      this.vidBroadObj.release(); //Useful for test on tv plateform
        this.vidBroadObj.removeEventListener("PlayStateChange", this.onPlayStateChange);
        this.timerManager.clearTimer();
    }

    getChannel(channelCollection) {
        if (channelCollection.length != 0) {
            return channelCollection[0];
        }
        return null;
    }

//    /*
//     *
//     * @param {type} resolve
//     * @param {type} reject
//     * @param {type} newState
//     * @param {type} errorCode
//     * @returns {undefined}
//     * FIXME: Yannis - 01/04/2015 - The verification done in else clause
//     * could be reviewed.
//     */
//    onChangeState(resolve, reject, newState, errorCode) {
//        var currentTransition = this.transitions[this.currentIndex];
//        var begin = currentTransition.begin;
//        var end = currentTransition.end;
//        var error = currentTransition.error;
//        console.log(this.vidBroadObj.playState);
//        if (begin == this.previousState && end == newState && error == errorCode) {
//            currentTransition.callback && currentTransition.callback();
//            this.previousState = newState;
//            this.currentIndex++;
//        } else {
//            //
//            if (begin == this.CONNECTING
//            && this.transitions.length == this.currentIndex + 1) {
//                reject("Untestable case for the moment.");
//            } else {
//                reject("Invalid state");
//            }
//        }
//    }

    /*
     * This method must throw a error by using the method reject.
     *
     * @param {type} resolve
     * @param {type} reject
     * @returns {undefined}
     */
    testBindToCurrentChannelWhenNoChannelPresentation(resolve, reject) {
        var self = this;
        //Subject to modification not rigth way failed on the tv plateform.
        this.vidBroadObj._channelService.currentChannel = null;

        this.addTransitionWithError(this.UNREALIZED, this.UNREALIZED, 100, function() {
            self.vidBroadObj._channelService.setCurrentChannel("ARTE");
            resolve();
        });
        this.vidBroadObj.bindToCurrentChannel();
    }

    /*
     * Descrtiption:
     * This method test the binding between the video broadcast object and the current program stream.
     * ToDO: Extraction of method onPlayStateChange out.
     */
//    testBindToCurrentChannel(resolve, reject) {
//        var self = this;
//
//        this.addTransition(this.previousState, this.PRESENTING);
//
//        this.vidBroadObj.bindToCurrentChannel();
//    }

    testDoubleBindToCurrentChannel(resolve, reject) {

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, resolve);

        this.vidBroadObj.bindToCurrentChannel();
        this.vidBroadObj.bindToCurrentChannel();
    }

    testBindToCurrentChannelInConnecting(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING, function() {
            self.vidBroadObj.bindToCurrentChannel();
        });

        this.addTransition(this.CONNECTING, this.PRESENTING, resolve);

        this.vidBroadObj.bindToCurrentChannel();
    }

    testBindToCurrentChannelInPresenting(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {
            self.vidBroadObj.bindToCurrentChannel();
            resolve();
        });

        this.vidBroadObj.bindToCurrentChannel();
    }

    testBindToCurrentChannelInStopped(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {
            self.vidBroadObj.stop();
        });

        this.addTransition(this.PRESENTING, this.STOPPED, function() {
            self.vidBroadObj.bindToCurrentChannel();
        });

        this.addTransition(this.STOPPED, this.CONNECTING);
        this.addTransition(this.CONNECTING, this.PRESENTING, resolve);

        this.vidBroadObj.bindToCurrentChannel();

    }

    /*
     * This method must throw a error by using the method reject.
     * @param {type} resolve
     * @param {type} reject
     * @returns {undefined}
     */
    testBindToCurrentChannelInStoppedWhenNoTuner(resolve, reject) {
        var self = this;

        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {
            self.vidBroadObj.stop();
        });

        this.addTransition(this.PRESENTING, this.STOPPED, function() {
            //We want test the case where the current channel has a idType whose
            //any tuner supporting this kind of channel exist.
            //So we create channel of idType "ATSCT".
            self.vidBroadObj._channelService.currentChannel = new Channel(ID_ATSC_T, null, null);
            self.vidBroadObj.bindToCurrentChannel();
        });

        this.addTransitionWithError(this.STOPPED, this.STOPPED, 0, function() {
            self.vidBroadObj._channelService.setCurrentChannel("ARTE");
            console.log("CurrentChannel", self.vidBroadObj.getChannelConfig().currentChannel.name);
            resolve();
        });

        this.vidBroadObj.bindToCurrentChannel();
    }

    testPrevChannelInUnrealized(resolve, reject) {
        this.addTransition(null, null, reject);
        this.vidBroadObj.onChannelChangeSucceeded = function(channel) {
            reject();
        };
        this.vidBroadObj.prevChannel();
        this.timeout(3000).then(resolve);

    }

    testPrevChannelInConnecting(resolve, reject) {
        var self = this;
        this.addTransition(this.UNREALIZED, this.CONNECTING, function() {
            self.vidBroadObj.prevChannel();
        });

        this.addTransition(this.CONNECTING, this.CONNECTING);

        this.addTransition(this.PRESENTING, this.CONNECTING);

        this.vidBroadObj.onChannelChangeSucceeded = function(channel) {

            if (self.assertNotNull(channel) && self.assertEquals(this.vidBroadObj, this.PRESENTING)) {
                OipfUtils.logTest("State: Successful");
                resolve();
            } else {
                OipfUtils.logTest("State: Failure");
                reject("Failure : The channel is null for a unknown reason or wrong play state.");
            }
        };

        this.vidBroadObj.bindToCurrentChannel();

    }

    testPrevChannelInPresenting(resolve, reject) {
        var self = this;
        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {
            self.vidBroadObj.prevChannel();
        });

        this.addTransition(this.PRESENTING, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING);

        this.vidBroadObj.onChannelChangeSucceeded = function(channel) {

            if (self.assertNotNull(channel) && self.assertEquals(this.vidBroadObj, this.PRESENTING)) {
                OipfUtils.logTest("State: Successful");
                resolve("Successfull");
            } else {
                OipfUtils.logTest("State: Failure");
                reject("Failure : The channel is null for a unknown reason or wrong play state.");
            }
        };

        this.vidBroadObj.bindToCurrentChannel();

    }

    testPrevChannelInStopped(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {
            self.vidBroadObj.stop();
        });

        this.addTransition(this.PRESENTING, this.STOPPED, function() {
            self.vidBroadObj.prevChannel();
        });

        this.addTransition(this.STOPPED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING);

        this.vidBroadObj.onChannelChangeSucceeded = function(channel) {

            if (self.assertNotNull(channel) && self.assertEquals(this.vidBroadObj, this.PRESENTING)) {
                OipfUtils.logTest("State: Successful");
                resolve("Successfull");
            } else {
                OipfUtils.logTest("State: Failure");
                reject("Failure : The channel is null for a unknown reason or wrong play state.");
            }
        };

        this.vidBroadObj.bindToCurrentChannel();

    }

//    testPrevChannel(resolve, reject) {
//        var self = this;
//
//        this.addTransition(this.PRESENTING, this.CONNECTING);
//
//        this.addTransition(this.CONNECTING, this.PRESENTING);
//
//        this.vidBroadObj.onChannelChangeSucceeded = function(channel) {
//
//            if (self.assertNotNull(channel) && self.assertEquals(this.vidBroadObj, this.PRESENTING)) {
//                OipfUtils.logTest("State: Successful");
//                resolve("Successfull");
//            } else {
//                OipfUtils.logTest("State: Failure");
//                reject("Failure : The channel is null for a unknown reason or wrong play state.");
//            }
//        };
//
//        this.vidBroadObj.prevChannel();
//
//    }

    testNextChannelInUnrealized(resolve, reject) {
        this.addTransition(null, null, reject);
        this.vidBroadObj.onChannelChangeSucceeded = function(channel) {
            reject();
        };
        this.vidBroadObj.nextChannel();
        this.timeout(3000)
        .then(resolve);
    }

    testNextChannelInConnecting(resolve, reject) {
        var self = this;
        this.addTransition(this.UNREALIZED, this.CONNECTING, function() {
            self.vidBroadObj.nextChannel();
        });

        this.addTransition(this.CONNECTING, this.CONNECTING);

        this.addTransition(this.PRESENTING, this.CONNECTING);

        this.vidBroadObj.onChannelChangeSucceeded = function(channel) {

            if (self.assertNotNull(channel) && self.assertEquals(this.vidBroadObj, this.PRESENTING)) {
                OipfUtils.logTest("State: Successful");
                resolve("Successfull");
            } else {
                OipfUtils.logTest("State: Failure");
                reject("Failure : The channel is null for a unknown reason or wrong play state.");
            }
        };

        this.vidBroadObj.bindToCurrentChannel();

    }

    testNextChannelInPresenting(resolve, reject) {
        var self = this;
        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {
            self.vidBroadObj.nextChannel();
        });

        this.addTransition(this.PRESENTING, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING);

        this.vidBroadObj.onChannelChangeSucceeded = function(channel) {

            if (self.assertNotNull(channel) && self.assertEquals(this.vidBroadObj, this.PRESENTING)) {
                OipfUtils.logTest("State: Successful");
                resolve("Successfull");
            } else {
                OipfUtils.logTest("State: Failure");
                reject("Failure : The channel is null for a unknown reason or wrong play state.");
            }
        };

        this.vidBroadObj.bindToCurrentChannel();

    }

    testNextChannelInStopped(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {
            self.vidBroadObj.stop();
        });

        this.addTransition(this.PRESENTING, this.STOPPED, function() {
            self.vidBroadObj.nextChannel();
        });

        this.addTransition(this.STOPPED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING);

        this.vidBroadObj.onChannelChangeSucceeded = function(channel) {

            if (self.assertNotNull(channel) && self.assertEquals(this.vidBroadObj, this.PRESENTING)) {
                OipfUtils.logTest("State: Successful");
                resolve("Successfull");
            } else {
                OipfUtils.logTest("State: Failure");
                reject("Failure : The channel is null for a unknown reason or wrong play state.");
            }
        };

        this.vidBroadObj.bindToCurrentChannel();

    }

//    testNextChannel(resolve, reject) {
//        var self = this;
//
//        this.addTransition(this.PRESENTING, this.CONNECTING);
//
//        this.addTransition(this.CONNECTING, this.PRESENTING);
//
//        this.vidBroadObj.onChannelChangeSucceeded = function(channel) {
//
//            if (self.assertNotNull(channel) && self.assertEquals(this.vidBroadObj, this.PRESENTING)) {
//                OipfUtils.logTest("State: Successful");
//                resolve("Successfull");
//            } else {
//                OipfUtils.logTest("State: Failure");
//                reject("Failure : The channel is null for a unknown reason or wrong play state.");
//            }
//        };
//
//        this.vidBroadObj.nextChannel();
//    }

    /*
     * Description:
     * Nominal Case
     */
    testReleaseInConnecting(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING, function() {
            self.vidBroadObj.release();
        });

        this.addTransition(this.CONNECTING, this.UNREALIZED, resolve);

        this.vidBroadObj.bindToCurrentChannel();
    }

    testReleaseInPresenting(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {
            self.vidBroadObj.release();
        });

        this.addTransition(this.PRESENTING, this.UNREALIZED, resolve);

        this.vidBroadObj.bindToCurrentChannel();
    }

    /*
     * Description:
     * Method stop is called in presenting state(but can be also in connecting state).
     * @param {type} resolve
     * @param {type} reject
     * @returns {undefined}
     */
    testReleaseInStopped(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {
            self.vidBroadObj.stop();
        });

        this.addTransition(this.PRESENTING, this.STOPPED, function() {
            self.vidBroadObj.release();
        });

        this.addTransition(this.STOPPED, this.UNREALIZED, resolve);

        this.vidBroadObj.bindToCurrentChannel();
    }

//    testRelease(resolve, reject) {
//        var self = this;
//
//        this.addTransition(this.PRESENTING, this.UNREALIZED, resolve);
//
//        this.vidBroadObj.release();
//    }

    /*
     * Description:
     * Nominal Case
     */
    testStopInConnecting(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING, function() {
            self.vidBroadObj.stop();
        });

        this.addTransition(this.CONNECTING, this.STOPPED, resolve);

        this.vidBroadObj.bindToCurrentChannel();
    }

    testStopInPresenting(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {
            self.vidBroadObj.stop();
        });

        this.addTransition(this.PRESENTING, this.STOPPED, resolve);

        this.vidBroadObj.bindToCurrentChannel();
    }

//    testStop(resolve, reject) {
//        var self = this;
//
//        this.addTransition(this.PRESENTING, this.STOPPED, resolve);
//
//        this.vidBroadObj.stop();
//    }

    /*
     * Description:
     * Nominal Case
     */
    testSetVolume(resolve, reject) {
        var volume = 15;
        var done = this.vidBroadObj.setVolume(volume);

        if (done && this.assertEquals(volume, this.vidBroadObj.getVolume())) {
            resolve();
        } else {
            reject();
        }
    }

    testSetVolumeWithoutArgument(resolve, reject) {
        var done = this.vidBroadObj.setVolume();

        if (done) {
            reject();
        } else {
            resolve();
        }
    }

    testSetVolumeWithFloatArgument(resolve, reject) {
        var volume = 15.8;
        var done = this.vidBroadObj.setVolume(volume);

        if (done || this.assertEquals(volume, this.vidBroadObj.getVolume())) {
            reject("[Stub]Wrong Implementation.");
        } else {
            resolve();
        }
    }

    testSetVolumeWithFloatUnderBoundMin(resolve, reject) {
        var volume = -15.8;
        var done = this.vidBroadObj.setVolume(volume);

        if (done || this.assertEquals(volume, this.vidBroadObj.getVolume())) {
            reject("[Stub]Wrong Implementation.");
        } else {
            resolve();
        }
    }

    testSetVolumeWithFloatAboveBoundMax(resolve, reject) {
        var volume = 151.74;
        var done = this.vidBroadObj.setVolume(volume);

        if (done || this.assertEquals(volume, this.vidBroadObj.getVolume())) {
            reject("[Stub]Wrong Implementation.");
        } else {
            resolve();
        }
    }

    testSetVolumeWithValueUnderBoundMin(resolve, reject) {
        var volume = -15;
        var done = this.vidBroadObj.setVolume(volume);

        if (done && this.assertEquals(volume, this.vidBroadObj.getVolume())) {
            reject();
        } else {
            resolve();
        }
    }

    testSetVolumeWithValueAboveBoundMax(resolve, reject) {
        var volume = 105;
        var done = this.vidBroadObj.setVolume(volume);

        if (done && this.assertEquals(volume, this.vidBroadObj.getVolume())) {
            reject();
        } else {
            resolve();
        }
    }

//    testSetVolume(resolve, reject) {
//        var volume = 15;
//
//        var done = this.vidBroadObj.setVolume(volume);
//
//        if (done) {
//            resolve();
//        } else {
//            reject();
//        }
//    }

    /*
     * Description:
     * Nominal Case
     * FIX-ME: Yannis - 24/03/2015 - Exception not throw in callback of method addTransition
     */
    testGetVolume(resolve, reject) {
        var constructorialVolume = 15;
        var done = this.vidBroadObj.setVolume(constructorialVolume);

        var finalVolume = this.vidBroadObj.getVolume();

        if (done && this.assertEquals(constructorialVolume, this.vidBroadObj.getVolume())
            && this.assertEquals(constructorialVolume, finalVolume)
            && this.assertEquals(this.vidBroadObj.getVolume(), finalVolume)) {

            resolve();
        } else {
            reject();
        }
    }

//    testGetVolume(resolve, reject) {
//        var volume = this.vidBroadObj.getVolume();
//
//        if (volume ) {
//            resovle();
//        } else {
//            reject();
//        }
//    }
    testSetChannelInUnrealized(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {
            self.vidBroadObj.setChannel(self.channel);
        });

        this.addTransition(this.PRESENTING, this.CONNECTING);
        this.addTransition(this.CONNECTING, this.PRESENTING);

        this.vidBroadObj.onChannelChangeSucceeded = function(channel) {

            if (self.assertNotNull(channel) && self.assertEquals(self.vidBroadObj.playState, self.PRESENTING)) {
                OipfUtils.logTest("State: Successful");
                resolve("Success");
            } else {
                OipfUtils.logTest("State: Failure");
                reject("Failure : The channel is null for a unknown reason or wrong play state.");
            }
        };

        this.vidBroadObj.bindToCurrentChannel();
    }

    testSetChannelInConnecting(resolve, reject) {
        var self = this;

        this.channel = this.getChannel(
            this.vidBroadObj.getChannelConfig().channelList);

        this.addTransition(this.UNREALIZED, this.CONNECTING, function() {
            self.vidBroadObj.setChannel(self.channel);
        });

        this.addTransition(this.CONNECTING, this.PRESENTING);

        this.vidBroadObj.onChannelChangeSucceeded = function(channel) {

            if (self.assertNotNull(channel) && self.assertEquals(self.vidBroadObj.playState, self.PRESENTING)) {
                OipfUtils.logTest("State: Successful");
                resolve("Success");
            } else {
                OipfUtils.logTest("State: Failure");
                reject("Failure : The channel is null for a unknown reason or wrong play state.");
            }
        };

        this.vidBroadObj.bindToCurrentChannel();
    }

    testSetChannelInConnectingWithNull(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING, function() {
            self.vidBroadObj.setChannel(null);
        });

        this.addTransition(this.CONNECTING, this.UNREALIZED, resolve);

        this.vidBroadObj.bindToCurrentChannel();
    }

    testSetChannelInPresenting(resolve, reject) {
        var self = this;

        this.channel = this.getChannel(
            this.vidBroadObj.getChannelConfig().channelList);

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {
            self.vidBroadObj.setChannel(self.channel);
        });

        this.addTransition(this.PRESENTING, this.CONNECTING);
        this.addTransition(this.CONNECTING, this.PRESENTING);

        this.vidBroadObj.onChannelChangeSucceeded = function(channel) {

            if (self.assertNotNull(channel) && self.assertEquals(self.vidBroadObj.playState, self.PRESENTING)) {
                OipfUtils.logTest("State: Successful");
                resolve("Success");
            } else {
                OipfUtils.logTest("State: Failure");
                reject("Failure : The channel is null for a unknown reason or wrong play state.");
            }
        };

        this.vidBroadObj.bindToCurrentChannel();
    }

    testSetChannelInPresentingWithNull(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {
            self.vidBroadObj.setChannel(null);
        });

        this.addTransition(this.PRESENTING, this.UNREALIZED, resolve);

        this.vidBroadObj.bindToCurrentChannel();

    }

    testSetChannelInStopped(resolve, reject) {
        var self = this;

        this.channel = this.getChannel(
            this.vidBroadObj.getChannelConfig().channelList);

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {
            self.vidBroadObj.stop();
        });

        this.addTransition(this.PRESENTING, this.STOPPED, function() {
            self.vidBroadObj.setChannel(self.channel);
        });

        this.addTransition(this.STOPPED, this.CONNECTING);
        this.addTransition(this.CONNECTING, this.PRESENTING);

        this.vidBroadObj.onChannelChangeSucceeded = function(channel) {

            if (self.assertNotNull(channel) && self.assertEquals(self.vidBroadObj.playState, self.PRESENTING)) {
                OipfUtils.logTest("State: Successful");
                resolve("Success");
            } else {
                OipfUtils.logTest("State: Failure");
                reject("Failure : The channel is null for a unknown reason or wrong play state.");
            }
        };

        this.vidBroadObj.bindToCurrentChannel();
    }

    testSetChannelInStoppedWithNull(resolve, reject) {
        var self = this;

        this.addTransition(this.UNREALIZED, this.CONNECTING);

        this.addTransition(this.CONNECTING, this.PRESENTING, function() {
            self.vidBroadObj.stop();
        });

        this.addTransition(this.PRESENTING, this.STOPPED, function() {
            self.vidBroadObj.setChannel(null);
        });

        this.addTransition(this.STOPPED, this.UNREALIZED, resolve);

        this.vidBroadObj.bindToCurrentChannel();

    }

};

