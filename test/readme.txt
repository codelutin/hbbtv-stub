This is project contains unit test for oipf implementation. You can run it on a machine to determine how well it handle oipf : it will gives you a feedback on what part of the spec is supported or not.

To run the project, run :
> gulp

Then browse http://<ip address of your machine>:3000
